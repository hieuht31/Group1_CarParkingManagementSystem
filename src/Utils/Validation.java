package Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import Dao.DepartmentDAO;
import Entities.Department;

public class Validation {

	public boolean validString(String inputString, int max, int min) {
		if (inputString.length() < min || inputString.length() > max) {
			return false;
		}
		return true;
	}

	public boolean validPhone(String phone) {
		String regex = "^\\d{10}$";
		if (phone.matches(regex)) {
			return true;
		}

		return false;
	}

	public boolean validDate(String date) {
		// format date yyyy-MM-dd in java
		String regex = "^\\d{4}-\\d{2}-\\d{2}$";
		if (date.matches(regex)) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			sdf.setLenient(false);
			try {
				Date d = sdf.parse(date);
				// check date must be less than current date
				if (d.after(new Date())) {
					return false;
				}
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		return false;
	}

	public boolean validEmail(String email) {
		// email format
		String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
		if (email.matches(regex) || email.length() == 0) {
			return true;
		}
		return false;
	}

	public boolean validPassword(String password) {
		// at least 1 digit, 1 lower case, 1 upper case from 6 to 50 characters in java
		// (not javascript)
		String regex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,50}$";
		if (password.matches(regex)) {
			return true;
		}
		return false;
	}

	public boolean intergerNumber(String number, String element) {
		try {
			int a = Integer.parseInt(number);
			if (element.equalsIgnoreCase("sex")) {
				if (a != 1 && a != 0) {
					return false;
				}
			}
			if (element.equalsIgnoreCase("department")) {
				DepartmentDAO DEPARTMENT_DAO = new DepartmentDAO();
				ArrayList<Department> deps = DEPARTMENT_DAO.getAllDepartment();
				// if a is not in list department id
				if (!deps.stream().anyMatch(dep -> dep.getDepartment_id() == a)) {
					return false;
				}
			}
			
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		Validation c = new Validation();
		String phone = "0355928414";
		String email = "a@gmail.com";
		String password = "123jjj";
		String date = "2021-12-12";
		System.out.println(c.validDate(date));
	}
}
