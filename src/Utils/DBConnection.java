package Utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnection {
	public Connection getConnection() {
		try {
			String url = "jdbc:sqlserver://localhost:1433;databaseName=car_parking_management_system";
			String user = "sa";
			String pass = "12345678";
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			return DriverManager.getConnection(url, user, pass);
		} catch (Exception ex) {
			Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}
	public static void main(String[] args) {
		DBConnection d = new DBConnection();
		System.out.println(d.getConnection());
	}
}