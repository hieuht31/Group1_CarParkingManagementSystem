package Controller.ParkingStaff.CarManagement;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.CarDAO;
import Entities.Car;
import Entities.FilterContent;
import Entities.ServiceResponse;

/**
 * Servlet implementation class ViewCarServlet
 */
@WebServlet({"/car/list","/car/view"})


public class ViewCarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewCarServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		System.out.println(request.getQueryString());
//		CarDAO carDAO = new CarDAO();
//		List<Car> listCars = carDAO.listCars();
//		
//		String fillterBy = request.getParameter("fillterBy");
//		String searchString = request.getParameter("searchString");
//		
//		int currentPage = request.getParameter("currentPage") == null ? 1: Integer.parseInt(request.getParameter("currentPage"));
//		int pageSize = 3;
//		
//		
//		if(searchString == null) {
//			try {
//				listCars = carDAO.listCarsWithPagging(currentPage, pageSize);
//				request.setAttribute("listCars", listCars);
//				request.setAttribute("currentPage", currentPage);
//				request.setAttribute("pageSize", pageSize);
//				request.setAttribute("numberPage", (int) Math.ceil(carDAO.listCars().size() / pageSize) + 1);
//				request.getRequestDispatcher("../Views/View_CarManagement/CarView.jsp").forward(request, response);
//			} catch (Exception e) {
//				// TODO: handle exception
//			}	
//		}
		request.getRequestDispatcher("../Views/View_CarManagement/CarView.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		FilterContent filterContent = new FilterContent();
		CarDAO carDAO = new CarDAO();
		try {
			filterContent.setPageNumber(Integer.parseInt(request.getParameter("pageNumber")));
			filterContent.setPageSize(Integer.parseInt(request.getParameter("pageSize")));
			filterContent.setFilterBy(request.getParameter("filterBy"));
			filterContent.setSearchValue(request.getParameter("searchValue"));
			serviceResponse.setSuccess(true);
			serviceResponse.setData(carDAO.findAll(filterContent));
		} catch (Exception e) {
			serviceResponse.setSuccess(false);
			serviceResponse.setMessage(e.getMessage());
		}
		out.print(gson.toJson(serviceResponse));
	}

}
