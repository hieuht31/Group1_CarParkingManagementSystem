package Controller.ParkingStaff.CarManagement;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.CarDAO;
import Dao.TicketDAO;
import Entities.ServiceResponse;

/**
 * Servlet implementation class DeleteCarServlet
 */
@WebServlet("/car/delete")
public class DeleteCarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteCarServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		int car_id = Integer.parseInt(request.getParameter("car_id")==null ? "0" : request.getParameter("car_id"));
//		
//		boolean deleteFlag = (new CarDAO()).deleteCar(car_id);
//		if(deleteFlag) {
//			request.setAttribute("successMessage", "Delete success");
//		} else request.setAttribute("errorMessage", "Oops, Connect with database has some error!Delete fail");
//		request.getRequestDispatcher("view?"+request.getQueryString()).forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json; charset=UTF-8");
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		
		int car_id = Integer.parseInt(request.getParameter("car_id")==null?"0":request.getParameter("car_id"));
		boolean deleteFlag = (new CarDAO().deleteCar(car_id));
		if(deleteFlag) {
				serviceResponse.setSuccess(true);
				serviceResponse.setMessage("Delete car successfully !");
			} else {
				serviceResponse.setSuccess(false);
				serviceResponse.setMessage("Delete car failed !");
			}
		
		out.print(gson.toJson(serviceResponse));
	}

}
