package Controller.ParkingStaff.CarManagement;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Time;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.CarDAO;
import Dao.ParkingLotDAO;
import Dao.TicketDAO;
import Entities.Car;
import Entities.ParkingLot;
import Entities.ServiceResponse;
import Entities.Ticket;
import Entities.Trip;

/**
 * Servlet implementation class AddCarServlet
 */
@WebServlet("/car/add")
public class AddCarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddCarServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		CarDAO carDAO = new CarDAO();
		ParkingLotDAO parkingLotDAO = new ParkingLotDAO();
		
		List<Car> listCars = carDAO.listCars();
		List<ParkingLot> listParkingLot = parkingLotDAO.getAllParkingLot();
		
		request.setAttribute("listParkingLot", listParkingLot);
		request.setAttribute("listCars", listCars);
		request.getRequestDispatcher("../Views/View_CarManagement/CarAdd.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		response.setContentType("application/json; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		String license_plate = request.getParameter("license_plate");
		String car_type = request.getParameter("car_type");
		String car_color = request.getParameter("car_color");
		String company = request.getParameter("company");
		String park_id = request.getParameter("parkingLot");
		boolean flagPark = new ParkingLotDAO().checkAvailablePark(park_id);
		if(!flagPark) {
			serviceResponse.setSuccess(false);
			serviceResponse.setMessage("Add Car failed ! Parking lot have full");
		}
		boolean flagLicense = new CarDAO().checkLicense(license_plate);
		if(flagLicense) {
			serviceResponse.setSuccess(false);
			serviceResponse.setMessage("Add Car failed ! License plate have exist!");
		}
		if(flagPark&&!flagLicense) {
			boolean flag = new CarDAO().addCar(new Car(0, 
					license_plate, 
					car_color, 
					car_type, 
					company, 
					new ParkingLot(Integer.parseInt(park_id), 0, null, null, null, 0,0)));
			
			
			if(flag) {
				serviceResponse.setSuccess(true);
				serviceResponse.setMessage("Car added into list car");
			} else {
				serviceResponse.setSuccess(false);
				serviceResponse.setMessage("Add Car failed !");
			}			
		} 
		out.print(gson.toJson(serviceResponse));
	}

	}

