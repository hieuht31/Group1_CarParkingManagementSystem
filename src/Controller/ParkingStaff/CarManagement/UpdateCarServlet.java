package Controller.ParkingStaff.CarManagement;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.CarDAO;
import Dao.ParkingLotDAO;
import Entities.Car;
import Entities.ParkingLot;
import Entities.ServiceResponse;

/**
 * Servlet implementation class UpdateCarServlet
 */
@WebServlet("/car/update")
public class UpdateCarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateCarServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String car_id = request.getParameter("car_id");
		
		List<Car> listCars = new CarDAO().listCars();
		List<ParkingLot> listParkingLot = new ParkingLotDAO().getAllParkingLot();
		
		Car car = new CarDAO().findCarById(Integer.parseInt(car_id == null?"0":car_id));
		
		request.setAttribute("car", car);
		request.setAttribute("listCars", listCars);
		request.setAttribute("listParkingLot", listParkingLot);
		
		request.getRequestDispatcher("../Views/View_CarManagement/CarUpdate.jsp").forward(request, response);
		 
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		String car_id = request.getParameter("car_id");
//		String license_plate = request.getParameter("license_plate");
//		String car_type = request.getParameter("car_type");
//		String car_color = request.getParameter("car_color");
//		String company = request.getParameter("company");
//		String park_id = request.getParameter("parkingLot");
//		
//		boolean flag = new CarDAO().updateCar(new Car(Integer.parseInt(car_id), 
//				license_plate, 
//				car_color, 
//				car_type, 
//				company, 
//				new ParkingLot(Integer.parseInt(park_id), 0, null, null, null, false)));
//		
//		if(flag) {
//			request.setAttribute("successMessage", "Ticket update into list ticket");
//		} else {
//			request.setAttribute("errorMessage", "Oops Add failed! Have some trouble when connect with database");
//		}
//		request.getRequestDispatcher("view").forward(request, response);
		response.setContentType("application/json; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		String car_id = request.getParameter("car_id");
		String license_plate = request.getParameter("license_plate");
		String car_type = request.getParameter("car_type");
		String car_color = request.getParameter("car_color");
		String company = request.getParameter("company");
		String park_id = request.getParameter("parkingLot");
		boolean flagPark = new ParkingLotDAO().checkAvailablePark(park_id);
		if(!flagPark) {
			serviceResponse.setSuccess(false);
			serviceResponse.setMessage("Add Car failed ! Parking lot have full");
		}
		
		boolean flagLicense = new CarDAO().checkLicense(license_plate);
		if(flagLicense) {
			serviceResponse.setSuccess(false);
			serviceResponse.setMessage("Add Car failed ! License plate have exist!");
		} 
		if(flagPark&&!flagLicense){
			boolean flag = new CarDAO().updateCar(new Car(Integer.parseInt(car_id), 
					license_plate, 
					car_color, 
					car_type, 
					company, 
					new ParkingLot(Integer.parseInt(park_id), 0, null, null, null, 0,0)));
			
			if(flag) {
				serviceResponse.setSuccess(true);
				serviceResponse.setMessage("Car update sucessful");
			} else {
				serviceResponse.setSuccess(false);
				serviceResponse.setMessage("Update Car failed !");
			}			
		}
		out.print(gson.toJson(serviceResponse));

	}

}
