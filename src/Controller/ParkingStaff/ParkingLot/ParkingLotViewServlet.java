package Controller.ParkingStaff.ParkingLot;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.ParkingLotDAO;
import Dao.TicketDAO;
import Entities.FilterContent;
import Entities.ParkingLot;
import Entities.ServiceResponse;

/**
 * Servlet implementation class ParkingLotViewServlet
 */
@WebServlet({"/park/view","/park/list"})
public class ParkingLotViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ParkingLotViewServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("../Views/View_ParkingLot/ParkingLotView.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
			
		ServiceResponse serviceResponse = new ServiceResponse();
		FilterContent filterContent = new FilterContent();
		ParkingLotDAO parkingLotDAO = new ParkingLotDAO();
		try {
			filterContent.setPageNumber(Integer.parseInt(request.getParameter("pageNumber")));
			filterContent.setPageSize(Integer.parseInt(request.getParameter("pageSize")));
			filterContent.setFilterBy(request.getParameter("filterBy"));
			if(request.getParameter("filterBy").equalsIgnoreCase("park_status")) {
				switch (request.getParameter("searchValue").toLowerCase()) {
				case "active":
					filterContent.setSearchValue("0");
					break;
				case "inactive":
					filterContent.setSearchValue("2");
					break;
				case "full":
					filterContent.setSearchValue("1");
					break;	
				default:
					filterContent.setSearchValue("3");
					break;
				}
			} else {
				filterContent.setSearchValue(request.getParameter("searchValue"));
			}
			
			serviceResponse.setData(parkingLotDAO.findAll(filterContent));
			System.out.println(gson.toJson(serviceResponse)); 

				serviceResponse.setSuccess(true);

		} catch (Exception e) {
			serviceResponse.setSuccess(false);
			serviceResponse.setMessage(e.getMessage());
		}
		out.print(gson.toJson(serviceResponse));
			
	}

}
