package Controller.ParkingStaff.ParkingLot;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.ParkingLotDAO;
import Entities.ParkingLot;
import Entities.ServiceResponse;

/**
 * Servlet implementation class UpdateParkingLot
 */
@WebServlet("/park/update")
public class UpdateParkingLot extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateParkingLot() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int park_id = Integer.parseInt(request.getParameter("park_id")==null?"0":request.getParameter("park_id"));
		ParkingLotDAO parkingLotDAO = new ParkingLotDAO();
		ParkingLot parkingLot = parkingLotDAO.getParkingLotById(park_id);
		request.setAttribute("park",parkingLot);
		request.getRequestDispatcher("../Views/View_ParkingLot/ParkingLotUpdate.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		response.setContentType("application/json; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		ParkingLotDAO parkingLotDAO = new ParkingLotDAO();
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		String parkId = request.getParameter("park_id");
		String parkName = request.getParameter("park_name");
		String parkPlace = request.getParameter("park_place");
		String park_area= request.getParameter("park_area");
		String parkPrice = request.getParameter("park_price");
		String parkStatus = request.getParameter("park_status");
		String max_slot =  request.getParameter("max_park_slot");
		ParkingLot parkingLot = new ParkingLot(Integer.parseInt(request.getParameter("park_id")), Integer.parseInt(park_area),parkName , parkPlace, Float.parseFloat(parkPrice),Integer.parseInt(parkStatus),Integer.parseInt(max_slot));
		
		int updateFlag= parkingLotDAO.updateParkingLot(parkingLot);
		
		if(updateFlag==1) {
			serviceResponse.setSuccess(true);
			serviceResponse.setMessage("Update Parking Lot successfully !");
		} else {
			serviceResponse.setSuccess(false);
			serviceResponse.setMessage("Update Parking Lot failed !");
		}
		out.print(gson.toJson(serviceResponse));
			
		
		
	}

}
