package Controller.ParkingStaff.ParkingLot;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Time;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.ParkingLotDAO;
import Dao.TicketDAO;
import Entities.Car;
import Entities.ParkingLot;
import Entities.ServiceResponse;
import Entities.Ticket;
import Entities.Trip;

/**
 * Servlet implementation class AddParkingLot
 */
@WebServlet("/park/add")
public class AddParkingLot extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddParkingLot() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		request.getRequestDispatcher("../Views/View_ParkingLot/ParkingLotAdd.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	
		response.setContentType("application/json; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		ParkingLotDAO parkingLotDAO = new ParkingLotDAO();
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		String parkName = request.getParameter("park_name");
		String parkPlace = request.getParameter("park_place");
		String park_area= request.getParameter("park_area");
		String parkPrice = request.getParameter("park_price");
		String max_slot =  request.getParameter("max_park_slot");
		ParkingLot parkingLot = new ParkingLot(0, Integer.parseInt(park_area),parkName , parkPlace, Float.parseFloat(parkPrice),0,Integer.parseInt(max_slot));
		
		int flag = parkingLotDAO.addParkingLot(parkingLot);
		if(flag==1) {
			serviceResponse.setSuccess(true);
			serviceResponse.setMessage("Add Parking Lot successfully !");
		} else {
			serviceResponse.setSuccess(false);
			serviceResponse.setMessage("Add Parking Lot failed !");
		}
		out.print(gson.toJson(serviceResponse));
		
	}

}
