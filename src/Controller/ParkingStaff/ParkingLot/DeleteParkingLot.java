package Controller.ParkingStaff.ParkingLot;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.ParkingLotDAO;
import Dao.TicketDAO;
import Entities.ParkingLot;
import Entities.ServiceResponse;

/**
 * Servlet implementation class DeleteParkingLot
 */
@WebServlet("/park/delete")
public class DeleteParkingLot extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteParkingLot() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		out.print("Hello");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json; charset=UTF-8");

		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();

		int park_id = Integer.parseInt(request.getParameter("park_id") == null ? "0" : request.getParameter("park_id"));
		ParkingLotDAO parkingLotDAO = new ParkingLotDAO();

		int deleteFlag = parkingLotDAO.changeParkingLotStatus(park_id, 2);
		if (deleteFlag == 1) {
			serviceResponse.setSuccess(true);
			serviceResponse.setMessage("Delete Parking Lot successfully !");
		} else {
			serviceResponse.setSuccess(false);
			serviceResponse.setMessage("Delete Parking Lot failed !");
		}

		out.print(gson.toJson(serviceResponse));
	}

}
