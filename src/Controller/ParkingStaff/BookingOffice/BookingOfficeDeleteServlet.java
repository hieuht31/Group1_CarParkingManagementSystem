package Controller.ParkingStaff.BookingOffice;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.BookingOfficeDAO;
import Entities.BookingOffice;
import Entities.ServiceResponse;
import Entities.Trip;

/**
 * Servlet implementation class BookingOfficeDeleteServlet
 */
@WebServlet("/booking/delete")
public class BookingOfficeDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookingOfficeDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		BookingOfficeDAO bookDAO = new BookingOfficeDAO();
		
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		String office_id = request.getParameter("office_id");
		boolean flag = bookDAO.deleteBookingOffice(Integer.parseInt(office_id));
		if(flag) {
			serviceResponse.setSuccess(true);
			serviceResponse.setMessage("Add booking office successfully !");
		} else {
			serviceResponse.setSuccess(false);
			serviceResponse.setMessage("Add booking office failed !");
		}
		out.print(gson.toJson(serviceResponse));
	}

}
