package Controller.ParkingStaff.BookingOffice;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.BookingOfficeDAO;
import Dao.TripDAO;
import Entities.BookingOffice;
import Entities.ServiceResponse;
import Entities.Trip;

/**
 * Servlet implementation class BookingOfficeUpdateServlet
 */
@WebServlet("/booking/update")
public class BookingOfficeUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private List<Trip> listTrip;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookingOfficeUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    @Override
    public void init() throws ServletException {
    	// TODO Auto-generated method stub
    	super.init();
    	listTrip = new TripDAO().getAvailbleTrip(true);
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String office_id = request.getParameter("office_id")==null?null:request.getParameter("office_id");
		BookingOffice office = new BookingOfficeDAO().findById(Integer.parseInt(office_id==null?"0":office_id));
		if(office == null) {
			request.setAttribute("warningMessage", "Office id doesn't exist!");
			request.getRequestDispatcher("list").forward(request, response);	
		} else {
			request.setAttribute("listTrip", listTrip);
			request.setAttribute("office", office);
			request.getRequestDispatcher("../Views/View_BookingOffice/BookingOfficeUpdate.jsp").forward(request, response);	
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		BookingOfficeDAO bookDAO = new BookingOfficeDAO();
		
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		String office_id = request.getParameter("office_id");
		String office_name = request.getParameter("office_name");
		String trip = request.getParameter("trip");
		String phoneNumber = request.getParameter("phoneNumber");
		String place = request.getParameter("place");
		String office_price = request.getParameter("office_price");
		String fromDate = request.getParameter("fromDate");
		String toDate = request.getParameter("toDate");
		boolean flag = bookDAO.updateBookingOffice(new BookingOffice(Integer.parseInt(office_id), Date.valueOf(toDate), office_name, phoneNumber, place, Float.parseFloat(office_price), 
				Date.valueOf(fromDate), new Trip(Integer.parseInt(trip),0,null,null,null,null,null,0)));
		if(flag) {
			serviceResponse.setSuccess(true);
			serviceResponse.setMessage("Update booking office successfully !");
		} else {
			serviceResponse.setSuccess(false);
			serviceResponse.setMessage("Update booking office failed !");
		}
		out.print(gson.toJson(serviceResponse));
	}

}
