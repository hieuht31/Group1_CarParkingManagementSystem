package Controller.ParkingStaff.BookingOffice;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.BookingOfficeDAO;
import Dao.TicketDAO;
import Entities.FilterContent;
import Entities.ServiceResponse;

/**
 * Servlet implementation class BookingOfficeViewServlet
 */
@WebServlet({ "/booking/view", "/booking/list" })
public class BookingOfficeViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BookingOfficeViewServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("../Views/View_BookingOffice/BookingView.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		FilterContent filterContent = new FilterContent();
		BookingOfficeDAO bookDAO = new BookingOfficeDAO();
		try {
			filterContent.setPageNumber(Integer.parseInt(request.getParameter("pageNumber")));
			filterContent.setPageSize(Integer.parseInt(request.getParameter("pageSize")));
			filterContent.setFilterBy(request.getParameter("filterBy"));
			filterContent.setSearchValue(request.getParameter("searchValue"));

			serviceResponse.setData(bookDAO.findAll(filterContent));

			serviceResponse.setSuccess(true);

		} catch (Exception e) {
			serviceResponse.setSuccess(false);
			serviceResponse.setMessage(e.getMessage());
		}
		out.print(gson.toJson(serviceResponse));
	}

}
