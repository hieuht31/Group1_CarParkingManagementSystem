package Controller.HR.EmployeeManagement;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.DepartmentDAO;
import Dao.EmployeeDAO;
import Entities.Department;
import Entities.ServiceResponse;
import Utils.Validation;

/**
 * Servlet implementation class AddEmployee
 */
@WebServlet({ "/employee/add" })
public class AddEmployee extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddEmployee() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		DepartmentDAO DEPARTMENT_DAO = new DepartmentDAO();
		ArrayList<Department> departments = DEPARTMENT_DAO.getAllDepartment();
		request.setAttribute("departments", departments);
		request.getRequestDispatcher("../Views/View_EmployeeManagement/AddEmployee.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		// response.setContentType("text/html;charset=UTF-8");
		Validation VALIDATE = new Validation();
		EmployeeDAO EMPLOYEE_DAO = new EmployeeDAO();
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		boolean success = false;
		String message = "";
		ArrayList<String> errorMessages = new ArrayList<>();
		String fullname = request.getParameter("fullName");
		addToErrorList(errorMessages, VALIDATE.validString(fullname, 50, 1), "Length of fullname must be from 1-50. ");
		String phone = request.getParameter("phoneNumber");
		addToErrorList(errorMessages, VALIDATE.validPhone(phone), "PhoneNumber is invalid(Ex:0355928414). ");
		String dob = request.getParameter("dateOfBirth");
		addToErrorList(errorMessages, VALIDATE.validDate(dob), "Date of birth is invalid(Ex:2011-12-12). ");
		String sex = request.getParameter("sex");
		addToErrorList(errorMessages, VALIDATE.intergerNumber(sex, "sex"), "Sex is invalid(Male:0 - Female:1). ");
		String address = request.getParameter("address");
		if (address.length() > 0) {
			addToErrorList(errorMessages, VALIDATE.validString(address, 50, 1), "Length of address from 1 - 50. ");
		}
		String email = request.getParameter("email");
		addToErrorList(errorMessages, VALIDATE.validEmail(email), "Invalid email format(Ex:nguyet@gmail.com). ");
		String account = request.getParameter("account");
		addToErrorList(errorMessages, VALIDATE.validString(account, 50, 1), "Length of account must be from 1-50. ");
		String password = request.getParameter("password");
		addToErrorList(errorMessages, VALIDATE.validPassword(password),
				"Password must be contains at least 1UP, 1LW, 1Number(Ex:NguyetKTB2108). ");
		String department_id = request.getParameter("department");
		addToErrorList(errorMessages, VALIDATE.intergerNumber(request.getParameter("department"), "department"),
				"Department is invalid. ");
		if (EMPLOYEE_DAO.checkAccountExist(account) != null) {
			errorMessages.add("Account already exist !");
		}
		if (errorMessages.isEmpty()) {
			if (EMPLOYEE_DAO.addEmployee(fullname, phone, dob, Integer.parseInt(sex), address, email, account, password,
					Integer.parseInt(department_id))) {
				success = true;
				message = "Add employee successfully !";
			} else {
				success = false;
				message = "Add employee failed !";
			}
		} else {
			success = false;
			message = "Add employee failed !";
		}
		serviceResponse.setSuccess(success);
		serviceResponse.setMessage(message);
		serviceResponse.setData(errorMessages);
		out.print(gson.toJson(serviceResponse));
	}

	public void addToErrorList(ArrayList<String> errorMessages, boolean error, String message) {
		if (!error) {
			errorMessages.add(message);
		}
	}
}
