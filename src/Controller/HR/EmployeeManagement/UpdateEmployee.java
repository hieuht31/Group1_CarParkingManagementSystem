package Controller.HR.EmployeeManagement;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.DepartmentDAO;
import Dao.EmployeeDAO;
import Entities.Department;
import Entities.Employee;
import Entities.ServiceResponse;
import Utils.Validation;

/**
 * Servlet implementation class UpdateEmployee
 */
@WebServlet({ "/employee/update","/user/profile" })
public class UpdateEmployee extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateEmployee() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		DepartmentDAO DEPARTMENT_DAO = new DepartmentDAO();
		EmployeeDAO EMPLOYEE_DAO = new EmployeeDAO();
		Employee e = new Employee();
		ArrayList<Department> departments = new ArrayList<>();
		try {
			String employee_id = request.getParameter("employee_id");
			e = EMPLOYEE_DAO.getEmployeeById(employee_id);
			departments = DEPARTMENT_DAO.getAllDepartment();
		} catch (Exception ex) {
			System.err.println(ex);
		}
		request.setAttribute("departments", departments);
		request.setAttribute("employee", e);
		request.getRequestDispatcher("../Views/View_EmployeeManagement/UpdateEmployee.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json; charset=UTF-8");
		// response.setContentType("text/html;charset=UTF-8");
		EmployeeDAO EMPLOYEE_DAO = new EmployeeDAO();
		Validation VALIDATE = new Validation();
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		boolean success = false;
		String message = "";
		ArrayList<String> errorMessages = new ArrayList<>();
		String user_login_depart = request.getParameter("user_login_department");
		String employee_id = request.getParameter("employee_id");
		if (EMPLOYEE_DAO.getEmployeeById(employee_id) == null || request.getParameter("employee_id") == null) {
			addToErrorList(errorMessages, false, "Account is not exist !");
		}
		String fullname = request.getParameter("fullName");
		addToErrorList(errorMessages, VALIDATE.validString(fullname, 50, 1), "Length of fullname must be from 1-50. ");
		String phone = request.getParameter("phoneNumber");
		addToErrorList(errorMessages, VALIDATE.validPhone(phone), "PhoneNumber is invalid(Ex:0355928414). ");
		String dob = request.getParameter("dateOfBirth");
		addToErrorList(errorMessages, VALIDATE.validDate(dob), "Date of birth is invalid(Ex:2011-12-12). ");
		String sex = request.getParameter("sex");
		addToErrorList(errorMessages, VALIDATE.intergerNumber(sex, "sex"), "Sex is invalid(Male:0 - Female:1). ");
		String address = request.getParameter("address");
		if (address.length() > 0) {
			addToErrorList(errorMessages, VALIDATE.validString(address, 50, 1), "Length of address from 1 - 50. ");
		}
		String email = request.getParameter("email");
		addToErrorList(errorMessages, VALIDATE.validEmail(email), "Invalid email format(Ex:nguyet@gmail.com). ");
		String department_id = request.getParameter("department");
		addToErrorList(errorMessages, VALIDATE.intergerNumber(request.getParameter("department"), "department"),
				"Department is invalid. ");
		if (user_login_depart.equalsIgnoreCase("Admin")) {
			if (errorMessages.isEmpty()) {
				if (EMPLOYEE_DAO.updateEmployeeForAdmin(fullname, phone, dob, Integer.parseInt(sex), address, email,
						Integer.parseInt(department_id), Integer.parseInt(employee_id))) {
					success = true;
					message = "Update employee successfully !";
				} else {
					success = false;
					message = "Update employee failed !";
				}

			} else {
				success = false;
				message = "Update employee failed !";
			}
		} else {
			String account = request.getParameter("account");
			addToErrorList(errorMessages, VALIDATE.validString(account, 50, 1),
					"Length of account must be from 1-50. ");
			String password = request.getParameter("password");
			addToErrorList(errorMessages, VALIDATE.validPassword(password),
					"Password must be contains at least 1UP, 1LW, 1Number(Ex:NguyetKTB2108). ");
			if (errorMessages.isEmpty()) {
				if (EMPLOYEE_DAO.updateEmployeeForEmployee(fullname, phone, dob, Integer.parseInt(sex), address, email,
						account, password, Integer.parseInt(department_id), Integer.parseInt(employee_id))) {
					success = true;
					message = "Update employee successfully !";
				} else {
					success = false;
					message = "Update employee failed !";
				}

			} else {
				success = false;
				message = "Update employee failed !";
			}
		}

		serviceResponse.setSuccess(success);
		serviceResponse.setMessage(message);
		serviceResponse.setData(errorMessages);
		out.print(gson.toJson(serviceResponse));
	}

	public void addToErrorList(ArrayList<String> errorMessages, boolean error, String message) {
		if (!error) {
			errorMessages.add(message);
		}
	}

}
