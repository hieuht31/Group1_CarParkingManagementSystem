package Controller.HR.EmployeeManagement;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.EmployeeDAO;
import Entities.FilterContent;
import Entities.ServiceResponse;

/**
 * Servlet implementation class ListEmployee
 */
@WebServlet({ "/employee/list_employee" })
public class ListEmployee extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ListEmployee() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("../Views/View_EmployeeManagement/ListEmployee.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		EmployeeDAO EMPLOYEE_DAO = new EmployeeDAO();
		FilterContent filterContent = new FilterContent();
		try {
			filterContent.setPageNumber(Integer.parseInt(request.getParameter("pageNumber")));
			filterContent.setPageSize(Integer.parseInt(request.getParameter("pageSize")));
			filterContent.setFilterBy(request.getParameter("filterBy"));
			filterContent.setSearchValue(request.getParameter("searchValue"));
			// System.out.println(EMPLOYEE_DAO.getListEmployeeFilter(filterContent));
			serviceResponse.setSuccess(true);
			serviceResponse.setData(EMPLOYEE_DAO.getListEmployeeFilter(filterContent));
		} catch (Exception e) {
			serviceResponse.setSuccess(false);
			serviceResponse.setMessage(e.getMessage());
		}
		out.print(gson.toJson(serviceResponse));
	}

	
}
