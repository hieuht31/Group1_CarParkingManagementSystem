package Controller.HR.EmployeeManagement;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import Dao.EmployeeDAO;
import Entities.Employee;
import Entities.ServiceResponse;
import Utils.Validation;

/**
 * Servlet implementation class EmployeeService
 */
@WebServlet("/employee/employee_action")
public class EmployeeService extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EmployeeService() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// response.setContentType("text/html;charset=UTF-8");
		response.setContentType("application/json; charset=UTF-8");
		EmployeeDAO EMPLOYEE_DAO = new EmployeeDAO();
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		HttpSession session = request.getSession();
		String action = request.getParameter("action");
		if (action.equalsIgnoreCase("get-session-employee")) {
			Employee employee = (Employee) session.getAttribute("employee");
			if (employee != null) {
				serviceResponse.setData(employee);
				serviceResponse.setMessage("Get session employee dung roi nhe !");
				serviceResponse.setSuccess(true);
			}
		}
		if (action.equalsIgnoreCase("reset-password")) {
			int employee_Id = Integer.parseInt(request.getParameter("employee_id"));
			if(EMPLOYEE_DAO.resetPassword(employee_Id)) {
				serviceResponse.setMessage("Reset password thanh cong roi nhe !");
				serviceResponse.setSuccess(true);
			}else {
				serviceResponse.setMessage("Reset password khong duoc !");
				serviceResponse.setSuccess(false);
			}
		}
		out.print(gson.toJson(serviceResponse));

	}

}
