package Controller.HR.EmployeeManagement;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.EmployeeDAO;
import Entities.ServiceResponse;

/**
 * Servlet implementation class DeleteEmployee
 */
@WebServlet("/employee/delete")
public class DeleteEmployee extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteEmployee() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json; charset=UTF-8");
		// response.setContentType("text/html;charset=UTF-8");
		EmployeeDAO EMPLOYEE_DAO = new EmployeeDAO();
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			int employee_id = Integer.parseInt(request.getParameter("employee_id"));
			if (EMPLOYEE_DAO.deleteEmployeeById(employee_id)) {
				serviceResponse.setSuccess(true);
				serviceResponse.setMessage("Delete employee successfully !");
			} else {
				serviceResponse.setSuccess(false);
				serviceResponse.setMessage("Delete employee failed !");
			}
		} catch (Exception e) {
			System.err.println(e);
		}
		out.print(gson.toJson(serviceResponse));
	}

}
