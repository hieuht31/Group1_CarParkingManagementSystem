package Controller.ServiceStaff.TicketManagement;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.TicketDAO;
import Dao.TripDAO;
import Entities.FilterContent;
import Entities.ServiceResponse;

/**
 * Servlet implementation class ViewServlet
 */
@WebServlet({ "/ticket/view", "/ticket/list" })
public class ViewTicketServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ViewTicketServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("../Views/View_TicketManagement/TicketList.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		FilterContent filterContent = new FilterContent();
		TicketDAO ticketDAO = new TicketDAO();
		try {
			filterContent.setPageNumber(Integer.parseInt(request.getParameter("pageNumber")));
			filterContent.setPageSize(Integer.parseInt(request.getParameter("pageSize")));
			filterContent.setFilterBy(request.getParameter("filterBy"));
			filterContent.setSearchValue(request.getParameter("searchValue"));

			serviceResponse.setData(ticketDAO.findAll(filterContent));
			serviceResponse.setSuccess(true);

		} catch (Exception e) {
			serviceResponse.setSuccess(false);
			serviceResponse.setMessage(e.getMessage());
		}
		out.print(gson.toJson(serviceResponse));
	}

}
