package Controller.ServiceStaff.TicketManagement;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.TicketDAO;
import Entities.ServiceResponse;

/**
 * Servlet implementation class DeleteTicketServlet
 */
@WebServlet("/ticket/delete")
public class DeleteTicketServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteTicketServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		out.print("Hello");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
response.setContentType("application/json; charset=UTF-8");
		
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		
		int ticket_id = Integer.parseInt(request.getParameter("ticket_id")==null?"0":request.getParameter("ticket_id"));
		boolean deleteFlag = (new TicketDAO()).deleteTicket(ticket_id);
		if(deleteFlag) {
				serviceResponse.setSuccess(true);
				serviceResponse.setMessage("Delete trip successfully !");
			} else {
				serviceResponse.setSuccess(false);
				serviceResponse.setMessage("Delete trip failed !");
			}
		
		out.print(gson.toJson(serviceResponse));
	}

}
