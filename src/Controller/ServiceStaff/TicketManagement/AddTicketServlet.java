package Controller.ServiceStaff.TicketManagement;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.CarDAO;
import Dao.TicketDAO;
import Dao.TripDAO;
import Entities.Car;
import Entities.ServiceResponse;
import Entities.Ticket;
import Entities.Trip;

/**
 * Servlet implementation class AddTicket
 */
@WebServlet("/ticket/add")
public class AddTicketServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private List<Car> listCar;
	private List<Trip> listTrip;
	private TicketDAO ticketDAO;
	private CarDAO carDAO;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddTicketServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		ticketDAO = new TicketDAO();
		carDAO = new CarDAO();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		listCar = carDAO.getAllCar();
		listTrip = new TripDAO().getAvailbleTrip(false);
		request.setAttribute("listCar", listCar);
		request.setAttribute("listTrip", listTrip);
		request.getRequestDispatcher("../Views/View_TicketManagement/AddTicket.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		TicketDAO ticketDAO = new TicketDAO();

		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		String customer = request.getParameter("customer");
		String bookingTime = request.getParameter("bookingTime");
		String trip = request.getParameter("trip");
		String licensePlate = request.getParameter("licensePlate");
		boolean tripFlag = new TripDAO().checkAvailableTrip(trip);
		if (tripFlag) {
			boolean flag = ticketDAO.addTicket(new Ticket(0, Time.valueOf(bookingTime + ":00"), customer,
					new Car(Integer.parseInt(licensePlate), null, null, null, null, null),
					new Trip(Integer.parseInt(trip), 0, null, null, null, null, null, 0)));
			if (flag) {
				serviceResponse.setSuccess(true);
				serviceResponse.setMessage("Add ticket successfully !");
			} else {
				serviceResponse.setSuccess(false);
				serviceResponse.setMessage("Add ticket failed !");
			}
		} else {
			serviceResponse.setSuccess(false);
			serviceResponse.setMessage("Add ticket failed ! Trip have full");

		}
		out.print(gson.toJson(serviceResponse));
	}

}
