package Controller.ServiceStaff.TripManagement;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Time;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.TripDAO;
import Entities.ServiceResponse;
import Entities.Trip;

/**
 * Servlet implementation class TripUpdate
 */
@WebServlet("/trip/update")
public class TripUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TripUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int trip_id = request.getParameter("trip_id")==null?0:Integer.parseInt(request.getParameter("trip_id"));
		Trip trip = new TripDAO().findTripById(trip_id);
		if(trip==null) {
			request.setAttribute("warningMessage", "Oops, trip id doesn't exist!");
			request.getRequestDispatcher("view").forward(request, response);
		} else {
			request.setAttribute("trip", trip);
			request.getRequestDispatcher("../Views/View_TripManagement/UpdateTrip.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json; charset=UTF-8");
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		int trip_id = Integer.parseInt(request.getParameter("trip_id"));
		String car_type = request.getParameter("car_type");
		Date departure_date = Date.valueOf(request.getParameter("departure_date"));
		Time departure_time = Time.valueOf(request.getParameter("departure_time").length()>5?request.getParameter("departure_time"):request.getParameter("departure_time")+":00");
		String destination = request.getParameter("destination");
		String driver = request.getParameter("driver");
		int max_number = Integer.parseInt(request.getParameter("maximum_online_ticket_number"));
		TripDAO tripDAO = new TripDAO();
		boolean flag = tripDAO.update(new Trip(trip_id, 0, car_type, departure_date, departure_time, destination, driver, max_number));
		if(flag) {
			serviceResponse.setSuccess(true);
			serviceResponse.setMessage("Update employee successfully !");
		} else {
			serviceResponse.setSuccess(true);
			serviceResponse.setMessage("Update employee successfully !");
		}
		out.print(gson.toJson(serviceResponse));
	}

}
