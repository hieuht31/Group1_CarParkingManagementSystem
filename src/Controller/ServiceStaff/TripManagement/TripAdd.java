package Controller.ServiceStaff.TripManagement;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Time;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.EmployeeDAO;
import Dao.TripDAO;
import Entities.ServiceResponse;
import Entities.Trip;

/**
 * Servlet implementation class TripAdd
 */
@WebServlet("/trip/add")
public class TripAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TripAdd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("../Views/View_TripManagement/AddTrip.jsp").forward(request, response);;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		response.setContentType("application/json; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		TripDAO tripDAO = new TripDAO();
		
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		int trip_id = 0;
		String car_type = request.getParameter("car_type");
		Date departure_date = Date.valueOf(request.getParameter("departure_date"));
		Time departure_time = Time.valueOf(request.getParameter("departure_time")+":00");
		String destination = request.getParameter("destination");
		String driver = request.getParameter("driver");
		int max_number = Integer.parseInt(request.getParameter("maximum_online_ticket_number"));
		boolean flag = tripDAO.add(new Trip(trip_id, 0, car_type, departure_date, departure_time, destination, driver, max_number));
		if(flag) {
			serviceResponse.setSuccess(true);
			serviceResponse.setMessage("Add trip successfully !");
		} else {
			serviceResponse.setSuccess(false);
			serviceResponse.setMessage("Add trip failed !");
		}
		out.print(gson.toJson(serviceResponse));
	}

}
