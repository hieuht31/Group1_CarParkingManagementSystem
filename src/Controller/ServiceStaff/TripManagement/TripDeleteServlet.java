package Controller.ServiceStaff.TripManagement;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.TicketDAO;
import Dao.TripDAO;
import Entities.ServiceResponse;

/**
 * Servlet implementation class TripDeleteServlet
 */
@WebServlet("/trip/delete")
public class TripDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TripDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int trip_id = Integer.parseInt(request.getParameter("trip_id")==null?"0":request.getParameter("trip_id"));
		boolean deleteFlag = (new TripDAO()).delete(trip_id);
		if(deleteFlag) {
			request.setAttribute("successMessage", "Delete success");
		} else request.setAttribute("errorMessage", "Oops, Connect with database has some error!Delete fail");
		request.getRequestDispatcher("view?"+request.getQueryString()).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json; charset=UTF-8");
		
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		
		int trip_id = Integer.parseInt(request.getParameter("trip_id")==null?"0":request.getParameter("trip_id"));
		boolean deleteFlag = (new TripDAO()).delete(trip_id);
		
		if(deleteFlag){
				serviceResponse.setSuccess(true);
				serviceResponse.setMessage("Delete trip successfully !");
			} else {
				serviceResponse.setSuccess(false);
				serviceResponse.setMessage("Delete trip failed !");
			}
		
		out.print(gson.toJson(serviceResponse));
	}

}
