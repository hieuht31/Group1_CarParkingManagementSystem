package Controller.User;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import Dao.EmployeeDAO;
import Entities.Employee;
import Entities.ServiceResponse;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("/Views/View_UserManagement/Login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json; charset=UTF-8");
		// response.setContentType("text/html;charset=UTF-8");
		Gson gson = new Gson();
		PrintWriter out = response.getWriter();
		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			String account = request.getParameter("account");
			String password = request.getParameter("password");
			EmployeeDAO EMPLOYEE_DAO = new EmployeeDAO();
			Employee e = EMPLOYEE_DAO.getEmployeeByAccount(account, password);
			if (e != null) {
				request.getSession().setAttribute("employee", e);
				serviceResponse.setSuccess(true);
				serviceResponse.setMessage("success");
				serviceResponse.setData(e);
			} else {
				serviceResponse.setSuccess(false);
				serviceResponse.setMessage("fail");
			}
		} catch (Exception e) {
			System.err.println(e);
		}
		out.print(gson.toJson(serviceResponse));
	}

}
