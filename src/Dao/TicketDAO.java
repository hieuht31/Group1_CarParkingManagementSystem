package Dao;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Entities.Car;
import Entities.FilterContent;
import Entities.PagingModel;
import Entities.Ticket;
import Entities.Trip;
import Utils.DBConnection;

public class TicketDAO {
	Connection con = null;
	PreparedStatement pstate = null;
	ResultSet result = null;
	String sql = null;
	
	public PagingModel findAll(FilterContent filterContent){
		List<Ticket> listTicket = new ArrayList<Ticket>();
		PagingModel pagingModel = new PagingModel();
		con = new DBConnection().getConnection();
		Date dateFilter = filterContent.getFilterDate();
		String searchCondition = (filterContent.getSearch().equals("")?"":" AND "+filterContent.getFilterBy()+" LIKE '%'+?+'%' \r\n");
		if(!filterContent.getSearch().equals("")&&
				(filterContent.getFilterBy().equals("booking_time"))
				) {
			searchCondition = " AND "+filterContent.getFilterBy()+"=?\r\n";
		}
		sql="SELECT ti.*,car.license_plate,tr.destination,tr.departure_date, COUNT(*) OVER ()[record] \r\n"
				+ "FROM ticket ti INNER JOIN trip tr on ti.trip_id = tr.trip_id "
				+ "INNER JOIN car ON ti.car_id = car.car_id\r\n"
				+ "WHERE 1 = 1 "
				+ ((dateFilter==null?"":" AND departure_date >= ?\r\n"))
/**/		+ searchCondition	
				+ "ORDER BY departure_date ASC\r\n"
				+ "OFFSET ? ROW FETCH NEXT ? ROWS ONLY";
		int numberRecord = filterContent.getPageSize();
		int startRecord = (filterContent.getPageNumber()-1)*filterContent.getPageSize();
		int countTotalRecord = 0;
		try {
			pstate = con.prepareStatement(sql);
			int step = 1;
			if(dateFilter!=null) {
				pstate.setDate(step++, dateFilter);
			}
			
			if(!filterContent.getSearch().equals("")) {
				pstate.setString(step++, filterContent.getSearch());				
			}
			pstate.setInt(step++, startRecord);
			pstate.setInt(step++, numberRecord);
			result = pstate.executeQuery();
			
			if(result.next()) {
				listTicket.add(new Ticket(result.getInt(1),
						result.getTime(2),
						result.getString(3), 
						new Car(result.getInt(4),result.getString(6), null, null, null, null), 
						new Trip(result.getInt(5),0,null, 
								result.getDate(8), null, 
								result.getString(7), null, 0)));
				countTotalRecord+=result.getInt(9);
			}
			while(result.next()) {
				listTicket.add(new Ticket(result.getInt(1),
						result.getTime(2),
						result.getString(3), 
						new Car(result.getInt(4),result.getString(6), null, null, null, null), 
						new Trip(result.getInt(5),0,null, 
								result.getDate(8), null, 
								result.getString(7), null, 0)));
			}
			
			pagingModel.setPageNumber(filterContent.getPageNumber());
			pagingModel.setPageSize(filterContent.getPageSize());
			pagingModel.setTotalRecord(countTotalRecord);
			pagingModel.setData(listTicket);
			return pagingModel;
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			try {
				if(!result.isClosed()) {
					result.close();
				}
				if(!pstate.isClosed()) {
					pstate.close();
				}
				if(!con.isClosed()) {
					con.close();
				}
			} catch (Exception e2) {
				
			}
		}
		return null;
	}
	
	public Map<Integer, List<Ticket>> findAll(int currentPage) {
		int numberRecord = 3;
		int startRecord = (currentPage-1)*numberRecord;
		int countTotalRecord = 0;
		List<Ticket> listTicket = new ArrayList<>();
		con = new DBConnection().getConnection();
		sql = "SELECT ti.*,car.license_plate,tr.destination,tr.departure_date, COUNT(*) OVER ()[record] \r\n"
				+ "FROM ticket ti inner join trip tr on ti.trip_id = tr.trip_id inner join car ON ti.car_id = car.car_id WHERE departure_date>=''\r\n"
				+ "ORDER BY departure_date ASC\r\n"
				+ "OFFSET ? ROWS FETCH NEXT ? ROW ONLY";
		try {
			pstate = con.prepareStatement(sql);
			pstate.setInt(1, startRecord);
			pstate.setInt(2, numberRecord);
			
			result = pstate.executeQuery();
			if(result.next()) {
				listTicket.add(new Ticket(result.getInt(1),
						result.getTime(2),
						result.getString(3), 
						new Car(result.getInt(4),result.getString(6), null, null, null, null), 
						new Trip(result.getInt(5),0,null, 
								result.getDate(8), null, 
								result.getString(7), null, 0)));
				countTotalRecord+=result.getInt(9);
			}
			while(result.next()) {
				listTicket.add(new Ticket(result.getInt(1),
						result.getTime(2),
						result.getString(3), 
						new Car(result.getInt(4),result.getString(6), null, null, null, null), 
						new Trip(result.getInt(5),0,null, 
								result.getDate(8), null, 
								result.getString(7), null, 0)));
			}
			Map<Integer,List<Ticket>> map = new HashMap<>();
			map.put(countTotalRecord, listTicket);
			return map;
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			try {
				if(!result.isClosed()) {
					result.close();
				}
				if(!pstate.isClosed()) {
					pstate.close();
				}
				if(!con.isClosed()) {
					con.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		return null;
	}
	
	public Map<Integer, List<Ticket>> findTicket(Ticket ticket,int currentPage) {
		int numberRecord = 3;
		int startRecord = (currentPage-1)*numberRecord;
		int countTotalRecord = 0;
		List<Ticket> listTicket = new ArrayList<>();
		con = new DBConnection().getConnection();
		sql = "SELECT ti.*,car.license_plate,tr.destination,tr.departure_date, COUNT(*) OVER ()[record] \r\n"
				+ "FROM ticket ti inner join trip tr on ti.trip_id = tr.trip_id inner join car ON ti.car_id = car.car_id\r\n"
				+ "WHERE car.license_plate LIKE '%'+?+'%' AND customer_name LIKE '%'+?+'%' AND destination LIKE '%'+?+'%' AND CAST(departure_date AS VARCHAR) LIKE '%'+?+'%'\r\n"
				+ "ORDER BY departure_date ASC\r\n"
				+ "OFFSET ? ROWS FETCH NEXT ? ROW ONLY";
		try {
			pstate = con.prepareStatement(sql);
			pstate.setString(1, ticket.getCar().getLicense_plate());
			pstate.setString(2, ticket.getCustomer_name());
			pstate.setString(3, ticket.getTrip().getDestination());
			pstate.setString(4, ticket.getTrip().getDeparture_date()==null?"":ticket.getTrip().getDeparture_date().toString());
			pstate.setInt(5, startRecord);
			pstate.setInt(6, numberRecord);
			result = pstate.executeQuery();
			if(result.next()) {
				listTicket.add(new Ticket(result.getInt(1),
						result.getTime(2),
						result.getString(3), 
						new Car(result.getInt(4),result.getString(6), null, null, null, null), 
						new Trip(result.getInt(5),0,null, 
								result.getDate(8), null, 
								result.getString(7), null, 0)));
				countTotalRecord+=result.getInt(9);
			}
			while(result.next()) {
				listTicket.add(new Ticket(result.getInt(1),
						result.getTime(2),
						result.getString(3), 
						new Car(result.getInt(4),result.getString(6), null, null, null, null), 
						new Trip(result.getInt(5),0,null, 
								result.getDate(8), null, 
								result.getString(7), null, 0)));
			}
			Map<Integer,List<Ticket>> map = new HashMap<>();
			map.put(countTotalRecord, listTicket);
			return map;
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			try {
				if(!result.isClosed()) {
					result.close();
				}
				if(!pstate.isClosed()) {
					pstate.close();
				}
				if(!con.isClosed()) {
					con.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		return null;
	}
	
	public boolean addTicket(Ticket ticket) {
		con = new DBConnection().getConnection();
		sql = "INSERT INTO ticket(booking_time,customer_name,car_id,trip_id) VALUES(?,?,?,?)";
		try {
			pstate = con.prepareStatement(sql);
			pstate.setTime(1, ticket.getBooking_time());
			pstate.setString(2, ticket.getCustomer_name());
			pstate.setInt(3, ticket.getCar().getCar_id());
			pstate.setInt(4, ticket.getTrip().getTrip_id());
			pstate.executeUpdate();
			
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}finally {
			try {
				if(!pstate.isClosed()) {
					pstate.close();
				}
				if(!con.isClosed()) {
					con.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		return false;
	}
	public boolean updateTicket(Ticket ticket) {
		con = new DBConnection().getConnection();
		sql = "UPDATE ticket SET booking_time=?, customer_name=?,car_id=?,trip_id=? WHERE ticket_id =?";
		try {
			pstate = con.prepareStatement(sql);
			pstate.setTime(1, ticket.getBooking_time());
			pstate.setString(2, ticket.getCustomer_name());
			pstate.setInt(3, ticket.getCar().getCar_id());
			pstate.setInt(4, ticket.getTrip().getTrip_id());
			pstate.setInt(5, ticket.getTicket_id());
			pstate.executeUpdate();
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}finally {
			try {
				if(!pstate.isClosed()) {
					pstate.close();
				}
				if(!con.isClosed()) {
					con.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		return false;
	}
	
	public boolean deleteTicket(int ticket_id) {
		con = new DBConnection().getConnection();
		sql = "DELETE FROM ticket WHERE ticket_id=?";
		try {
			pstate = con.prepareStatement(sql);
			pstate.setInt(1, ticket_id);
			pstate.executeUpdate();
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}finally {
			try {
				if(!pstate.isClosed()) {
					pstate.close();
				}
				if(!con.isClosed()) {
					con.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		return false;
	}
	public Ticket findTicketById(int ticket_id) {
			con = new DBConnection().getConnection();
			sql = "SELECT ti.*,car.license_plate,tr.destination,tr.departure_date \r\n"
					+ "FROM ticket ti inner join trip tr on ti.trip_id = tr.trip_id \r\n"
					+ "inner join car ON ti.car_id = car.car_id WHERE ticket_id=?";
			try {
				pstate = con.prepareStatement(sql);
				pstate.setInt(1, ticket_id);
				result = pstate.executeQuery();
				if(result.next()) {
					return new Ticket(result.getInt(1),
							result.getTime(2),
							result.getString(3), 
							new Car(result.getInt(4),result.getString(6), null, null, null, null), 
							new Trip(result.getInt(5),0,null, 
									result.getDate(8), null, 
									result.getString(7), null, 0));	 
				}
			} catch (Exception e) {
				// TODO: handle exception
			}finally {
				try {
					if(!result.isClosed()) {
						result.close();
					}
					if(!pstate.isClosed()) {
						pstate.close();
					}
					if(!con.isClosed()) {
						con.close();
					}
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
			return null;
	}
}
