package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import Entities.Car;
import Entities.FilterContent;
import Entities.PagingModel;
import Entities.ParkingLot;
import Utils.DBConnection;


public class CarDAO {
	private Connection conn;
	private PreparedStatement ps;
	private ResultSet rs;
	
	public List<Car> listCars() {
		List<Car> listCars = new ArrayList<>();
		String sql = "SELECT * FROM car INNER JOIN parking_lot ON car.park_id = parking_lot.park_id";
		
		try {
			conn = new DBConnection().getConnection();
			
			ps = conn.prepareStatement(sql);
			
			rs = ps.executeQuery();
			
			while(rs.next()) {
				listCars.add(new Car(rs.getInt("car_id"), 
									rs.getString("license_plate"), 
									rs.getString("car_color"), 
									rs.getString("car_type"), 
									rs.getString("company"),
									new ParkingLot(rs.getInt("park_id"), 0, rs.getString("park_name"), null, null, 0,0)));
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			try {
				if(!ps.isClosed()) {
					ps.close();
				}
				if(!conn.isClosed()) {
					conn.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}

		}
		
		return listCars;
	}
	
	public PagingModel findAll(FilterContent filterContent) {
		List<Car> listCar = new ArrayList<>();
		PagingModel pagingModel = new PagingModel();
		conn = new DBConnection().getConnection();
		String searchCondition = (filterContent.getSearchValue().equals("") ? ""
				: " AND " + filterContent.getFilterBy() + " LIKE '%' + ? + '%' \r\n");

		String sql = "  SELECT *, COUNT(*) OVER()[number_record] FROM car INNER JOIN parking_lot ON car.park_id = parking_lot.park_id \r\n"
				+ searchCondition 
				+ "ORDER BY car.car_id\r\n" + "OFFSET ? ROW FETCH NEXT ? ROWS ONLY";
		int numberRecord = filterContent.getPageSize();
		int startRecord = (filterContent.getPageNumber() - 1) * filterContent.getPageSize();
		int countTotalRecord = 0;

		try {
			ps = conn.prepareStatement(sql);
			int step = 1;
			if (!filterContent.getSearchValue().equals("")) {
				ps.setString(step++, filterContent.getSearchValue());
			}
			ps.setInt(step++, startRecord);
			ps.setInt(step++, numberRecord);
			rs = ps.executeQuery();

			if (rs.next()) {
				countTotalRecord = rs.getInt("number_record");
				listCar.add(new Car(rs.getInt("car_id"), 
						rs.getString("license_plate"), 
						rs.getString("car_color"),
						rs.getString("car_type"), 
						rs.getString("company"),
						new ParkingLot(rs.getInt("park_id"), 0, rs.getString("park_name"), null, null, 0,0)));
			}
			while (rs.next()) {
				listCar.add(new Car(rs.getInt("car_id"), 
						rs.getString("license_plate"), 
						rs.getString("car_color"),
						rs.getString("car_type"), 
						rs.getString("company"),
						new ParkingLot(rs.getInt("park_id"), 0, rs.getString("park_name"), null, null, 0 ,0)));
				
			}
			pagingModel.setPageNumber(filterContent.getPageNumber());
			pagingModel.setPageSize(filterContent.getPageSize());
			pagingModel.setTotalRecord(countTotalRecord);
			pagingModel.setData(listCar);
			return pagingModel;
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			try {
				if (!rs.isClosed()) {
					rs.close();
				}
				if (!ps.isClosed()) {
					ps.close();
				}
				if (!conn.isClosed()) {
					conn.close();
				}
			} catch (Exception e2) {

			}
		}

		return null;

	}
	
	public Car findCarById(int car_id) {
		String sql = "SELECT * FROM car INNER JOIN parking_lot ON car.park_id = parking_lot.park_id WHERE car_id = ?";
		
		try {
			conn = new DBConnection().getConnection();
			
			ps = conn.prepareStatement(sql);
			
			ps.setInt(1, car_id);
			
			rs = ps.executeQuery();
			
			if(rs.next()) {
				return new Car(rs.getInt("car_id"), 
						rs.getString("license_plate"), 
						rs.getString("car_color"),
						rs.getString("car_type"), 
						rs.getString("company"), 
						new ParkingLot(rs.getInt("park_id"), 0, rs.getString("park_name"), null, null, 0,0)); 
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			try {
				if(!ps.isClosed()) {
					ps.close();
				}
				if(!conn.isClosed()) {
					conn.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}

		}
		
		return null;
		
	}
	
	
	
	public boolean addCar(Car car) {
		String sql = "INSERT INTO car(license_plate, car_color, car_type, company, park_id) VALUES (?,?,?,?,?)";
		try {
			conn = new DBConnection().getConnection();
			
			ps = conn.prepareStatement(sql);
			
			ps.setString(1, car.getLicense_plate());
			ps.setString(2, car.getCar_color());
			ps.setString(3, car.getCar_type());
			ps.setString(4, car.getCompany());
			ps.setInt(5, car.getParkingLot().getPark_id());
			
			ps.executeUpdate();
			
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if(!ps.isClosed()) {
					ps.close();
				}
				if(!conn.isClosed()) {
					conn.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}

		}
		return false;
	}
	
	public boolean updateCar(Car car) {
		String sql = "UPDATE car SET license_plate =?, car_color =?, car_type =?, company =?, park_id =? WHERE car_id =?";
		try {
			conn = new DBConnection().getConnection();
			
			ps = conn.prepareStatement(sql);
			
			ps.setString(1, car.getLicense_plate());
			ps.setString(2, car.getCar_color());
			ps.setString(3, car.getCar_type());
			ps.setString(4, car.getCompany());
			ps.setInt(5, car.getParkingLot().getPark_id());
			ps.setInt(6, car.getCar_id());
			
			ps.executeUpdate();
			
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if(!ps.isClosed()) {
					ps.close();
				}
				if(!conn.isClosed()) {
					conn.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}

		}
		return false;
	}
	
	public boolean deleteCar(int car_id) {
		String sql = "DELETE FROM car WHERE car_id =?";
		
		try {
			conn = new DBConnection().getConnection();
			
			ps = conn.prepareStatement(sql);
			
			ps.setInt(1, car_id);
			
			ps.executeUpdate();
			
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if(!ps.isClosed()) {
					ps.close();
				}
				if(!conn.isClosed()) {
					conn.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}

		}
		return false;
	}
		
	public boolean checkLicense(String license_plate) {
		conn = new DBConnection().getConnection();
		String sql = "SELECT * FROM car WHERE license_plate = ?";
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, license_plate);
			rs = ps.executeQuery();
			if(rs.next()) {
				return true;
			}
		} catch (Exception e) {

		}finally {
			try {
				if(!rs.isClosed()) {
					rs.close();
				}
				if(!ps.isClosed()) {
					ps.close();
				}
				if(!conn.isClosed()) {
					conn.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		
		return false;
	}
	
	public List<Car> getAllCar(){
		List<Car> listCar = new ArrayList<>();
		conn = new DBConnection().getConnection();
		String sql = "SELECT * FROM car";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
				listCar.add(new Car(rs.getInt(1), rs.getString(2), null, null, null, null));
			}
		} catch (Exception e) {

		}finally {
			try {
				if(!rs.isClosed()) {
					rs.close();
				}
				if(!ps.isClosed()) {
					ps.close();
				}
				if(!conn.isClosed()) {
					conn.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		
		return listCar;
	}

}
