package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import Entities.BookingOffice;
import Entities.FilterContent;
import Entities.PagingModel;
import Entities.Trip;
import Utils.DBConnection;

public class BookingOfficeDAO {
	private Connection conn;
	private PreparedStatement ps;
	private ResultSet rs;
	
	public PagingModel findAll(FilterContent filterContent){
		PagingModel pagingModel = new PagingModel();
		conn = new DBConnection().getConnection();
		List<BookingOffice> list = new ArrayList<>();
		
		String sql="SELECT bo.*, t.destination,COUNT(*) OVER()[count_record] "
				+ "FROM booking_office bo INNER JOIN trip t ON bo.trip_id = t.trip_id \r\n"
				+ "WHERE "+filterContent.getFilterBy()+" LIKE '%'+?+'%'\r\n"
				+ "ORDER BY office_id ASC\r\n" 
				+ "OFFSET ? ROW FETCH NEXT ? ROWS ONLY";
		int numberRecord = filterContent.getPageSize();
		int startRecord = (filterContent.getPageNumber()-1)*filterContent.getPageSize();
		int countTotalRecord = 0;
		
		try {
			ps = conn.prepareStatement(sql);
			int step = 1;
			
			ps.setString(step++, filterContent.getSearch());				
			ps.setInt(step++, startRecord);
			ps.setInt(step++, numberRecord);
			rs = ps.executeQuery();
			
			if(rs.next()) {
				countTotalRecord = rs.getInt("count_record");
				list.add(new BookingOffice(rs.getInt(1), rs.getDate(2), rs.getString(3), rs.getString(4), rs.getString(5), 
						rs.getFloat(6), rs.getDate(7), 
						new Trip(rs.getInt(8),0,null,null,null,rs.getString(9),null,0)));
			}
			while(rs.next()) {
				list.add(new BookingOffice(rs.getInt(1), rs.getDate(2), rs.getString(3), rs.getString(4), rs.getString(5), 
						Float.parseFloat(rs.getInt(6)+""), rs.getDate(7), 
						new Trip(rs.getInt(8),0,null,null,null,rs.getString(9),null,0)));
			}
			
			pagingModel.setPageNumber(filterContent.getPageNumber());
			pagingModel.setPageSize(filterContent.getPageSize());
			pagingModel.setTotalRecord(countTotalRecord);
			pagingModel.setData(list);
			return pagingModel;
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			try {
				if(!rs.isClosed()) {
					rs.close();
				}
				if(!ps.isClosed()) {
					ps.close();
				}
				if(!conn.isClosed()) {
					conn.close();
				}
			} catch (Exception e2) {
				
			}
		}
		return null;
	}
	
	public BookingOffice findById(int office_id) {
		conn = new DBConnection().getConnection();
		
		String sql="SELECT bo.*, t.destination "
				+ "FROM booking_office bo INNER JOIN trip t ON bo.trip_id = t.trip_id \r\n"
				+ "WHERE  office_id = ?\r\n";
		try {
			ps = conn.prepareStatement(sql);
			
			ps.setInt(1, office_id);			
			rs = ps.executeQuery();
			if(rs.next()) {
				return new BookingOffice(rs.getInt(1), rs.getDate(2), rs.getString(3), rs.getString(4), rs.getString(5), 
						rs.getFloat(6), rs.getDate(7), 
						new Trip(rs.getInt(8),0,null,null,null,rs.getString(9),null,0));
			}
			
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			try {
				if(!rs.isClosed()) {
					rs.close();
				}
				if(!ps.isClosed()) {
					ps.close();
				}
				if(!conn.isClosed()) {
					conn.close();
				}
			} catch (Exception e2) {
				
			}
		}
		return null;
	}

	public boolean addBookingOffice(BookingOffice bookingOffice) {
		String sql = "INSERT INTO booking_office( end_contract_deadline, office_name, office_phone, office_place, office_price, start_contract_deadline, trip_id) VALUES (?,?,?,?,?,?,?)";
		try {
			conn = new DBConnection().getConnection();
			ps = conn.prepareStatement(sql);
			ps.setDate(1, bookingOffice.getEnd_contract_deadline());
			ps.setString(2, bookingOffice.getOffice_name());
			ps.setString(3, bookingOffice.getOffice_phone());
			ps.setString(4, bookingOffice.getOffice_place());
			ps.setFloat(5, bookingOffice.getOffice_price());
			ps.setDate(6, bookingOffice.getStart_contract_deadline());
			ps.setInt(7, bookingOffice.getTrip().getTrip_id());

			ps.executeUpdate();

			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (!ps.isClosed()) {
					ps.close();
				}
				if (!conn.isClosed()) {
					conn.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}

		}
		return false;
	}
	
	public boolean updateBookingOffice(BookingOffice bookingOffice) {
		String sql = "UPDATE [dbo].[booking_office]\r\n"
				+ "   SET [end_contract_deadline] = ?\r\n"
				+ "      ,[office_name] = ?\r\n"
				+ "      ,[office_phone] = ?\r\n"
				+ "      ,[office_place] = ?\r\n"
				+ "      ,[office_price] = ?\r\n"
				+ "      ,[start_contract_deadline] = ?\r\n"
				+ "      ,[trip_id] = ?\r\n"
				+ " WHERE office_id = ? ";
		
		try {
			conn = new DBConnection().getConnection();
			ps = conn.prepareStatement(sql);
			ps.setDate(1, bookingOffice.getEnd_contract_deadline());
			ps.setString(2, bookingOffice.getOffice_name());
			ps.setString(3, bookingOffice.getOffice_phone());
			ps.setString(4, bookingOffice.getOffice_place());
			ps.setFloat(5, bookingOffice.getOffice_price());
			ps.setDate(6, bookingOffice.getStart_contract_deadline());
			ps.setInt(7, bookingOffice.getTrip().getTrip_id());
			ps.setInt(8, bookingOffice.getOffice_id());

			ps.executeUpdate();

			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (!ps.isClosed()) {
					ps.close();
				}
				if (!conn.isClosed()) {
					conn.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}

		}
		return false;
	}
	
	public boolean deleteBookingOffice(int office_id) {
		String sql = "DELETE FROM [dbo].[booking_office]\r\n"
				+ "      WHERE office_id = ?\r\n";
		try {
			conn = new DBConnection().getConnection();
			ps = conn.prepareStatement(sql);
			ps.setInt(1, office_id);
			ps.executeUpdate();

			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (!ps.isClosed()) {
					ps.close();
				}
				if (!conn.isClosed()) {
					conn.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}

		}
		return false;
	}
	
}
