package Dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import Entities.Department;
import Entities.Employee;
import Entities.FilterContent;
import Entities.PagingModel;
import Utils.DBConnection;

public class EmployeeDAO extends DBConnection {
	public Employee getEmployeeByAccount(String account, String password) {
		try {
			String sql = "SELECT e.employee_id , e.account , e.employee_address , e.employee_birthdate , e.employee_email , e.employee_name, \r\n"
					+ "  e.employee_phone , e.password , e.sex ,d.department_id , d.department AS department_name\r\n"
					+ " FROM employee e, department d \r\n"
					+ "  WHERE e.department_id = d.department_id AND e.account = ? AND e.password = ?\r\n" + "";
			PreparedStatement stm = getConnection().prepareStatement(sql);
			stm.setString(1, account);
			stm.setString(2, password);
			ResultSet rs = stm.executeQuery();
			while (rs.next()) {
				Department d = new Department(rs.getInt("department_id"), rs.getString("department_name"));
				boolean sex = (rs.getInt("sex") == 1) ? true : false;
				return new Employee(rs.getInt("employee_id"), rs.getString("account"), rs.getString("employee_address"),
						rs.getDate("employee_birthdate"), rs.getString("employee_email"), rs.getString("employee_name"),
						rs.getString("employee_phone"), rs.getString("password"), sex, d);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	public Employee checkAccountExist(String account) {
		try {
			String sql = "  SELECT e.employee_id, e.account,e.employee_address, e.employee_birthdate,e.employee_email, e.employee_name, e.employee_phone, e.password , e.sex, d.department_id , d.department AS department_name"
					+ "    FROM employee e , department d "
					+ "    WHERE e.department_id = d.department_id AND e.account = ?  ";
			PreparedStatement stm = getConnection().prepareStatement(sql);
			stm.setString(1, account);
			ResultSet rs = stm.executeQuery();
			while (rs.next()) {
				Department d = new Department(rs.getInt("department_id"), rs.getString("department_name"));
				boolean sex = (rs.getInt("sex") == 1) ? true : false;
				return new Employee(rs.getInt("employee_id"), rs.getString("account"), rs.getString("employee_address"),
						rs.getDate("employee_birthdate"), rs.getString("employee_email"), rs.getString("employee_name"),
						rs.getString("employee_phone"), rs.getString("password"), sex, d);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	public Employee getEmployeeById(String employee_id) {
		try {
			String sql = "  SELECT e.employee_id, e.account,e.employee_address, e.employee_birthdate,e.employee_email, e.employee_name, e.employee_phone, e.password , e.sex, d.department_id , d.department AS department_name"
					+ "    FROM employee e , department d "
					+ "    WHERE e.department_id = d.department_id AND e.employee_id = ? ";
			PreparedStatement stm = getConnection().prepareStatement(sql);
			stm.setString(1, employee_id);
			ResultSet rs = stm.executeQuery();
			while (rs.next()) {
				Department d = new Department(rs.getInt("department_id"), rs.getString("department_name"));
				boolean sex = (rs.getInt("sex") == 1) ? true : false;
				return new Employee(rs.getInt("employee_id"), rs.getString("account"), rs.getString("employee_address"),
						rs.getDate("employee_birthdate"), rs.getString("employee_email"), rs.getString("employee_name"),
						rs.getString("employee_phone"), rs.getString("password"), sex, d);
			}
		} catch (Exception e) {
			System.err.println(e);
		}
		return null;
	}

	public boolean addEmployee(String fullname, String phone, String date, int sex, String address, String email,
			String account, String password, int department) {
		try {
			String sql = "INSERT INTO employee (account, department_id, employee_address, employee_birthdate, employee_email, employee_name, employee_phone, password, sex) "
					+ "  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement stm = getConnection().prepareStatement(sql);
			stm.setString(1, account);
			stm.setInt(2, department);
			stm.setString(3, address);
			stm.setString(4, date);
			stm.setString(5, email);
			stm.setString(6, fullname);
			stm.setString(7, phone);
			stm.setString(8, password);
			stm.setInt(9, sex);
			return (stm.executeUpdate() >= 1) ? true : false;
		} catch (Exception e) {
			System.err.println(e);
		}
		return false;
	}

	public boolean updateEmployeeForEmployee(String fullname, String phone, String date, int sex, String address,
			String email, String account, String password, int department, int employeeId) {
		try {
			String sql = "UPDATE employee " + "SET account = ? " + "   ,department_id = ? "
					+ "   ,employee_address = ? " + "   ,employee_birthdate = ? " + "   ,employee_email = ? "
					+ "   ,employee_name = ? " + "   ,employee_phone = ? " + "   ,password = ? " + "   ,sex = ? "
					+ "WHERE employee_id = ? ";
			PreparedStatement stm = getConnection().prepareStatement(sql);
			stm.setString(1, account);
			stm.setInt(2, department);
			stm.setString(3, address);
			stm.setString(4, date);
			stm.setString(5, email);
			stm.setString(6, fullname);
			stm.setString(7, phone);
			stm.setString(8, password);
			stm.setInt(9, sex);
			stm.setInt(10, employeeId);
			return (stm.executeUpdate() >= 1) ? true : false;
		} catch (Exception e) {
			System.err.println(e);
		}
		return false;
	}

	public boolean updateEmployeeForAdmin(String fullname, String phone, String date, int sex, String address,
			String email, int department, int employeeId) {
		try {
			String sql = "UPDATE employee " + "SET department_id = ? " + "   ,employee_address = ? "
					+ "   ,employee_birthdate = ? " + "   ,employee_email = ? " + "   ,employee_name = ? "
					+ "   ,employee_phone = ? " + "   ,sex = ? " + "WHERE employee_id = ? ";
			PreparedStatement stm = getConnection().prepareStatement(sql);
			stm.setInt(1, department);
			stm.setString(2, address);
			stm.setString(3, date);
			stm.setString(4, email);
			stm.setString(5, fullname);
			stm.setString(6, phone);
			stm.setInt(7, sex);
			stm.setInt(8, employeeId);
			return (stm.executeUpdate() >= 1) ? true : false;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return false;
	}

	public PagingModel getListEmployeeFilter(FilterContent filterContent) {
		ArrayList<Employee> employees = new ArrayList<>();
		PagingModel pagingModel = new PagingModel();
		try {
			String condition = "e." + filterContent.getFilterBy() + " LIKE '%" + filterContent.getSearchValue() + "%' ";
			if (filterContent.getFilterBy().equalsIgnoreCase("employee_birthdate")) {
				condition = "e." + filterContent.getFilterBy() + " = '" + filterContent.getSearchValue() + "'";
			} else if (filterContent.getFilterBy().equalsIgnoreCase("department")) {
				condition = "d.department " + " LIKE '%" + filterContent.getSearchValue() + "%' ";
			}
			String sql = "SELECT e.employee_id, e.account,e.employee_address, e.employee_birthdate,e.employee_email, e.employee_name, e.employee_phone, e.password , e.sex, d.department_id , d.department AS department_name, COUNT(e.employee_id) OVER() AS TotalRecord \r\n"
					+ "	FROM employee e , department d " + "	WHERE e.department_id = d.department_id AND "
					+ condition + "	ORDER BY e.employee_id OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
			PreparedStatement stm = getConnection().prepareStatement(sql);
			stm.setInt(1, (filterContent.getPageNumber() - 1) * filterContent.getPageSize());
			stm.setInt(2, filterContent.getPageSize());
			ResultSet rs = stm.executeQuery();
			int totalRecord = 0;
			while (rs.next()) {
				totalRecord = rs.getInt("TotalRecord");
				Department d = new Department(rs.getInt("department_id"), rs.getString("department_name"));
				boolean sex = (rs.getInt("sex") == 1) ? true : false;
				employees.add(new Employee(rs.getInt("employee_id"), rs.getString("account"),
						rs.getString("employee_address"), rs.getDate("employee_birthdate"),
						rs.getString("employee_email"), rs.getString("employee_name"), rs.getString("employee_phone"),
						rs.getString("password"), sex, d));

			}
			pagingModel.setPageNumber(filterContent.getPageNumber());
			pagingModel.setPageSize(filterContent.getPageSize());
			pagingModel.setData(employees);
			pagingModel.setTotalRecord(totalRecord);
		} catch (Exception e) {
			System.err.println(e);
		}

		return pagingModel;
	}

	public boolean deleteEmployeeById(int employee_id) {
		try {
			String sql = "DELETE employee " + "WHERE employee_id = ? ";
			PreparedStatement stm = getConnection().prepareStatement(sql);
			stm.setInt(1, employee_id);
			return (stm.executeUpdate() >= 1) ? true : false;
		} catch (Exception e) {
			System.err.println(e);
		}
		return false;
	}
	
	public boolean resetPassword(int employee_id) {
		try {
			String sql = "UPDATE employee \r\n" + 
					"SET password = '123456'\r\n" + 
					"WHERE employee_id = ?  ";
			PreparedStatement stm = getConnection().prepareStatement(sql);
			stm.setInt(1, employee_id);
			return (stm.executeUpdate() >= 1) ? true : false;
			} catch (Exception e) {
			System.err.println(e);
		}
		return false;
	}

	public static void main(String[] args) {
		EmployeeDAO d = new EmployeeDAO();
		FilterContent f = new FilterContent(1, 10, "A", "department");
		System.out.println(d.resetPassword(6));
	}

}
