package Dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import Entities.Department;
import Entities.Employee;

/**
 * This class inherits all the methods from BaseDAO
 */
public class UserDAO extends BaseDAO<Employee> {

    public Employee getEmployee(String username, String password) {
        String sql = "  SELECT e.employee_id, e.account,e.employee_address, e.employee_birthdate,e.employee_email, e.employee_name, e.employee_phone, e.password , e.sex, d.department_id , d.deparment AS department_name"
                + "    FROM employee e , department d "
                + "    WHERE e.department_id = d.department_id AND e.employee_email = '" + username
                + "' AND e.password = '" + password + "'";
        return executeQuerySingle(sql);
    }

    @Override
    public Employee getObject(ResultSet rs) {
        try {
            Department d = new Department(rs.getInt("department_id"), rs.getString("department_name"));
            boolean sex = (rs.getInt("sex") == 1) ? true : false;
            return new Employee(rs.getInt("employee_id"), rs.getString("account"), rs.getString("employee_address"),
                    rs.getDate("employee_birthdate"), rs.getString("employee_email"),
                    rs.getString("employee_name"), rs.getString("employee_phone"), rs.getString("password"), sex,
                    d);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
}
