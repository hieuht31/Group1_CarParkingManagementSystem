package Dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Entities.FilterContent;
import Entities.PagingModel;
import Entities.Trip;
import Utils.DBConnection;

public class TripDAO {
	Connection con = null;
	PreparedStatement pstate = null;
	ResultSet result = null;
	String sql = null;
	
	/**
	 * Get available trip to add (get from current date) and update (get all exist trip) in data base
	 * */
	public Trip findTripById(int trip_id) {
		con = new DBConnection().getConnection();
		sql="SELECT * FROM Trip WHERE trip_id = ?";
		try {
			pstate = con.prepareStatement(sql);
			pstate.setInt(1, trip_id); 
			result = pstate.executeQuery();			
			if(result.next()) {
				return new Trip(result.getInt(1), result.getInt(2), result.getString(3), result.getDate(4), result.getTime(5), result.getString(6),result.getString(7),result.getInt(8));
			}
		} catch (Exception e) {

		} finally {
			try {
				if(!result.isClosed()) {
					result.close();
				}
				if(!pstate.isClosed()) {
					pstate.close();
				}
				if(!con.isClosed()) {
					con.close();
				}
			} catch (Exception e2) {
				
			}
		}
		return null;
	}
	/**
	 * Get available trip (which has departure date >= current date)
	 * */
	public List<Trip> getAvailbleTrip(boolean isGetAll){
		List<Trip> listTrip = new ArrayList<Trip>();
		con = new DBConnection().getConnection();
		sql="SELECT *, COUNT(*) OVER()[number_record] FROM trip WHERE \r\n"
				+ (isGetAll?" ":"departure_date>= GETDATE() AND \r\n")
				+ "booked_ticket_number < maximum_online_ticket_number \r\n"
				+ "ORDER BY departure_date ASC\r\n";
		try {
			pstate = con.prepareStatement(sql);
			result = pstate.executeQuery();
			
			while(result.next()) {
				listTrip.add(new Trip(result.getInt(1), result.getInt(2), result.getString(3), result.getDate(4), result.getTime(5), result.getString(6),result.getString(7),result.getInt(8)));
			}
			return listTrip;
		} catch (Exception e) {

		} finally {
			try {
				if(!result.isClosed()) {
					result.close();
				}
				if(!pstate.isClosed()) {
					pstate.close();
				}
				if(!con.isClosed()) {
					con.close();
				}
			} catch (Exception e2) {
				
			}
		}
		return null;
	}
	/**
	 * Get list trip with paging by search filter value
	 * */
	public PagingModel findAll(FilterContent filterContent){
		List<Trip> listTrip = new ArrayList<Trip>();
		PagingModel pagingModel = new PagingModel();
		con = new DBConnection().getConnection();
		Date dateFilter = filterContent.getFilterDate();
		String searchCondition = (filterContent.getSearch().equals("")?"":" AND "+filterContent.getFilterBy()+" LIKE '%'+?+'%' \r\n");
		if(!filterContent.getSearch().equals("")&&
				(filterContent.getFilterBy().equals("department_date")||
						filterContent.getFilterBy().equals("department_time")||
						filterContent.getFilterBy().equals("booked_ticket_number"))
				) {
			searchCondition = " AND "+filterContent.getFilterBy()+"=?\r\n";
		}
		sql="SELECT *, COUNT(*) OVER()[number_record] FROM trip "
				+ "WHERE 1 = 1 "
				+ ((dateFilter==null?"":" AND departure_date >= ?\r\n"))
/**/		+ searchCondition	
				+ "ORDER BY departure_date ASC\r\n"
				+ "OFFSET ? ROW FETCH NEXT ? ROWS ONLY";
		int numberRecord = filterContent.getPageSize();
		int startRecord = (filterContent.getPageNumber()-1)*filterContent.getPageSize();
		int countTotalRecord = 0;
		try {
			pstate = con.prepareStatement(sql);
			int step = 1;
			if(dateFilter!=null) {
				pstate.setDate(step++, dateFilter);
			}
			
			if(!filterContent.getSearch().equals("")) {
				pstate.setString(step++, filterContent.getSearch());				
			}
			pstate.setInt(step++, startRecord);
			pstate.setInt(step++, numberRecord);
			result = pstate.executeQuery();
			
			if(result.next()) {
				countTotalRecord = result.getInt(9);
				listTrip.add(new Trip(result.getInt(1), result.getInt(2), result.getString(3), result.getDate(4), result.getTime(5), result.getString(6),result.getString(7),result.getInt(8)));
			}
			while(result.next()) {
				listTrip.add(new Trip(result.getInt(1), result.getInt(2), result.getString(3), result.getDate(4), result.getTime(5), result.getString(6),result.getString(7),result.getInt(8)));
			}
			
			pagingModel.setPageNumber(filterContent.getPageNumber());
			pagingModel.setPageSize(filterContent.getPageSize());
			pagingModel.setTotalRecord(countTotalRecord);
			pagingModel.setData(listTrip);
			return pagingModel;
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			try {
				if(!result.isClosed()) {
					result.close();
				}
				if(!pstate.isClosed()) {
					pstate.close();
				}
				if(!con.isClosed()) {
					con.close();
				}
			} catch (Exception e2) {
				
			}
		}
		return null;
	}
	
	
	/**
	 * Add trip
	 * */
	public boolean add(Trip trip) {
		con = new DBConnection().getConnection();
		sql="INSERT INTO trip VALUES(0,?,?,?,?,?,?)";
		try {
			pstate = con.prepareStatement(sql);
			pstate.setString(1, trip.getCar_type());
			pstate.setDate(2, trip.getDeparture_date());
			pstate.setTime(3, trip.getDeparture_time());
			pstate.setString(4, trip.getDestination());
			pstate.setString(5,trip.getDriver());
			pstate.setInt(6, trip.getMaximum_online_ticket_number());
			
			pstate.executeUpdate();
			
			return true;
		} catch (Exception e) {

		} finally {
			try {
				if(!result.isClosed()) {
					result.close();
				}
				if(!pstate.isClosed()) {
					pstate.close();
				}
				if(!con.isClosed()) {
					con.close();
				}
			} catch (Exception e2) {
				
			}
		}
		return false;
	}
	/**
	 * Update trip
	 * */
	public boolean update(Trip trip) {
		con = new DBConnection().getConnection();
		sql="      UPDATE trip SET car_type = ?, "
				+ "departure_date = ?, "
				+ "departure_time = ?, "
				+ "destination = ?, driver = ?,"
				+ "maximum_online_ticket_number = ? "
				+ "WHERE trip_id = ?";
		try {
			pstate = con.prepareStatement(sql);
			pstate.setString(1, trip.getCar_type());
			pstate.setDate(2, trip.getDeparture_date());
			pstate.setTime(3, trip.getDeparture_time());
			pstate.setString(4, trip.getDestination());
			pstate.setString(5,trip.getDriver());
			pstate.setInt(6, trip.getMaximum_online_ticket_number());
			pstate.setInt(7, trip.getTrip_id());
			
			pstate.executeUpdate();
			
			return true;
		} catch (Exception e) {

		} finally {
			try {
				if(!result.isClosed()) {
					result.close();
				}
				if(!pstate.isClosed()) {
					pstate.close();
				}
				if(!con.isClosed()) {
					con.close();
				}
			} catch (Exception e2) {
				
			}
		}
		return false;
		
		
	}
	/**
	 * Delete trip
	 * */
	public boolean delete(int trip_id) {
		con = new DBConnection().getConnection();
		sql="DELETE FROM trip WHERE trip_id = ?";
		try {
			pstate = con.prepareStatement(sql);
			
			pstate.setInt(1, trip_id);
			pstate.executeUpdate();
			
			return true;
		} catch (Exception e) {

		} finally {
			try {
				if(!result.isClosed()) {
					result.close();
				}
				if(!pstate.isClosed()) {
					pstate.close();
				}
				if(!con.isClosed()) {
					con.close();
				}
			} catch (Exception e2) {
				
			}
		}
		return false;
	}
	
	public boolean checkAvailableTrip(String trip_id) {
		con = new DBConnection().getConnection();
		sql="SELECT * FROM Trip WHERE trip_id = ? AND booked_ticket_number < maximum_online_ticket_number";
		try {
			pstate = con.prepareStatement(sql);
			pstate.setString(1, trip_id); 
			result = pstate.executeQuery();			
			if(result.next()) {
				return true;
			}
		} catch (Exception e) {

		} finally {
			try {
				if(!result.isClosed()) {
					result.close();
				}
				if(!pstate.isClosed()) {
					pstate.close();
				}
				if(!con.isClosed()) {
					con.close();
				}
			} catch (Exception e2) {
				
			}
		}
		return false;
	}
}

