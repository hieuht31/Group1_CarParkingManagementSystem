package Dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import Entities.Car;
import Entities.FilterContent;
import Entities.PagingModel;
import Entities.ParkingLot;
import Entities.Ticket;
import Entities.Trip;
import Utils.DBConnection;

public class ParkingLotDAO {

	public ArrayList<ParkingLot> getAllParkingLot() {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pre = null;
		DBConnection db = new DBConnection();
		ArrayList<ParkingLot> listParkingLot = new ArrayList<>();
		String sql = "SELECT * FROM parking_lot WHERE park_status = 0";
		try {
			conn = db.getConnection();
			pre = conn.prepareStatement(sql);

			rs = pre.executeQuery();
			while (rs.next()) {
				ParkingLot parkingLot = new ParkingLot(rs.getInt("park_id"), rs.getInt("park_area"),
						rs.getString("park_name"), rs.getString("park_place"), rs.getFloat("park_price"),
						rs.getInt("park_status"), rs.getInt("park_maximum_slot"));

				listParkingLot.add(parkingLot);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return listParkingLot;
	}

	public PagingModel findAll(FilterContent filterContent) {
		Connection conn = null;
		ResultSet result = null;
		PreparedStatement pstate = null;
		List<ParkingLot> list = new ArrayList<ParkingLot>();
		PagingModel pagingModel = new PagingModel();
		conn = new DBConnection().getConnection();

		String searchCondition = (filterContent.getSearch().equals("") ? ""
				: " AND " + filterContent.getFilterBy() + " LIKE '%'+?+'%' \r\n");
		if (!filterContent.getSearch().equals("")
				&& (filterContent.getFilterBy().equals("park_area") || filterContent.getFilterBy().equals("park_price")
						|| filterContent.getFilterBy().equals("park_status"))) {
			searchCondition = " AND " + filterContent.getFilterBy() + "=?\r\n";
		}
		String sql = "SELECT *, COUNT(*) OVER()[record] FROM parking_lot where 1 = 1 \r\n" + searchCondition
				+ "ORDER BY park_id ASC\r\n" + "OFFSET ? ROW FETCH NEXT ? ROW ONLY";
		int numberRecord = filterContent.getPageSize();
		int startRecord = (filterContent.getPageNumber() - 1) * filterContent.getPageSize();
		int countTotalRecord = 0;
		try {
			pstate = conn.prepareStatement(sql);
			int step = 1;

			if (!filterContent.getSearch().equals("")) {
				pstate.setString(step++, filterContent.getSearch());
			}
			pstate.setInt(step++, startRecord);
			pstate.setInt(step++, numberRecord);
			result = pstate.executeQuery();

			if (result.next()) {
				list.add(new ParkingLot(result.getInt("park_id"), result.getInt("park_area"),
						result.getString("park_name"), result.getString("park_place"), result.getFloat("park_price"),
						result.getInt("park_status"), result.getInt("park_maximum_slot")));
				countTotalRecord += result.getInt("record");
			}
			while (result.next()) {
				list.add(new ParkingLot(result.getInt("park_id"), result.getInt("park_area"),
						result.getString("park_name"), result.getString("park_place"), result.getFloat("park_price"),
						result.getInt("park_status"), result.getInt("park_maximum_slot")));
			}

			pagingModel.setPageNumber(filterContent.getPageNumber());
			pagingModel.setPageSize(filterContent.getPageSize());
			pagingModel.setTotalRecord(countTotalRecord);
			pagingModel.setData(list);

			return pagingModel;
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			try {
				if (!result.isClosed()) {
					result.close();
				}
				if (!pstate.isClosed()) {
					pstate.close();
				}
				if (!conn.isClosed()) {
					conn.close();
				}
			} catch (Exception e2) {

			}
		}
		return null;
	}

	public int addParkingLot(ParkingLot parkingLot) {
		int var = 0;
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pre = null;
		DBConnection db = new DBConnection();

		String sql = "INSERT INTO parking_lot (park_area,park_name,park_place,park_price,park_status,park_maximum_slot) \n"
				+ "VALUES (?,?,?,?,?,?);";
		try {
			conn = db.getConnection();
			pre = conn.prepareStatement(sql);
			pre.setInt(1, parkingLot.getPark_area());
			pre.setString(2, parkingLot.getPark_name());
			pre.setString(3, parkingLot.getPark_place());
			pre.setDouble(4, parkingLot.getPark_price());
			pre.setInt(5, parkingLot.getPark_status());
			pre.setInt(6, parkingLot.getPark_maximum_slot());

			var += pre.executeUpdate();
		} catch (Exception ex) {
			System.out.println("Error");
		}
		return var;
	}

	public int deleteParkingLot(int parkingLotId) throws Exception {
		int i = 0;
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pre = null;
		DBConnection db = new DBConnection();

		String sql = "DELETE FROM parking_lot WHERE (`park_id` = ?);";

		try {
			conn = db.getConnection();
			pre = conn.prepareStatement(sql);
			pre.setInt(1, parkingLotId);
			i += pre.executeUpdate();
		} catch (Exception ex) {
			throw ex;
		}
		return i;
	}

	public int updateParkingLot(ParkingLot parkingLot) {
		int i = 0;
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pre = null;
		DBConnection db = new DBConnection();

		String sql = "UPDATE parking_lot SET park_area = ?, park_name = ?, park_place = ?, park_price = ?, park_status = ?, park_maximum_slot = ? WHERE park_id = ?;";

		try {
			conn = db.getConnection();
			pre = conn.prepareStatement(sql);
			pre.setInt(1, parkingLot.getPark_area());
			pre.setString(2, parkingLot.getPark_name());

			pre.setString(3, parkingLot.getPark_place());
			pre.setFloat(4, parkingLot.getPark_price());
			pre.setInt(5, parkingLot.getPark_status());
			pre.setInt(6, parkingLot.getPark_maximum_slot());
			pre.setInt(7, parkingLot.getPark_id());
			i += pre.executeUpdate();
		} catch (Exception ex) {
			System.out.println("Error");
		}
		return i;
	}

	public int changeParkingLotStatus(int park_id, int park_status) {
		int i = 0;
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pre = null;
		DBConnection db = new DBConnection();

		String sql = "UPDATE parking_lot SET park_status = ? WHERE park_id = ?;";

		try {
			conn = db.getConnection();
			pre = conn.prepareStatement(sql);

			pre.setInt(1, park_status);
			pre.setInt(2, park_id);

			i += pre.executeUpdate();
		} catch (Exception ex) {
			System.out.println("Error");
		}
		return i;
	}

	public ParkingLot getParkingLotById(int park_id) {

		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pre = null;
		DBConnection db = new DBConnection();

		String sql = "SELECT * FROM parking_lot WHERE park_id = ?";
		try {
			conn = db.getConnection();
			pre = conn.prepareStatement(sql);
			pre.setInt(1, park_id);

			rs = pre.executeQuery();
			while (rs.next()) {
				ParkingLot parkingLot = new ParkingLot(rs.getInt("park_id"), rs.getInt("park_area"),
						rs.getString("park_name"), rs.getString("park_place"), rs.getFloat("park_price"),
						rs.getInt("park_status"), rs.getInt("park_maximum_slot"));

				return parkingLot;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;

	}

	public ArrayList<ParkingLot> getAllActiveParkingLot() {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pre = null;
		DBConnection db = new DBConnection();
		ArrayList<ParkingLot> listParkingLot = new ArrayList<>();
		String sql = "SELECT * FROM parking_lot WHERE　park_status == 1";
		try {
			conn = db.getConnection();
			pre = conn.prepareStatement(sql);

			rs = pre.executeQuery();
			while (rs.next()) {
				ParkingLot parkingLot = new ParkingLot(rs.getInt("park_id"), rs.getInt("park_area"),
						rs.getString("park_name"), rs.getString("park_place"), rs.getFloat("park_price"),
						rs.getInt("park_status"), rs.getInt("park_maximum_slot"));

				listParkingLot.add(parkingLot);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return listParkingLot;
	}

	public boolean checkAvailablePark(String park_id) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pre = null;
		DBConnection db = new DBConnection();

		String sql = "SELECT * FROM parking_lot WHERE park_id = ? AND park_status=0";
		try {
			conn = db.getConnection();
			pre = conn.prepareStatement(sql);
			pre.setString(1, park_id);

			rs = pre.executeQuery();
			if (rs.next()) {				return true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}
}
