package Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * This class is used to connect to the database
 * and execute queries
 */
public class BaseDAO<T> {

    /*
     * This method is used to connect to the database
     */
    public Connection getConnection() {
        try {
            String url = "jdbc:sqlserver://localhost:1433;databaseName=car_parking_management_system";
            String user = "sa";
            String pass = "12345678";
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            return DriverManager.getConnection(url, user, pass);
        } catch (Exception ex) {
            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /*
     * This method is used to close the connection
     */
    public void closeConnection(Connection con) {
        try {
            if (con != null) {
                con.close();
            }
        } catch (Exception ex) {
            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*
     * This method is used to execute update queries
     */
    public boolean executeUpdate(String sql) {
        Connection con = getConnection();
        try {
            con.createStatement().executeUpdate(sql);
            return true;
        } catch (Exception ex) {
            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection(con);
        }
        return false;
    }

    /*
     * This method is used to execute queries and return list of objects
     */
    public List<T> executeQuery(String sql) {
        Connection con = getConnection();
        try {
            ResultSet rs = con.createStatement().executeQuery(sql);
            List<T> list = new ArrayList<>();
            while (rs.next()) {
                list.add(getObject(rs));
            }
            return list;
        } catch (Exception ex) {
            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection(con);
        }
        return null;
    }

    /*
     * This method is used to execute queries and return a single object
     */
    public T executeQuerySingle(String sql) {
        Connection con = getConnection();
        try {
            ResultSet rs = con.createStatement().executeQuery(sql);
            if (rs.next()) {
                return getObject(rs);
            }
        } catch (Exception ex) {
            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection(con);
        }
        return null;
    }

    /*
     * This method is used to get object from ResultSet
     */
    public T getObject(ResultSet rs) {
        return null;
    }
}
