package Dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import Entities.Department;
import Utils.DBConnection;

public class DepartmentDAO extends DBConnection {
	public ArrayList<Department> getAllDepartment() {
		ArrayList<Department> departments = new ArrayList<>();
		try {
			String sql = "SELECT * FROM department ";
			PreparedStatement stm = getConnection().prepareStatement(sql);
			ResultSet rs = stm.executeQuery();
			while (rs.next()) {
				Department d = new Department(rs.getInt("department_id"), rs.getString("department"));
				departments.add(d);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return departments;
	}

	public static void main(String[] args) {
		DepartmentDAO d = new DepartmentDAO();
		System.out.println(d.getAllDepartment());
	}
}
