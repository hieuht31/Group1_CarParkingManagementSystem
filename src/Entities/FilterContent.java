package Entities;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FilterContent {
	private int pageNumber;
	private int pageSize;
	private String searchValue;
	private String filterBy;
	public String getSearch() {
		String search[] = searchValue.split("&");
		return search.length==1?searchValue:search[0];
	}
	public Date getFilterDate() {
		String search[] = searchValue.split("&");
		
		for(String s:search) {
			System.out.println(s+ " " +search.length);
		}
		if(search.length>1)System.out.println(search[1].trim());
		return search.length==1?null:Date.valueOf(search[1].trim());
	}
}
