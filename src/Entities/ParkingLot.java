package Entities;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ParkingLot {
    private int park_id;
    private int park_area;
    private String park_name;
    private String park_place;
    private Float park_price;
    private int park_status;
    private int park_maximum_slot;
}
 