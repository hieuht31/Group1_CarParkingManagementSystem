package Entities;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BookingOffice {
    private int office_id;
    private Date end_contract_deadline;
    private String office_name;
    private String office_phone;
    private String office_place;
    private Float office_price;
    private Date start_contract_deadline;
    private Trip trip;
}
