package Entities;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Car {
	private int car_id;
    private String license_plate;
    private String car_color;
    private String car_type;
    private String company;
    private ParkingLot parkingLot;
}
