package Entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {
    private int employee_Id;
    private String account_name;
    private String employee_address;
    private Date employee_birthdate;
    private String employee_email;
    private String employee_name;
    private String employee_phone;
    private String password;
    private boolean sex;
    private Department department;
    
    public static void main(String[] args) {
		Employee e = new Employee();
		System.out.println(e.isSex());
	}

}
