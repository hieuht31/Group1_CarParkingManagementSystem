package Entities;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Time;

@Data
@AllArgsConstructor
public class Ticket {
	private int ticket_id;
	private Time booking_time;
	private String customer_name;
	private Car car;
	private Trip trip;
}
