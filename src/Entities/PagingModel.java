package Entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PagingModel {
	private int pageNumber;
	private int pageSize;
	private Object data;
	private int totalRecord;
}
