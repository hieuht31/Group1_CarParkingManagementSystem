package Entities;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Time;
import java.sql.Date;

@Data
@AllArgsConstructor
public class Trip {
	private int trip_id;
	private int booked_ticket_number;
	private String car_type;
	private Date departure_date;
	private Time departure_time;
	private String destination;
	private String driver;
	private int maximum_online_ticket_number;

}
