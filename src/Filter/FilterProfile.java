package Filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entities.Employee;

/**
 * Servlet Filter implementation class FilterProfile
 */
@WebFilter({ "/FilterProfile", "/user/*" })
public class FilterProfile implements Filter {
       
    /**
     * @see HttpFilter#HttpFilter()
     */
    public FilterProfile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		Employee emp = (Employee) req.getSession().getAttribute("employee");
		String emp_id = req.getParameter("employee_id");
		if(emp_id == null) {
			res.sendRedirect("http://localhost:8080/CarParkingManagement/AccessDenied");
		} else if(!emp_id.equals(emp.getEmployee_Id()+"")){
			res.sendRedirect("http://localhost:8080/CarParkingManagement/AccessDenied");
		} else {
			chain.doFilter(request, response);			
		}
		// pass the request along the filter chain
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
