package Filter;

import java.io.IOException;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entities.Employee;

/**
 * Servlet Filter implementation class ServiceFilter
 */
//dispatcherTypes = { DispatcherType.REQUEST }, urlPatterns = 
@WebFilter({"/trip/*", "/ticket/*"})
public class ServiceFilter implements Filter {

	/**
	 * @see HttpFilter#HttpFilter()
	 */
	public ServiceFilter() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		Employee emp = (Employee) req.getSession().getAttribute("employee");
		if(req.getSession().getAttribute("employee")==null) {
			res.sendRedirect("http://localhost:8080/CarParkingManagement/LoginController");
		} else if (emp.getDepartment().getDepartment_id() == 1 || emp.getDepartment().getDepartment_id() == 4) {
			chain.doFilter(request, response);
		} else {
			res.sendRedirect("http://localhost:8080/CarParkingManagement/AccessDenied");
		}
		// pass the request along the filter chain
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
