--CREATE DATABASE [car_parking_management_system]
USE [car_parking_management_system]
GO
/****** Object:  Table [dbo].[booking_office]    Script Date: 13/12/2022 15:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[booking_office](
	[office_id] [int] IDENTITY(1,1) NOT NULL,
	[end_contract_deadline] [date] NOT NULL,
	[office_name] [varchar](50) NOT NULL,
	[office_phone] [varchar](11) NOT NULL,
	[office_place] [varchar](50) NOT NULL,
	[office_price] [money] NOT NULL,
	[start_contract_deadline] [date] NOT NULL,
	[trip_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[office_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[car]    Script Date: 13/12/2022 15:07:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[car](
	[car_id] [int] IDENTITY(1,1) NOT NULL,
	[license_plate] [varchar](11) NOT NULL,
	[car_color] [varchar](11) NOT NULL,
	[car_type] [varchar](50) NULL,
	[company] [varchar](50) NULL,
	[park_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[car_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[department]    Script Date: 13/12/2022 15:07:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[department](
	[department_id] [int] IDENTITY(1,1) NOT NULL,
	[department] [varchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[department_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[employee]    Script Date: 13/12/2022 15:07:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[employee](
	[employee_id] [int] IDENTITY(1,1) NOT NULL,
	[account] [varchar](50) NOT NULL,
	[department_id] [int] NULL,
	[employee_address] [varchar](50) NOT NULL,
	[employee_birthdate] [date] NOT NULL,
	[employee_email] [varchar](50) NOT NULL,
	[employee_name] [varchar](50) NOT NULL,
	[employee_phone] [varchar](10) NOT NULL,
	[password] [varchar](20) NOT NULL,
	[sex] [char](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[employee_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[parking_lot]    Script Date: 13/12/2022 15:07:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[parking_lot](
	[park_id] [int] IDENTITY(1,1) NOT NULL,
	[park_area] [int] NOT NULL,
	[park_name] [varchar](50) NOT NULL,
	[park_place] [varchar](50) NOT NULL,
	[park_price] [money] NOT NULL,
	[park_status] [char](1) NOT NULL,
	[park_maximum_slot] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[park_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ticket]    Script Date: 13/12/2022 15:07:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ticket](
	[ticket_id] [int] IDENTITY(1,1) NOT NULL,
	[booking_time] [time](7) NOT NULL,
	[customer_name] [varchar](50) NULL,
	[car_id] [int] NULL,
	[trip_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ticket_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[trip]    Script Date: 13/12/2022 15:07:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[trip](
	[trip_id] [int] IDENTITY(1,1) NOT NULL,
	[booked_ticket_number] [int] NOT NULL,
	[car_type] [varchar](50) NOT NULL,
	[departure_date] [date] NOT NULL,
	[departure_time] [time](7) NOT NULL,
	[destination] [varchar](50) NOT NULL,
	[driver] [varchar](50) NOT NULL,
	[maximum_online_ticket_number] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[trip_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[booking_office] ON 

INSERT [dbo].[booking_office] ([office_id], [end_contract_deadline], [office_name], [office_phone], [office_place], [office_price], [start_contract_deadline], [trip_id]) VALUES (1, CAST(N'2022-11-13' AS Date), N'Nam phong', N'0386534234', N'Hai Phong', 1400000.0000, CAST(N'2022-11-11' AS Date), 1)
INSERT [dbo].[booking_office] ([office_id], [end_contract_deadline], [office_name], [office_phone], [office_place], [office_price], [start_contract_deadline], [trip_id]) VALUES (2, CAST(N'2022-11-15' AS Date), N'Bac phong', N'0386534432', N'Bac Giang', 1500000.0000, CAST(N'2022-11-12' AS Date), 2)
INSERT [dbo].[booking_office] ([office_id], [end_contract_deadline], [office_name], [office_phone], [office_place], [office_price], [start_contract_deadline], [trip_id]) VALUES (3, CAST(N'2022-11-12' AS Date), N'Tay phong', N'0386534323', N'Phu Tho', 1200000.0000, CAST(N'2022-11-11' AS Date), 3)
INSERT [dbo].[booking_office] ([office_id], [end_contract_deadline], [office_name], [office_phone], [office_place], [office_price], [start_contract_deadline], [trip_id]) VALUES (4, CAST(N'2022-11-13' AS Date), N'Nam phong', N'0386534234', N'Hai Phong', 1400000.0000, CAST(N'2022-11-11' AS Date), 1)
INSERT [dbo].[booking_office] ([office_id], [end_contract_deadline], [office_name], [office_phone], [office_place], [office_price], [start_contract_deadline], [trip_id]) VALUES (5, CAST(N'2022-11-15' AS Date), N'Bac phong', N'0386534432', N'Bac Giang', 1500000.0000, CAST(N'2022-11-12' AS Date), 2)
INSERT [dbo].[booking_office] ([office_id], [end_contract_deadline], [office_name], [office_phone], [office_place], [office_price], [start_contract_deadline], [trip_id]) VALUES (6, CAST(N'2022-11-12' AS Date), N'Tay phong', N'0386534323', N'Phu Tho', 1200000.0000, CAST(N'2022-11-11' AS Date), 3)
INSERT [dbo].[booking_office] ([office_id], [end_contract_deadline], [office_name], [office_phone], [office_place], [office_price], [start_contract_deadline], [trip_id]) VALUES (7, CAST(N'2022-11-13' AS Date), N'Nam phong', N'0386534234', N'Hai Phong', 1400000.0000, CAST(N'2022-11-11' AS Date), 1)
INSERT [dbo].[booking_office] ([office_id], [end_contract_deadline], [office_name], [office_phone], [office_place], [office_price], [start_contract_deadline], [trip_id]) VALUES (8, CAST(N'2022-11-15' AS Date), N'Bac phong', N'0386534432', N'Bac Giang', 1500000.0000, CAST(N'2022-11-12' AS Date), 2)
INSERT [dbo].[booking_office] ([office_id], [end_contract_deadline], [office_name], [office_phone], [office_place], [office_price], [start_contract_deadline], [trip_id]) VALUES (9, CAST(N'2022-11-12' AS Date), N'Tay phong', N'0386534323', N'Phu Tho', 1200000.0000, CAST(N'2022-11-11' AS Date), 3)
INSERT [dbo].[booking_office] ([office_id], [end_contract_deadline], [office_name], [office_phone], [office_place], [office_price], [start_contract_deadline], [trip_id]) VALUES (10, CAST(N'2022-11-13' AS Date), N'Nam phong', N'0386534234', N'Hai Phong', 1400000.0000, CAST(N'2022-11-11' AS Date), 1)
INSERT [dbo].[booking_office] ([office_id], [end_contract_deadline], [office_name], [office_phone], [office_place], [office_price], [start_contract_deadline], [trip_id]) VALUES (11, CAST(N'2022-11-15' AS Date), N'Bac phong', N'0386534432', N'Bac Giang', 1500000.0000, CAST(N'2022-11-12' AS Date), 2)
INSERT [dbo].[booking_office] ([office_id], [end_contract_deadline], [office_name], [office_phone], [office_place], [office_price], [start_contract_deadline], [trip_id]) VALUES (12, CAST(N'2022-11-12' AS Date), N'Tay phong', N'0386534323', N'Phu Tho', 1200000.0000, CAST(N'2022-11-11' AS Date), 3)
INSERT [dbo].[booking_office] ([office_id], [end_contract_deadline], [office_name], [office_phone], [office_place], [office_price], [start_contract_deadline], [trip_id]) VALUES (13, CAST(N'2022-11-13' AS Date), N'Nam phong', N'0386534234', N'Hai Phong', 1400000.0000, CAST(N'2022-11-11' AS Date), 1)
INSERT [dbo].[booking_office] ([office_id], [end_contract_deadline], [office_name], [office_phone], [office_place], [office_price], [start_contract_deadline], [trip_id]) VALUES (14, CAST(N'2022-11-15' AS Date), N'Bac phong', N'0386534432', N'Bac Giang', 1500000.0000, CAST(N'2022-11-12' AS Date), 2)
INSERT [dbo].[booking_office] ([office_id], [end_contract_deadline], [office_name], [office_phone], [office_place], [office_price], [start_contract_deadline], [trip_id]) VALUES (15, CAST(N'2022-11-12' AS Date), N'Tay phong', N'0386534323', N'Phu Tho', 1200000.0000, CAST(N'2022-11-11' AS Date), 3)
INSERT [dbo].[booking_office] ([office_id], [end_contract_deadline], [office_name], [office_phone], [office_place], [office_price], [start_contract_deadline], [trip_id]) VALUES (16, CAST(N'2022-11-13' AS Date), N'Nam phong', N'0386534234', N'Hai Phong', 1400000.0000, CAST(N'2022-11-11' AS Date), 1)
INSERT [dbo].[booking_office] ([office_id], [end_contract_deadline], [office_name], [office_phone], [office_place], [office_price], [start_contract_deadline], [trip_id]) VALUES (17, CAST(N'2022-11-15' AS Date), N'Bac phong', N'0386534432', N'Bac Giang', 1500000.0000, CAST(N'2022-11-12' AS Date), 2)
INSERT [dbo].[booking_office] ([office_id], [end_contract_deadline], [office_name], [office_phone], [office_place], [office_price], [start_contract_deadline], [trip_id]) VALUES (18, CAST(N'2022-11-12' AS Date), N'Tay phong', N'0386534323', N'Phu Tho', 1200000.0000, CAST(N'2022-11-11' AS Date), 3)
SET IDENTITY_INSERT [dbo].[booking_office] OFF
GO
SET IDENTITY_INSERT [dbo].[car] ON 

INSERT [dbo].[car] ([car_id], [license_plate], [car_color], [car_type], [company], [park_id]) VALUES (1, N'30E-148.45', N'red', N'xe giuong nam', N'phuong nam', 2)
INSERT [dbo].[car] ([car_id], [license_plate], [car_color], [car_type], [company], [park_id]) VALUES (2, N'31A-888.88', N'classic', N'xe 32 cho', N'phuong bac', 2)
INSERT [dbo].[car] ([car_id], [license_plate], [car_color], [car_type], [company], [park_id]) VALUES (3, N'30C-149.45', N'yellow', N'xe 40 cho', N'phuong tay', 2)
INSERT [dbo].[car] ([car_id], [license_plate], [car_color], [car_type], [company], [park_id]) VALUES (4, N'30B-148.45', N'red', N'xe giuong nam', N'phuong nam', 2)
INSERT [dbo].[car] ([car_id], [license_plate], [car_color], [car_type], [company], [park_id]) VALUES (5, N'31D-888.88', N'classic', N'xe 32 cho', N'phuong dong', 2)
INSERT [dbo].[car] ([car_id], [license_plate], [car_color], [car_type], [company], [park_id]) VALUES (6, N'30F-149.45', N'yellow', N'xe 40 cho', N'phuong nam', 2)
INSERT [dbo].[car] ([car_id], [license_plate], [car_color], [car_type], [company], [park_id]) VALUES (7, N'30G-148.45', N'red', N'xe giuong nam', N'phuong bac', 2)
INSERT [dbo].[car] ([car_id], [license_plate], [car_color], [car_type], [company], [park_id]) VALUES (8, N'31H-888.88', N'classic', N'xe 32 cho', N'phuong nam', 2)
INSERT [dbo].[car] ([car_id], [license_plate], [car_color], [car_type], [company], [park_id]) VALUES (9, N'30K-149.45', N'yellow', N'xe 40 cho', N'phuong dong', 2)
INSERT [dbo].[car] ([car_id], [license_plate], [car_color], [car_type], [company], [park_id]) VALUES (10, N'30J-148.45', N'red', N'xe giuong nam', N'phuong nam', 1)
INSERT [dbo].[car] ([car_id], [license_plate], [car_color], [car_type], [company], [park_id]) VALUES (11, N'31L-888.88', N'classic', N'xe 32 cho', N'phuong tich', 2)
INSERT [dbo].[car] ([car_id], [license_plate], [car_color], [car_type], [company], [park_id]) VALUES (12, N'30M-149.45', N'yellow', N'xe 40 cho', N'bac nam', 4)
INSERT [dbo].[car] ([car_id], [license_plate], [car_color], [car_type], [company], [park_id]) VALUES (13, N'30L-148.45', N'red', N'xe giuong nam', N'dong nam', 1)
INSERT [dbo].[car] ([car_id], [license_plate], [car_color], [car_type], [company], [park_id]) VALUES (14, N'31R-888.88', N'classic', N'xe 32 cho', N'phuong trinh', 1)
INSERT [dbo].[car] ([car_id], [license_plate], [car_color], [car_type], [company], [park_id]) VALUES (15, N'30Y-149.45', N'yellow', N'xe 40 cho', N'phuong phap', 3)
SET IDENTITY_INSERT [dbo].[car] OFF
GO
SET IDENTITY_INSERT [dbo].[department] ON 

INSERT [dbo].[department] ([department_id], [department]) VALUES (1, N'Admin')
INSERT [dbo].[department] ([department_id], [department]) VALUES (2, N'Employee')
INSERT [dbo].[department] ([department_id], [department]) VALUES (3, N'Parking')
INSERT [dbo].[department] ([department_id], [department]) VALUES (4, N'Service')
SET IDENTITY_INSERT [dbo].[department] OFF
GO
SET IDENTITY_INSERT [dbo].[employee] ON 

INSERT [dbo].[employee] ([employee_id], [account], [department_id], [employee_address], [employee_birthdate], [employee_email], [employee_name], [employee_phone], [password], [sex]) VALUES (1, N'admin01', 1, N'Ha Noi', CAST(N'2001-09-01' AS Date), N'admin01@gmail.com', N'Myca Sarah', N'0849566990', N'Ad1234', N'0')
INSERT [dbo].[employee] ([employee_id], [account], [department_id], [employee_address], [employee_birthdate], [employee_email], [employee_name], [employee_phone], [password], [sex]) VALUES (2, N'hrstaff01', 2, N'Ha Noi', CAST(N'2001-10-01' AS Date), N'hrstaff01@gmail.com', N'Myca Sarah', N'0849566990', N'Hrs123', N'0')
INSERT [dbo].[employee] ([employee_id], [account], [department_id], [employee_address], [employee_birthdate], [employee_email], [employee_name], [employee_phone], [password], [sex]) VALUES (3, N'parkingstaff01', 3, N'Ha Nam', CAST(N'2001-11-02' AS Date), N'parkingstaff01@gmail.com', N'Rem Coulson', N'0576976668', N'Prk123', N'1')
INSERT [dbo].[employee] ([employee_id], [account], [department_id], [employee_address], [employee_birthdate], [employee_email], [employee_name], [employee_phone], [password], [sex]) VALUES (4, N'servicestaff01', 4, N'Phu Tho', CAST(N'2001-10-03' AS Date), N'servicestaff01@gmail.com', N'Dowdall Dowdall', N'0898436348', N'Svs123', N'0')
INSERT [dbo].[employee] ([employee_id], [account], [department_id], [employee_address], [employee_birthdate], [employee_email], [employee_name], [employee_phone], [password], [sex]) VALUES (5, N'servicestaff02', 4, N'Ba Dinh', CAST(N'2001-10-02' AS Date), N'servicestaff02@gmail.com', N'Merla Merla', N'0898436348', N'123456', N'0')
INSERT [dbo].[employee] ([employee_id], [account], [department_id], [employee_address], [employee_birthdate], [employee_email], [employee_name], [employee_phone], [password], [sex]) VALUES (6, N'hrstaff02', 2, N'Ha Giang', CAST(N'2001-10-01' AS Date), N'hrstaff02@gmail.com', N'Myca Sarah', N'0849566990', N'123456', N'0')
INSERT [dbo].[employee] ([employee_id], [account], [department_id], [employee_address], [employee_birthdate], [employee_email], [employee_name], [employee_phone], [password], [sex]) VALUES (7, N'servicestaff03', 4, N'Phu Quoc', CAST(N'2001-10-01' AS Date), N'servicestaff03@gmail.com', N'Merla DowMerladall', N'0898436348', N'123456', N'0')
INSERT [dbo].[employee] ([employee_id], [account], [department_id], [employee_address], [employee_birthdate], [employee_email], [employee_name], [employee_phone], [password], [sex]) VALUES (8, N'parkingstaff02', 3, N'Ha Dong', CAST(N'2001-10-03' AS Date), N'parkingstaff02@gmail.com', N'RemMerla Coulson', N'0576976668', N'123456', N'1')
INSERT [dbo].[employee] ([employee_id], [account], [department_id], [employee_address], [employee_birthdate], [employee_email], [employee_name], [employee_phone], [password], [sex]) VALUES (9, N'hrstaff03', 2, N'Noi Bai', CAST(N'2001-01-01' AS Date), N'hrstaff03@gmail.com', N'MycaSarah Sarah', N'0849566990', N'123456', N'0')
INSERT [dbo].[employee] ([employee_id], [account], [department_id], [employee_address], [employee_birthdate], [employee_email], [employee_name], [employee_phone], [password], [sex]) VALUES (10, N'servicestaff04', 4, N'Phu Tho', CAST(N'2001-10-15' AS Date), N'servicestaff04@gmail.com', N'MerlaRem Dowdall', N'0898436348', N'123456', N'0')
INSERT [dbo].[employee] ([employee_id], [account], [department_id], [employee_address], [employee_birthdate], [employee_email], [employee_name], [employee_phone], [password], [sex]) VALUES (11, N'parkingstaff03', 3, N'Ha Nam', CAST(N'2002-10-02' AS Date), N'parkingstaff03@gmail.com', N'RemRem Coulson', N'0576976668', N'123456', N'1')
INSERT [dbo].[employee] ([employee_id], [account], [department_id], [employee_address], [employee_birthdate], [employee_email], [employee_name], [employee_phone], [password], [sex]) VALUES (12, N'hrstaff04', 2, N'Ha Noi', CAST(N'2001-06-01' AS Date), N'hrstaff04@gmail.com', N'MycaRem Sarah', N'0849566990', N'123456', N'0')
INSERT [dbo].[employee] ([employee_id], [account], [department_id], [employee_address], [employee_birthdate], [employee_email], [employee_name], [employee_phone], [password], [sex]) VALUES (13, N'servicestaff05', 4, N'Phu Tho', CAST(N'2001-04-15' AS Date), N'servicestaff05@gmail.com', N'Merla DowdallRem', N'0898436348', N'123456', N'0')
INSERT [dbo].[employee] ([employee_id], [account], [department_id], [employee_address], [employee_birthdate], [employee_email], [employee_name], [employee_phone], [password], [sex]) VALUES (14, N'parkingstaff04', 3, N'Ha Nam', CAST(N'2001-05-02' AS Date), N'parkingstaff04@gmail.com', N'RemRem Coulson', N'0576976668', N'123456', N'1')
INSERT [dbo].[employee] ([employee_id], [account], [department_id], [employee_address], [employee_birthdate], [employee_email], [employee_name], [employee_phone], [password], [sex]) VALUES (15, N'hrstaff05', 2, N'Ha Noi', CAST(N'2001-07-01' AS Date), N'hrstaff05@gmail.com', N'Myca SarahRem', N'0849566990', N'123456', N'0')
SET IDENTITY_INSERT [dbo].[employee] OFF
GO
SET IDENTITY_INSERT [dbo].[parking_lot] ON 

INSERT [dbo].[parking_lot] ([park_id], [park_area], [park_name], [park_place], [park_price], [park_status], [park_maximum_slot]) VALUES (1, 30, N'Bai so 1A', N'Khu A', 10000.0000, N'0', 10)
INSERT [dbo].[parking_lot] ([park_id], [park_area], [park_name], [park_place], [park_price], [park_status], [park_maximum_slot]) VALUES (2, 20, N'Bai so 2B', N'Khu B', 10000.0000, N'1', 10)
INSERT [dbo].[parking_lot] ([park_id], [park_area], [park_name], [park_place], [park_price], [park_status], [park_maximum_slot]) VALUES (3, 30, N'Bai so 3C', N'Khu C', 10000.0000, N'0', 10)
INSERT [dbo].[parking_lot] ([park_id], [park_area], [park_name], [park_place], [park_price], [park_status], [park_maximum_slot]) VALUES (4, 30, N'Bai so 2A', N'Khu A', 10000.0000, N'0', 10)
INSERT [dbo].[parking_lot] ([park_id], [park_area], [park_name], [park_place], [park_price], [park_status], [park_maximum_slot]) VALUES (5, 20, N'Bai so 1B', N'Khu B', 10000.0000, N'0', 10)
INSERT [dbo].[parking_lot] ([park_id], [park_area], [park_name], [park_place], [park_price], [park_status], [park_maximum_slot]) VALUES (6, 30, N'Bai so 2C', N'Khu C', 10000.0000, N'0', 10)
INSERT [dbo].[parking_lot] ([park_id], [park_area], [park_name], [park_place], [park_price], [park_status], [park_maximum_slot]) VALUES (7, 30, N'Bai so 3A', N'Khu A', 10000.0000, N'0', 10)
INSERT [dbo].[parking_lot] ([park_id], [park_area], [park_name], [park_place], [park_price], [park_status], [park_maximum_slot]) VALUES (8, 20, N'Bai so 3B', N'Khu B', 10000.0000, N'0', 10)
INSERT [dbo].[parking_lot] ([park_id], [park_area], [park_name], [park_place], [park_price], [park_status], [park_maximum_slot]) VALUES (9, 30, N'Bai so 1C', N'Khu C', 10000.0000, N'0', 10)
SET IDENTITY_INSERT [dbo].[parking_lot] OFF
GO
SET IDENTITY_INSERT [dbo].[ticket] ON 

INSERT [dbo].[ticket] ([ticket_id], [booking_time], [customer_name], [car_id], [trip_id]) VALUES (1, CAST(N'12:40:00' AS Time), N'Nguyen Minh Muoi', 1, 1)
INSERT [dbo].[ticket] ([ticket_id], [booking_time], [customer_name], [car_id], [trip_id]) VALUES (2, CAST(N'11:45:00' AS Time), N'Nguyen Minh Mot', 1, 1)
INSERT [dbo].[ticket] ([ticket_id], [booking_time], [customer_name], [car_id], [trip_id]) VALUES (3, CAST(N'10:30:00' AS Time), N'Nguyen Minh Hai', 1, 1)
INSERT [dbo].[ticket] ([ticket_id], [booking_time], [customer_name], [car_id], [trip_id]) VALUES (4, CAST(N'12:40:00' AS Time), N'Nguyen Minh Muoi', 1, 1)
INSERT [dbo].[ticket] ([ticket_id], [booking_time], [customer_name], [car_id], [trip_id]) VALUES (5, CAST(N'11:45:00' AS Time), N'Nguyen Minh Mot', 1, 1)
INSERT [dbo].[ticket] ([ticket_id], [booking_time], [customer_name], [car_id], [trip_id]) VALUES (6, CAST(N'10:30:00' AS Time), N'Nguyen Minh Hai', 1, 1)
INSERT [dbo].[ticket] ([ticket_id], [booking_time], [customer_name], [car_id], [trip_id]) VALUES (7, CAST(N'12:40:00' AS Time), N'Nguyen Minh Muoi', 1, 1)
INSERT [dbo].[ticket] ([ticket_id], [booking_time], [customer_name], [car_id], [trip_id]) VALUES (8, CAST(N'11:45:00' AS Time), N'Nguyen Minh Mot', 1, 1)
INSERT [dbo].[ticket] ([ticket_id], [booking_time], [customer_name], [car_id], [trip_id]) VALUES (9, CAST(N'10:30:00' AS Time), N'Nguyen Minh Hai', 1, 1)
INSERT [dbo].[ticket] ([ticket_id], [booking_time], [customer_name], [car_id], [trip_id]) VALUES (10, CAST(N'12:40:00' AS Time), N'Nguyen Minh Muoi', 3, 2)
INSERT [dbo].[ticket] ([ticket_id], [booking_time], [customer_name], [car_id], [trip_id]) VALUES (11, CAST(N'11:45:00' AS Time), N'Nguyen Minh Mot', 4, 3)
INSERT [dbo].[ticket] ([ticket_id], [booking_time], [customer_name], [car_id], [trip_id]) VALUES (12, CAST(N'10:30:00' AS Time), N'Nguyen Minh Hai', 3, 2)
SET IDENTITY_INSERT [dbo].[ticket] OFF
GO
SET IDENTITY_INSERT [dbo].[trip] ON 

INSERT [dbo].[trip] ([trip_id], [booked_ticket_number], [car_type], [departure_date], [departure_time], [destination], [driver], [maximum_online_ticket_number]) VALUES (1, 9, N'xe giuong nam', CAST(N'2022-12-23' AS Date), CAST(N'10:30:00' AS Time), N'Hai Phong', N'Nguyen Van Chin', 10)
INSERT [dbo].[trip] ([trip_id], [booked_ticket_number], [car_type], [departure_date], [departure_time], [destination], [driver], [maximum_online_ticket_number]) VALUES (2, 2, N'xe 32 cho', CAST(N'2022-12-25' AS Date), CAST(N'10:30:00' AS Time), N'Bac Giang', N'Nguyen Van Tam', 10)
INSERT [dbo].[trip] ([trip_id], [booked_ticket_number], [car_type], [departure_date], [departure_time], [destination], [driver], [maximum_online_ticket_number]) VALUES (3, 1, N'xe 40 cho', CAST(N'2022-12-23' AS Date), CAST(N'09:30:00' AS Time), N'Phu Tho', N'Nguyen Van Bay', 10)
INSERT [dbo].[trip] ([trip_id], [booked_ticket_number], [car_type], [departure_date], [departure_time], [destination], [driver], [maximum_online_ticket_number]) VALUES (4, 0, N'xe giuong nam', CAST(N'2022-12-24' AS Date), CAST(N'10:30:00' AS Time), N'Hai Phong', N'Nguyen Van Lai', 10)
INSERT [dbo].[trip] ([trip_id], [booked_ticket_number], [car_type], [departure_date], [departure_time], [destination], [driver], [maximum_online_ticket_number]) VALUES (5, 0, N'xe 32 cho', CAST(N'2022-12-26' AS Date), CAST(N'10:30:00' AS Time), N'Bac Giang', N'Nguyen Van Hach', 10)
INSERT [dbo].[trip] ([trip_id], [booked_ticket_number], [car_type], [departure_date], [departure_time], [destination], [driver], [maximum_online_ticket_number]) VALUES (6, 0, N'xe 40 cho', CAST(N'2022-12-27' AS Date), CAST(N'09:30:00' AS Time), N'Phu Tho', N'Nguyen Van Nam', 10)
INSERT [dbo].[trip] ([trip_id], [booked_ticket_number], [car_type], [departure_date], [departure_time], [destination], [driver], [maximum_online_ticket_number]) VALUES (7, 0, N'xe giuong nam', CAST(N'2022-12-25' AS Date), CAST(N'10:30:00' AS Time), N'Son La', N'Nguyen Van Cuu', 10)
INSERT [dbo].[trip] ([trip_id], [booked_ticket_number], [car_type], [departure_date], [departure_time], [destination], [driver], [maximum_online_ticket_number]) VALUES (8, 0, N'xe 32 cho', CAST(N'2022-12-29' AS Date), CAST(N'10:30:00' AS Time), N'Quang Ninh', N'Nguyen Van Bat', 10)
INSERT [dbo].[trip] ([trip_id], [booked_ticket_number], [car_type], [departure_date], [departure_time], [destination], [driver], [maximum_online_ticket_number]) VALUES (9, 0, N'xe 40 cho', CAST(N'2022-12-28' AS Date), CAST(N'09:30:00' AS Time), N'Thai Binh', N'Nguyen Van That', 10)
INSERT [dbo].[trip] ([trip_id], [booked_ticket_number], [car_type], [departure_date], [departure_time], [destination], [driver], [maximum_online_ticket_number]) VALUES (10, 0, N'xe giuong nam', CAST(N'2022-12-27' AS Date), CAST(N'10:30:00' AS Time), N'Son La', N'Nguyen Van Ngu', 10)
INSERT [dbo].[trip] ([trip_id], [booked_ticket_number], [car_type], [departure_date], [departure_time], [destination], [driver], [maximum_online_ticket_number]) VALUES (11, 0, N'xe 32 cho', CAST(N'2022-12-27' AS Date), CAST(N'10:30:00' AS Time), N'Thai Binh', N'Nguyen Van Bon', 10)
INSERT [dbo].[trip] ([trip_id], [booked_ticket_number], [car_type], [departure_date], [departure_time], [destination], [driver], [maximum_online_ticket_number]) VALUES (12, 0, N'xe 40 cho', CAST(N'2022-12-28' AS Date), CAST(N'09:30:00' AS Time), N'Quan Ninh', N'Nguyen Van Tu', 10)
SET IDENTITY_INSERT [dbo].[trip] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__car__F72CD56EFFCA752F]    Script Date: 13/12/2022 15:07:50 ******/
ALTER TABLE [dbo].[car] ADD UNIQUE NONCLUSTERED 
(
	[license_plate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__employee__EA162E1193A9FCA3]    Script Date: 13/12/2022 15:07:50 ******/
ALTER TABLE [dbo].[employee] ADD UNIQUE NONCLUSTERED 
(
	[account] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[booking_office]  WITH CHECK ADD FOREIGN KEY([trip_id])
REFERENCES [dbo].[trip] ([trip_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[car]  WITH CHECK ADD FOREIGN KEY([park_id])
REFERENCES [dbo].[parking_lot] ([park_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[employee]  WITH CHECK ADD FOREIGN KEY([department_id])
REFERENCES [dbo].[department] ([department_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ticket]  WITH CHECK ADD FOREIGN KEY([car_id])
REFERENCES [dbo].[car] ([car_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ticket]  WITH CHECK ADD FOREIGN KEY([trip_id])
REFERENCES [dbo].[trip] ([trip_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  Trigger [dbo].[trg_parking_car]    Script Date: 13/12/2022 15:07:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[trg_parking_car] 
ON [dbo].[car]
FOR INSERT, UPDATE, DELETE
AS
BEGIN
	DECLARE @park_ori_id INT, @park_new_id INT, @parked_number INT
	-- set value if update, park_id in car
	-- get origin park_id if update park_id or delete car
	SELECT @park_ori_id = ISNULL(d.park_id,0) FROM deleted d FULL OUTER JOIN inserted i ON ISNULL(d.park_id,0) <> ISNULL(i.park_id,0)
	----SELECT * FROM deleted d FULL OUTER JOIN inserted i ON ISNULL(d.park_id,0) <> ISNULL(i.park_id,0)
	----SELECT * FROM deleted d
	----SELECT * FROM inserted 
	----SELECT * FROM deleted d, inserted i WHERE ISNULL(d.park_id,0) <> ISNULL(i.park_id,0)
	-- get new park_id if add car or update park_id
	SELECT @park_new_id = ISNULL(i.park_id,0) FROM deleted d FULL OUTER JOIN inserted i ON ISNULL(d.park_id,0) <> ISNULL(i.park_id,0)
	-- if update park_id or delete car 
	------PRINT 'park_ori_id = '+ CAST(@park_ori_id AS VARCHAR(10))
	------PRINT 'park_new_id = '+ CAST(@park_new_id AS VARCHAR(10))
	IF(@park_ori_id >0)
	BEGIN 
		UPDATE parking_lot SET park_status = '0' WHERE park_id = @park_ori_id
		SELECT @parked_number = ISNULL(COUNT(*),0) FROM parking_lot pl 
		INNER JOIN car c ON pl.park_id = c.park_id WHERE c.park_id = @park_new_id
		GROUP BY c.park_id
		UPDATE parking_lot SET park_status = '1' WHERE park_id = @park_new_id AND park_maximum_slot = @parked_number
	END
	-- if add car, update park_id then check park_status 
	IF(@park_ori_id = 0 AND @park_new_id > 0)
	BEGIN 
		SELECT @parked_number = ISNULL(COUNT(*),0) FROM parking_lot pl 
		INNER JOIN car c ON pl.park_id = c.park_id WHERE c.park_id = @park_new_id
		GROUP BY c.park_id
		UPDATE parking_lot SET park_status = '1' WHERE park_id = @park_new_id AND park_maximum_slot = @parked_number
	END
	------PRINT 'parked_number = '+ CAST(@parked_number AS VARCHAR(10))
END
GO
ALTER TABLE [dbo].[car] ENABLE TRIGGER [trg_parking_car]
GO
/****** Object:  Trigger [dbo].[trg_book_ticket]    Script Date: 13/12/2022 15:07:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[trg_book_ticket]
ON [dbo].[ticket]
FOR INSERT,UPDATE, DELETE
AS
BEGIN
	DECLARE @new_trip_id INT,@trip_ori_id INT

	SELECT @trip_ori_id = ISNULL(d.trip_id,0) FROM deleted d FULL OUTER JOIN inserted i ON ISNULL(d.trip_id,0) <> ISNULL(i.trip_id,0)
	SELECT @new_trip_id = ISNULL(i.trip_id,0) FROM inserted i FULL OUTER JOIN deleted d ON ISNULL(d.trip_id,0) <> ISNULL(i.trip_id,0)
	--PRINT '@trip_ori_id'+ CAST(@trip_ori_id AS VARCHAR(10))
	--PRINT '@new_trip_id'+ CAST(@new_trip_id AS VARCHAR(10))
	IF(@trip_ori_id>0)
	BEGIN 
		UPDATE trip SET booked_ticket_number = booked_ticket_number-1 WHERE trip_id = @trip_ori_id
		UPDATE trip SET booked_ticket_number = booked_ticket_number + 1 WHERE trip_id = @new_trip_id
	END

	IF(@trip_ori_id = 0 AND @new_trip_id >0)
	BEGIN
		UPDATE trip SET booked_ticket_number = booked_ticket_number + 1 WHERE trip_id = @new_trip_id
	END
END
GO
ALTER TABLE [dbo].[ticket] ENABLE TRIGGER [trg_book_ticket]
GO
