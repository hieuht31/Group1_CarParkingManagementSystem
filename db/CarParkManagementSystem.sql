CREATE DATABASE car_parking_management_system

USE car_parking_management_system

CREATE TABLE department(
	department_id INT PRIMARY KEY IDENTITY(1,1),
	department VARCHAR(10) NOT NULL
)

CREATE TABLE employee(
	employee_id INT PRIMARY KEY identity(1,1),
	account VARCHAR(50) NOT NULL UNIQUE,
	department_id INT REFERENCES department(department_id) ON UPDATE CASCADE ON DELETE CASCADE,
	employee_address VARCHAR(50) NOT NULL,
	employee_birthdate DATE NOT NULL,
	employee_email VARCHAR(50) NOT NULL,
	employee_name VARCHAR(50) NOT NULL,
	employee_phone VARCHAR(10) NOT NULL,
	[password] VARCHAR(20) NOT NULL,
	sex CHAR(1) NOT NULL
)

CREATE TABLE parking_lot(
	park_id INT PRIMARY KEY IDENTITY(1,1),
	park_area INT NOT NULL,
	park_name VARCHAR(50) NOT NULL,
	park_place VARCHAR(50) NOT NULL,
	park_price MONEY NOT NULL,
	park_status CHAR(1) NOT NULL
)

CREATE TABLE car(
	car_id INT PRIMARY KEY IDENTITY(1,1),
	license_plate VARCHAR(11) NOT NULL UNIQUE,
	car_color VARCHAR(11) NOT NULL,
	car_type VARCHAR(50),
	company VARCHAR(50),
	park_id INT REFERENCES parking_lot(park_id) ON UPDATE CASCADE ON DELETE CASCADE
)

CREATE TABLE trip(
	trip_id INT PRIMARY KEY IDENTITY(1,1),
	booked_ticked_number INT NOT NULL,
	car_type VARCHAR(50) NOT NULL,
	departure_date DATE NOT NULL,
	departure_time TIME NOT NULL,
	destination VARCHAR(50) NOT NULL,
	driver VARCHAR(50) NOT NULL,
	maximum_online_ticket_number INT NOT NULL,
)

CREATE TABLE ticket(
	ticket_id INT PRIMARY KEY IDENTITY(1,1),
	booking_time TIME NOT NULL,
	customer_name VARCHAR(50),
	car_id INT REFERENCES car(car_id) ON UPDATE CASCADE ON DELETE CASCADE,
	trip_id INT REFERENCES trip(trip_id) ON UPDATE CASCADE ON DELETE CASCADE
)

CREATE TABLE booking_office(
	office_id INT PRIMARY KEY IDENTITY(1,1),
	end_contract_deadline DATE NOT NULL,
	office_name VARCHAR(50) NOT NULL, 
	office_phone VARCHAR(11) NOT NULL,
	office_place VARCHAR(50) NOT NULL,
	office_price MONEY NOT NULL,
	start_contract_deadline DATE NOT NULL,
	trip_id INT REFERENCES trip(trip_id) ON UPDATE CASCADE ON DELETE CASCADE
)

INSERT INTO department VALUES('Employee'),('Parking'),('Service')

SELECT *FROM department

select * from employee
INSERT INTO employee VALUES ('hrstaff01', 1, 'Ha Noi', '2001-09-01', 'hrstaff01@gmail.com', 'Myca Sarah', '0849566990', '123', '0');
INSERT INTO employee values ('parkingstaff01', 2, 'Ha Nam', '2001-10-02', 'rcoulson1@gmail.com', 'Rem Coulson', '0576976668', '123', '1');
INSERT INTO employee values ('servicestaff01', 3, 'Phu Tho', '2001-10-15', 'mdowdall@gmail.com', 'Merla Dowdall', '0898436348', '123', '0');

select * from parking_lot
INSERT INTO parking_lot VALUES (30,'Bai so 1','Khu A',10000,'1')
INSERT INTO parking_lot VALUES (20,'Bai so 2','Khu B',10000,'0')
INSERT INTO parking_lot VALUES (30,'Bai so 3','Khu C',10000,'1')

select * from car 

insert into car VALUES ('30E-148.45','red','xe giuong nam','phuong nam',2)
insert into car VALUES ('31A-888.88','classic','xe 32 cho','phuong nam',2)
insert into car VALUES ('30C-149.45','yellow','xe 40 cho','phuong nam',2)

select * FROM trip

insert into trip VALUES (30,'xe giuong nam','2022-11-11','10:30:00','Hai Phong','Nguyen Van Chin',40)
insert into trip VALUES (20,'xe 32 cho','2022-11-12','10:30:00','Bac Giang','Nguyen Van Tam',32)
insert into trip VALUES (25,'xe 40 cho','2022-11-11','9:30:00','Phu Tho','Nguyen Van Bay',40)

select * from ticket

insert into ticket VALUES ('12:40:00','Nguyen Minh Muoi',1,1)
insert into ticket VALUES ('11:45:00','Nguyen Minh Mot',3,3)
insert into ticket VALUES ('10:30:00','Nguyen Minh Hai',2,2)

SELECT * FROM booking_office
INSERT INTO booking_office VALUES ('2022-11-13','Nam phong','0386534234','Hai Phong',1400000,'2022-11-11',1)
INSERT INTO booking_office VALUES ('2022-11-15','Bac phong','0386534432','Bac Giang',1500000,'2022-11-12',2)
INSERT INTO booking_office VALUES ('2022-11-12','Tay phong','0386534323','Phu Tho',1200000,'2022-11-11',3)

select * from ticket
select * FROM car
SELECT ti.*,car.license_plate,tr.destination,tr.departure_date, COUNT(*) OVER ()[record] 
FROM ticket ti inner join trip tr on ti.trip_id = tr.trip_id inner join car ON ti.car_id = car.car_id
WHERE car.license_plate LIKE '%%' AND customer_name LIKE '%%' AND destination LIKE '%%' AND departure_date >= '2022-11-11'
ORDER BY departure_date ASC
OFFSET 0 ROWS FETCH FIRST 3 ROW ONLY

SELECT ticket.*FROM ticket

SELECT * FROM trip
SELECT * FROM car

SELECT ti.*,car.license_plate,tr.destination,tr.departure_date 
FROM ticket ti inner join trip tr on ti.trip_id = tr.trip_id 
inner join car ON ti.car_id = car.car_id WHERE ticket_id=2

--ALTER TABLE employee
--ADD CONSTRAINT uq_employee_account UNIQUE(account)

SELECT *, COUNT(*) OVER()[number_record] FROM trip WHERE departure_date>= '2022-11-11' ORDER BY departure_date ASC
OFFSET 0 ROW FETCH NEXT 2 ROWS ONLY


