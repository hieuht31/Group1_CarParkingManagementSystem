<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<main>
	<h1>Ticket ${param.action}</h1>
	<hr>
	<form id="form" action="${param.action}" method="post">
		<c:if test="${requestScope.ticket != null}"><input type="hidden" value="${requestScope.ticket.ticket_id}" name="ticket_id"/></c:if>
		<div class="input-box">
			<label for="cusotmer">Customer<span>(*)</span></label> <input
				type="text" id="customer" name="customer"
				<c:if test="${requestScope.ticket != null}">value="${ticket.customer_name}"</c:if> required placeholder="Customer name">
		</div>
		<div class="input-box">
			<label for="bookingTime">Booking time<span>(*)</span></label> <input
				type="time" id="bookingTime" name="bookingTime" <c:if test="${requestScope.ticket != null}">value="${ticket.booking_time}"</c:if> required>
			<p class="error-message">a</p>
		</div>
		<div class="input-box">
			<label for="trip">Trip<span>(*)</span></label> 
			<select name="trip"	id="trip" required>
				<c:if test="${requestScope.ticket != null}"><option value="${requestScope.ticket.trip.trip_id}" hidden>${ticket.trip.destination}</option></c:if>
				<%----%><c:forEach var="trip" items="${requestScope.listTrip}">
					<option value="${trip.trip_id}">${trip.destination}</option>
				</c:forEach>
			</select>
			<p class="error-message">a</p>
		</div>
		<div class="input-box">
			<label for="licensePlate">License plate<span>(*)</span></label> <select
				name="licensePlate" id="licensePlate" required>
				<c:if test="${requestScope.ticket != null}"><option value="${requestScope.ticket.car.car_id}" hidden>${requestScope.ticket.car.license_plate}</option></c:if>
				<%----%><c:forEach var="car" varStatus="loop" items="${requestScope.listCar}">
					<option value="${car.car_id}">
					${car.license_plate}
					</option>
				</c:forEach>
			</select>
			
		</div>
		<footer>
			<button class="btn btn-m" id="btn-back" type="button">
				<i class="fas fa-angle-double-left"></i>Back
			</button>
			<button class="btn btn-m success" id="btn-${param.action}" type="button">
				<i class="fas fa-plus"></i><span class="up-first">${param.action}</span>
			</button>
			<button class="btn btn-m warning" type="reset">
				<i class="fas fa-undo"></i>Reset
			</button>
		</footer>
	</form>
</main>