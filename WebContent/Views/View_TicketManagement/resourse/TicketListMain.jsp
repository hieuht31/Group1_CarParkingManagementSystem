<%-- <%@page import="Entities.Ticket"%> --%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<main>
	<h1>Ticket List</h1>
	<hr>
	<div class="search-container">
		<form action="#" method="post">
			<div class="search-field search-group">
				<i class="fas fa-search"></i> <input type="search"
					name="searchString" value="">
			</div>
			<div class="filter-field search-group">
				<label for="filter-type"><i class="fas fa-filter"></i>Filter
					by</label> <select name="filterBy" id="filter-type">
					<option value = "destination" data-inputType = "search">Trip</option>
					<option value = "license_plate" data-inputType = "search">License</option>
					<option value = "customer_name" data-inputType = "search">Customer</option>
					<option value = "booking_time" data-type = "time">Booking time</option>
				</select>
			</div>
			<button class="filter-submit" type="button" onclick="filter()">Search</button>

			<div class="date-filter">
				<input type="date" name="date-filter" value =""/>
			</div>
		</form>
	</div>
	<table id="table-result">
		<thead>
			<tr>
				<td>No</td>
				<td>Trip</td>
				<td>License Plate</td>
				<td>Customer</td>
				<td>Booking time</td>
				<td>Action</td>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	<div class="table-pagination" style="margin-top: 50px;">
		<ul class="pagination" id="pagination">
		</ul>
	</div>
</main>