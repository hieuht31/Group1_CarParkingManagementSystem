<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<main>
	<h1>Parking Lot List</h1>
	<hr>
	<div class="search-container">
		<form action="#" method="post">
			<div class="search-field search-group">
				<i class="fas fa-search"></i> <input type="search"
					name="searchString">
			</div>
			<div class="filter-field search-group">
				<label for="filter-type"><i class="fas fa-filter"></i>Filter

					by</label> <select name="filterBy" id="filter-type">

					<option value="park_name">Parking lot</option>
					<option value="park_place">Place</option>
					<option value="park_area" data-type="number">Area</option>
					<option value="park_price" data-type="number">Price</option>
					<option value="park_status">Status</option>

				</select>
			</div>
			<button class="filter-submit" type="button" onclick="filter()">Search</button>
		</form>
	</div>
	<table id="table-result">
		<thead>
			<tr>
			<td>ID</td>
				<td>Parking lot</td>
				<td>Place</td>
				<td>Area</td>
				<td>Price</td>
				<td>Status</td>
				<td>Action</td>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	<div class="table-pagination" style="margin-top: 50px;">
		<ul class="pagination" id="pagination">
		</ul>
	</div>
</main>