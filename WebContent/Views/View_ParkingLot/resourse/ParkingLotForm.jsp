<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<main>
	<h1>Parking Lot</h1>
	<hr>
	<form id="form" action="${param.action}" method="post">
		<c:if test="${requestScope.park != null}"><input type="hidden" value="${requestScope.park.park_id}" name="park_id"/></c:if>
		<div class="input-box ">
			<label for="park_name">Parking name<span>(*)</span></label> <input
				type="text" id="park_name" name="park_name"
				<c:if test="${requestScope.park!= null}">value="${park.park_name}"</c:if> required>
			
		</div>
		<div class="input-box ">
			<label for="park_place">Place<span>(*)</span></label> <input
				type="text" id="park_place" name="park_place"
				<c:if test="${requestScope.park!= null}">value="${park.park_place}"</c:if> required>
			
		</div>
		<div class="input-box" data-unit="(m2)">
			<label for="area">Area<span>(*)</span></label> <input
				type="number" id="area" name="park_area" <c:if test="${requestScope.park!= null}">value="${park.park_area}"</c:if> required>
			
		</div>
		<div class="input-box" data-unit="(VNĐ)">
			<label for="price">Price<span>(*)</span></label> <input
				type="number" id="price" name="park_price" <c:if test="${requestScope.park!= null}">value="${park.park_price}"</c:if> required>
			
		</div>
		
		<div class="input-box" >
			<label for="max_slot">Max Slot<span>(*)</span></label> <input
				type="number" id="max_slot" name="max_park_slot" <c:if test="${requestScope.park!= null}">value="${park.park_maximum_slot}"</c:if> required>
			
		</div>
		<c:if test="${requestScope.park!= null}">
		<div class="input-box ">
			<label for="park_status">Status<span>(*)</span></label>
			<select name="park_status" id="status">
				<option value="0" ${park.park_status == 0?"selected":""}>Active</option>
				<option value="2" ${park.park_status == 2?"selected":""}>Inactive</option>
				<option value="1" ${park.park_status == 1?"selected":""}>Full</option>
			</select>
					
		</div>
		</c:if>
		<footer>
			<button class="btn btn-m" id="btn-back" type="button">
				<i class="fas fa-angle-double-left"></i>Back
			</button>
			<button class="btn btn-m success" id="btn-${param.action}" type="button">
				<i class="fas fa-plus"></i><span class="up-first">${param.action}</span>
			</button>
			<button class="btn btn-m warning" type="reset">
				<i class="fas fa-undo"></i>Reset
			</button>
		</footer>
	</form>
</main>