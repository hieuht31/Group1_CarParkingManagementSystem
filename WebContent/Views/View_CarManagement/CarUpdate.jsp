<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update Car</title>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.0/css/all.min.css"
	integrity="sha512-3PN6gfRNZEX4YFyz+sIyTF6pGlQiryJu9NlGhu9LrLMQ7eDjNgudQoFDK3WSNAayeIKc6B8WXXpo4a7HqxjKwg=="
	crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="../resourse/css/common-style.css">
<link rel="stylesheet" href="../resourse/css/form-style.css">
</head>
<body>

	<jsp:include page="../../Components/confirm-box.jsp" />
	<jsp:include page="../../Components/header.jsp">
		<jsp:param value="Car manager" name="logo" />
		<jsp:param value="fas fa-car" name="classes" />
		<jsp:param name="userName" value="Parking staff" />
	</jsp:include>
	<jsp:include page="../../Components/nav-left-aside-service.jsp" />
	<jsp:include page="resourse/CarFormMain.jsp">
		<jsp:param name="action" value="update" />
		<jsp:param value="${listCars}" name="listCars" />
		<jsp:param value="${listParkingLot}" name="listParkingLot" />
		<jsp:param value="${car}" name="car" />
	</jsp:include>
	<jsp:include page="../../Components/message-container.jsp">
		<jsp:param name="successMessage" value="${successMessage}" />
		<jsp:param name="warningMessage" value="${warningMessage}" />
		<jsp:param name="errorMessage" value="${errorMessage}" />
	</jsp:include>
	<jsp:include page="../../Components/scriptLink.jsp"></jsp:include>
	<script src="../resourse/js/form-validator.js"></script>
	<script src="../resourse/js/form-handler.js"></script>
	<script src="../resourse/js/repository.js"></script>
</body>
</html>