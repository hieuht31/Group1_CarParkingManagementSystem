<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!--<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content= "width=device-width, initial-scale=1.0">-->
<title>Car List</title>
<jsp:include page="../../Components/headLink.jsp"></jsp:include>
<link rel="stylesheet" href="../resourse/css/common-style.css">
</head>
<body>
	<jsp:include page="../../Components/confirm-box.jsp" />
	<jsp:include page="../../Components/header.jsp">
		<jsp:param value="Car manager" name="logo" />
		<jsp:param value="fas fas fa-car" name="classes" />
		<jsp:param name="userName" value="Parking staff" />
	</jsp:include>

	<c:if test="${employee.department.department_id == 1}"> 
		<jsp:include page="../../Components/nav-left-aside-admin.jsp">
			<jsp:param value="4" name="currentIndex"/>
		</jsp:include>	
	</c:if>
	<c:if test="${employee.department.department_id == 3}">
		<jsp:include page="../../Components/nav-left-aside-parking.jsp">
			<jsp:param value="2" name="currentIndex" />
		</jsp:include>	
	</c:if>

	<jsp:include page="resourse/CarViewMain.jsp" />
	<jsp:include page="../../Components/scriptLink.jsp"></jsp:include>
	<script src="../resourse/js/fetchAPI.js"></script>
	<script src="../resourse/js/form-handler.js"></script>
	<script src="../resourse/js/repository.js"></script>

</body>
</html>