<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.0/css/all.min.css"
	integrity="sha512-3PN6gfRNZEX4YFyz+sIyTF6pGlQiryJu9NlGhu9LrLMQ7eDjNgudQoFDK3WSNAayeIKc6B8WXXpo4a7HqxjKwg=="
	crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="../resourse/css/common-style.css">
<link rel="stylesheet" href="../resourse/css/form-style.css">
</head>
<body>

	<jsp:include page="../../Components/confirm-box.jsp" />
	<jsp:include page="../../Components/header.jsp">
		<jsp:param value="Car manager" name="logo" />
		<jsp:param value="fas fa-car" name="classes" />
		<jsp:param name="userName" value="Parking staff" />
	</jsp:include>
	<c:if test="${employee.department.department_id == 1}"> 
		<jsp:include page="../../Components/nav-left-aside-admin.jsp">
			<jsp:param value="5" name="currentIndex"/>
		</jsp:include>	
	</c:if>
	<c:if test="${employee.department.department_id == 3}">
		<jsp:include page="../../Components/nav-left-aside-parking.jsp">
			<jsp:param value="3" name="currentIndex" />
		</jsp:include>	
	</c:if>

	<jsp:include page="resourse/CarFormMain.jsp">
		<jsp:param name="action" value="add" />
		<jsp:param name="listParkingLot" value="${listParkingLot}" />
	</jsp:include>

	<jsp:include page="../../Components/message-container.jsp">
		<jsp:param name="successMessage" value="${successMessage}" />
		<jsp:param name="warningMessage" value="${warningMessage}" />
		<jsp:param name="errorMessage" value="${errorMessage}" />
	</jsp:include>
	<jsp:include page="../../Components/scriptLink.jsp"></jsp:include>
	<script src="../resourse/js/form-validator.js"></script>
	<script src="../resourse/js/form-handler.js"></script>
	<script src="../resourse/js/repository.js"></script>
</body>
</html>