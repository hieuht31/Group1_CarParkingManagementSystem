<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<main>
	<h1><span class="up-first">${param.action}</span> Car</h1>
	<hr>
	<form action="${param.action}" method="post" id="form"> 
		<c:if test="${requestScope.car != null}">
			<input type="hidden" value="${requestScope.car.car_id}"
				name="car_id" />
		</c:if>
		
		<div class="input-box ">
			<label for="license_plate">License plate<span>(*)</span></label> <input
				type="text" id="license_plate" name="license_plate"
				<c:if test="${requestScope.car!= null}">value="${requestScope.car.license_plate}"</c:if>
				required>
		</div> 
		<div class="input-box ">
			<label for="car_type">Car type<span>(*)</span></label> <input
				type="text" id="car_type" name="car_type"
				pattern="^[a-zA-Z]{2,}[a-zA-Z\s]{1,58}\s*$"
				<c:if test="${requestScope.car!= null}">value="${car.car_type}"</c:if>
				required>
		</div>
		<div class="input-box ">
			<label for="car_color">Car color<span>(*)</span></label> <input
				type="text" id="car_color" name="car_color"
				pattern="^[a-zA-Z]{2,}[a-zA-Z\s]{1,48}\s*$"
				<c:if test="${requestScope.car!= null}">value="${car.car_color}"</c:if>
				required>
			<p class="error-message">Car color is invalid</p>
		</div>
		<div class="input-box ">
			<label for="car_color">Company<span>(*)</span></label> <input
				type="text" id="company" name="company"
				pattern="^[a-zA-Z]{2,}[a-zA-Z\s]{1,48}\s*$"
				<c:if test="${requestScope.car!= null}">value="${car.company}"</c:if>
				required>
		</div>
		<div class="input-box">
			<label for="park">Paking lot<span>(*)</span></label> <select
				name="parkingLot" id="park" required>
				<c:if test="${requestScope.car != null}">
					<option value="${requestScope.car.parkingLot.park_id}">${requestScope.car.parkingLot.park_name}</option>
				</c:if>
				<c:forEach var="parkingLot" varStatus="loop" items="${requestScope.listParkingLot}">
					<option value="${parkingLot.park_id}">
					${parkingLot.park_name}
					</option>
				</c:forEach>
			</select>
		</div> 
		<footer>
			<button class="btn btn-m" id="btn-back" type="button">
				<i class="fas fa-angle-double-left"></i>Back
			</button>
			<button class="btn btn-m success" id="btn-${param.action}" type="button">
				<i class="fas fa-plus"></i><span class="up-first">${param.action}</span>
			</button>
			<button class="btn btn-m warning" type="reset">
				<i class="fas fa-undo"></i>Reset
			</button>
		</footer>
	</form>
</main>