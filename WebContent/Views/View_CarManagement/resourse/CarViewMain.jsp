<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<main>
	<h1>Car List</h1>
	<hr>
	<div class="search-container">
		<form action="#" method="post">
			<div class="search-field search-group">
				<i class="fas fa-search"></i> <input type="search"
					name="searchString" value="">
			</div>
			<div class="filter-field search-group">
				<label for="filter-type"><i class="fas fa-filter"></i>Filter
					by</label><select class="select-text-overflow" name="filterBy"
					id="filter-type" data-selected="">
					<option value="license_plate">License Plate</option>
					<option value="car_color">Car Color</option>
					<option value="car_type">Car Type</option>
					<option value="company">Company</option>
					<option value="park_name">Parking Lot</option>
				</select>
			</div>
			<button class="filter-submit" onclick="filter()" type="button">Search</button>

		</form>
	</div>

	<table id="table-result">
		<thead>
			<tr>
				<td>License plate</td>
				<td>Car type</td>
				<td>Car color</td>
				<td>Company</td>
				<td>Parking lot</td>
				<td>Action</td>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	<div class="table-pagination" style="margin-top: 50px;">
		<ul class="pagination" id="pagination">
		</ul>
	</div>


</main>