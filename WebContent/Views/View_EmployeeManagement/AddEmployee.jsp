<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Employee</title>
<jsp:include page="../../Components/headLink.jsp"></jsp:include>
<link rel="stylesheet" href="../resourse/css/common-style.css">
<link rel="stylesheet" href="../resourse/css/form-style.css">
<link rel="stylesheet" href="../resourse/css/add-employee.css">
</head>
<body>
	<jsp:include page="../../Components/confirm-box.jsp" />
	<jsp:include page="../../Components/header.jsp">
		<jsp:param value="Employee" name="logo" />
		<jsp:param value="fas fa-users" name="classes" />
	</jsp:include>
	<c:if test="${employee.department.department_id == 1}"> 
		<jsp:include page="../../Components/nav-left-aside-admin.jsp">
			<jsp:param value="2" name="currentIndex"/>
		</jsp:include>	
	</c:if>
	<c:if test="${employee.department.department_id == 2}">
		<jsp:include page="../../Components/nav-left-aside-hr.jsp">
			<jsp:param value="2" name="currentIndex" />
		</jsp:include>	
	</c:if>
	<main>
	<p
		style="font-size: 30px; border-bottom: 1px solid rgb(135 135 135/ 20%); padding-bottom: 15px;">Add
		Employee</p>
	<form method="post" id="form">
		<div class="input-box">
			<label for="fullName">Full Name<span>(*)</span></label>
			<div class="input-element">
				<input placeholder="Full name" type="text" id="fullName" name="fullName" required>
			</div>

		</div>
		<div class="input-box">
			<label for="phoneNumber">Phone Number<span>(*)</span></label>
			<div class="input-element">
				<input placeholder="Phone number" type="text" id="phoneNumber" name="phoneNumber" required>
			</div>
		</div>
		<div class="input-box">
			<label for="dob">Date Of Birth<span>(*)</span></label>
			<div class="input-element">
				<input type="date" id="dob" name="dateOfBirth" required>
			</div>
		</div>
		<div class="input-box">
			<label for="sex">Sex<span>(*)</span></label>
			<div class="input-element">
				<div class="sex-option">
					<input type="radio" id="male" value="0" name="sex" checked><label
						for="male">Male</label> <input type="radio" id="female" value="1"
						name="sex"><label for="female">Female</label>
				</div>
			</div>
		</div>
		<div class="input-box">
			<label for="address">Address</label>
			<div class="input-element">
				<input placeholder="Address" type="text" id="address" name="address" />
			</div>
		</div>
		<div class="input-box">
			<label for="email">Email</label>
			<div class="input-element">
				<input placeholder="email" type="email" id="email" name="email" autocomplete="false"/>
			</div>
		</div>
		<div class="input-box">
			<label for="account">Account<span>(*)</span></label>
			<div class="input-element">
				<input placeholder="Account" type="text" id="account" name="account" required>
			</div>
		</div>
		<div class="input-box">
			<label for="password">Password<span>(*)</span></label>
			<div class="input-element">
				<input placeholder="Password" type="password" id="password" name="password" required autocomplete="false">
				<i class="far fa-eye-slash" id="icon-eye" style="display:block;cursor: pointer;"></i>
			</div>
		</div>
		<div class="input-box">
			<label for="deparment">Deparment<span>(*)</span></label>
			<div class="input-element">
				<select name="department" id="deparment" required>
					<c:forEach items="${departments}" var="d">
						<c:if test="${sessionScope.employee.department.department_id <= d.department_id}">
							<option value="${d.getDepartment_id()}">${d.getDepartment_name()}</option>
						</c:if>
					</c:forEach>
				</select>
			</div>
		</div>
		<footer>
			<button class="btn btn-m primary" type="button" id="btn-back">
				<i class="fas fa-angle-double-left"></i>Back
			</button>
			<button class="btn btn-m warning" type="reset" id="btn-reset">
				<i class="fas fa-undo"></i>Reset
			</button>
			<button class="btn btn-m success" type="submit" id="btn-add">
				<i class="fas fa-plus"></i>Add
			</button>
		</footer>
	</form>
	</main>


	<jsp:include page="../../Components/scriptLink.jsp"></jsp:include>
	<script src="../resourse/js/employee-action.js"></script>
	<script src="../resourse/js/repository.js"></script>
</body>
</html>