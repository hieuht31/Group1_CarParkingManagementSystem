<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update Employee</title>
<jsp:include page="../../Components/headLink.jsp"></jsp:include>
<link rel="stylesheet" href="../resourse/css/common-style.css">
<link rel="stylesheet" href="../resourse/css/form-style.css">
<link rel="stylesheet" href="../resourse/css/add-employee.css">
</head>
<body>
	<jsp:include page="../../Components/confirm-box.jsp" />

	<c:choose>
		<c:when test="${sessionScope.employee.department.department_id == 1}">
			<jsp:include page="../../Components/header.jsp">
				<jsp:param value="Employee" name="logo" />
				<jsp:param value="fas fa-users" name="classes" />
			</jsp:include>
			<jsp:include page="../../Components/nav-left-aside-admin.jsp" />
		</c:when>
		<c:when test="${sessionScope.employee.department.department_id == 2}">
			<jsp:include page="../../Components/header.jsp">
				<jsp:param value="Employee" name="logo" />
				<jsp:param value="fas fa-users" name="classes" />
			</jsp:include>
			<jsp:include page="../../Components/nav-left-aside-hr.jsp" />
		</c:when>
		<c:when test="${sessionScope.employee.department.department_id == 3}">
			<jsp:include page="../../Components/header.jsp">
				<jsp:param value="Booking Office" name="logo" />
				<jsp:param value="fas fa-cart-plus" name="classes" />
			</jsp:include>
			<jsp:include page="../../Components/nav-left-aside-parking.jsp" />
		</c:when>
		<c:when test="${sessionScope.employee.department.department_id == 4}">
			<<jsp:include page="../../Components/header.jsp">
				<jsp:param value="Trip" name="logo" />
				<jsp:param value="fas fa-plane rotate" name="classes" />
			</jsp:include>
			<jsp:include page="../../Components/nav-left-aside-service.jsp" />
		</c:when>
	</c:choose>
	<main>
		<c:if test="${employee == null}">
	Khong co employee dau
	<button class="btn btn-m primary" type="button" id="btn-back">
				<i class="fas fa-angle-double-left"></i>Back
			</button>
		</c:if>
		<c:if test="${employee != null}">
			<p
				style="font-size: 30px; border-bottom: 1px solid rgb(135 135 135/ 20%); padding-bottom: 15px;">Update
				Employee</p>
			<form method="post" id="form">
				<div class="input-box" style="display: none;">
					<input type="text" value="${employee.getEmployee_Id()}"
						name="employee_id" id="employee_id">
				</div>
				<div class="input-box">
					<label for="fullName">Full Name<span>(*)</span></label>
					<div class="input-element">
						<input placeholder="Full name" type="text" id="fullName" name="fullName" required
							value="${employee.getEmployee_name()}">
					</div>

				</div>
				<div class="input-box">
					<label for="phoneNumber">Phone Number<span>(*)</span></label>
					<div class="input-element">
						<input placeholder="Phone number" type="text" id="phoneNumber" name="phoneNumber" required
							value="${employee.getEmployee_phone()}">
					</div>
				</div>
				<div class="input-box">
					<label for="dob">Date Of Birth<span>(*)</span></label>
					<div class="input-element">
						<input type="date" id="dob" name="dateOfBirth" required
							value="${employee.getEmployee_birthdate()}">
					</div>
				</div>
				<div class="input-box">
					<label for="sex">Sex<span>(*)</span></label>
					<div class="input-element">
						<div class="sex-option">
							<input type="radio" id="male" value="0" name="sex"
								${employee.isSex() == false? 'checked' : ''}><label
								for="male">Male</label> <input type="radio" id="female"
								value="1" ${employee.isSex() == true ? 'checked' : ''}
								name="sex"><label for="female">Female</label>
						</div>
					</div>
				</div>
				<div class="input-box">
					<label for="address">Address</label>
					<div class="input-element">
						<input type="text" id="address" name="address"
							value="${employee.getEmployee_address()}" />
					</div>
				</div>
				<div class="input-box">
					<label for="email">Email</label>
					<div class="input-element">
						<input placeholder="Email" type="email" id="email" name="email" autocomplete="false"
							value="${employee.getEmployee_email()}" />
					</div>
				</div>
				<div class="input-box">
					<label for="account">Account<span>(*)</span></label>
					<div class="input-element">
						<input placeholder="Account" type="text" id="account" name="account" required
							value="${employee.getAccount_name()}">
					</div>
				</div>
				<div class="input-box">
					<label for="password">Password<span>(*)</span></label>
					<div class="input-element">
						<input type="password" id="password" name="password" required
							autocomplete="false" value="${employee.getPassword()}"> <i
							class="far fa-eye-slash" id="icon-eye"
							style="display: block; cursor: pointer;"></i>

					</div>
				</div>
				<div class="input-box">
					<label for="deparment">Deparment<span>(*)</span></label>
					<div class="input-element">
						<select name="department" id="deparment" required>
						<%-- if employee view profile or hr open profile of admin just show current department --%>
							<c:if test="${sessionScope.employee.department.department_id>2||sessionScope.employee.department.department_id>employee.department.department_id}">
								<option value="${employee.department.department_id}">${employee.department.department_name}</option>
							</c:if>
							<c:forEach items="${departments}" var="d">
							<%-- just show department have author edit --%>
								<c:if
									test="${sessionScope.employee.department.department_id<=employee.department.department_id&&sessionScope.employee.department.department_id<=d.department_id}">
									<option value="${d.getDepartment_id()}"
										${employee.getDepartment().getDepartment_id() == d.getDepartment_id() ? 'selected' : ''}>${d.getDepartment_name()}</option>
								</c:if>
							</c:forEach>
						</select>
					</div>
				</div>
				</div>
				<footer style="width: 600px;">
					<button class="btn btn-m primary" type="button" id="btn-back">
						<i class="fas fa-angle-double-left"></i>Back
					</button>
					<button class="btn btn-m warning" type="reset" id="btn-reset">
						<i class="fas fa-undo"></i>Reset
					</button>
					<button class="btn btn-m success" type="submit" id="btn-update">
						<i class="fas fa-plus"></i>Update
					</button>
					<button class="btn btn-m btn-info " disabled="disabled"
						style="color: white;" type="submit" id="btn-reset-password">
						<i class="fa fa-lock icon "></i>Reset password
					</button>
					<button class="btn btn-m danger" type="submit" id="btn-delete">
						<i class="fas fa-trash"></i>Delete
					</button>
				</footer>
			</form>
		</c:if>
	</main>

	<jsp:include page="../../Components/scriptLink.jsp"></jsp:include>
	<script src="../resourse/js/employee-action.js"></script>
	<script src="../resourse/js/repository.js"></script>

</body>
</html>