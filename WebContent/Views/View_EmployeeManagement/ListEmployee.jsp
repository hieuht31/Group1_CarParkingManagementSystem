<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Employee</title>
<jsp:include page="../../Components/headLink.jsp"></jsp:include>
<link rel="stylesheet" href="../resourse/css/common-style.css">
<link rel="stylesheet" href="../resourse/css/form-style.css">
<style>
.filter-content {
	display: flex;
	justify-content: flex-end;
	margin-right: 74px;
}

.search-group {
	margin-right: 20px;
}

.search-field:first-child {
	width: 35%;
}

nav>ul {
	margin-left: 0px;
}
</style>
</head>
<body>
	<jsp:include page="../../Components/confirm-box.jsp" />
	<jsp:include page="../../Components/header.jsp">
		<jsp:param value="Employee" name="logo" />
		<jsp:param value="fas fa-users" name="classes" />
		<jsp:param name="userName" value="HR staff" />
	</jsp:include>
	<c:if test="${employee.department.department_id == 1}"> 
		<jsp:include page="../../Components/nav-left-aside-admin.jsp">
			<jsp:param value="1" name="currentIndex"/>
		</jsp:include>	
	</c:if>
	<c:if test="${employee.department.department_id == 2}">
		<jsp:include page="../../Components/nav-left-aside-hr.jsp">
			<jsp:param value="1" name="currentIndex" />
		</jsp:include>	
	</c:if>
	<main>
		<h1>List Employee</h1>
		<hr>
		<div class="search-container">
			<div class="filter-content">
				<div class="search-field search-group">
					<i class="fas fa-search" style="z-index: 1; cursor: pointer;"
						onclick="filter()"></i> <input type="search"
						style="margin-bottom: 0px;" name="ticketSearch">
				</div>
				<div class="filter-field search-group">
					<label for="filter-type"><i class="fas fa-filter"></i>Filter
						by</label> <select name="filterType" id="filter-type"
						style="margin-bottom: 0px;">
						<option value="employee_name">Name</option>
						<option value="employee_birthdate" data-type='date'>DateOfBirth</option>
						<option value="employee_address">Address</option>
						<option value="employee_phone" data-type='phone'>Phone</option>
						<option value="department">Department</option>
					</select>
				</div>
				<button class="filter-submit" onclick="filter()" type="submit"
					style="cursor: pointer;">Search</button>
			</div>

		</div>
		<table id="table-result" style="margin-top: 20px;">
			<thead>
				<tr>
					<td>ID</td>
					<td>Name</td>
					<td>Date of birth</td>
					<td>Address</td>
					<td>Phone number</td>
					<td>Department</td>
					<td>Action</td>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
		<div class="table-pagination" style="margin-top: 50px;">
			<ul class="pagination" id="pagination">
			</ul>
		</div>
	</main>
	<jsp:include page="../../Components/scriptLink.jsp"></jsp:include>
	<script src="../resourse/js/list-employee.js"></script>
	<script src="../resourse/js/repository.js"></script>
</body>
</html>