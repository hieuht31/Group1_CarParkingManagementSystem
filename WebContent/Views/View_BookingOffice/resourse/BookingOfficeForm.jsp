<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<main>
	<h1><span class="up-first">${param.action}</span> Booking Office</h1>
	<hr>
	<form id="form" action="${param.action}" method="post">
		<c:if test="${requestScope.office != null}"><input type="hidden" value="${requestScope.office.office_id}" name="office_id"/></c:if>
		<div class="input-box input-box-l">
			<label for="office_name">Booking office name<span>(*)</span></label> <input
				type="text" id="office_name" name="office_name" placeholder="Office name"
				<c:if test="${requestScope.office != null}">value="${office.office_name}"</c:if> required>
		</div>
		<div class="input-box input-box-l">
			<label for="trip">Trip<span>(*)</span></label> <select name="trip"
				id="trip" required>
				<c:if test="${requestScope.ticket != null}">
					<option value="${requestScope.ticket.trip.trip_id}" hidden>${ticket.trip.destination}</option>
				</c:if>
				<%----%>
				<c:forEach var="trip" items="${requestScope.listTrip}">
					<option value="${trip.trip_id}">${trip.destination}</option>
				</c:forEach>
			</select>
			<p class="error-message">a</p>
		</div>
		<div class="input-box input-box-l">
                <label for="phoneNumber">Phone Number<span>(*)</span></label>
                <input type="text" id="phoneNumber" name="phoneNumber" required <c:if test="${requestScope.office != null}">value="${requestScope.office.office_phone}" </c:if> placeholder="Phone number">
        </div>
        <div class="input-box input-box-l">
			<label for="place">Place<span>(*)</span></label> <input type="text" placeholder="Office place" 
				name="place" id="place"
				<c:if test="${requestScope.office != null}">value="${requestScope.office.office_place}" </c:if>
				 required placeholder="Place">
		</div>
		<div class="input-box input-box-l" data-unit="(VNĐ)">
			<label for="price">Price<span>(*)</span></label> <input	type="number" id="price" name="office_price"
			 <c:if test="${requestScope.office!= null}">value="${office.office_price}"</c:if> placeholder="Price" required>
		</div>
		<div class="input-box input-box-l">
			<label for="#">Contract deadline<span>(*)</span></label>
			<div>
				<div class="group-label">
					<label for="fromDate">From date</label><input type="date" name="fromDate"
						id="fromDate" <c:if test="${requestScope.office != null}">value="${requestScope.office.start_contract_deadline}" </c:if>>
				</div>
				<div class="group-label">
					<label for="toDate">To date</label> <input type="date" name="toDate"
						id="toDate" required <c:if test="${requestScope.office != null}">value="${requestScope.office.end_contract_deadline}" </c:if>>
				</div>
			</div>
		</div>
		<footer>
			<button class="btn btn-m" id="btn-back" type="button">
				<i class="fas fa-angle-double-left"></i>Back
			</button>
			<button class="btn btn-m success" id="btn-${param.action}" type="button">
				<i class="fas fa-plus"></i><span class="up-first">${param.action}</span>
			</button>
			<button class="btn btn-m warning" type="reset">
				<i class="fas fa-undo"></i>Reset
			</button>
		</footer>
	</form>
</main>