<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Booking view</title>
<jsp:include page="../../Components/headLink.jsp"></jsp:include>
<link rel="stylesheet" href="../resourse/css/common-style.css">
</head>
<body>
	<jsp:include page="../../Components/confirm-box.jsp" />
	<jsp:include page="../../Components/header.jsp">
		<jsp:param value="Booking Office" name="logo" />
		<jsp:param value="fas fa-cart-plus" name="classes" />
		<jsp:param name="userName" value="Parking staff" />
	</jsp:include>
	<c:if test="${employee.department.department_id == 1}"> 
		<jsp:include page="../../Components/nav-left-aside-admin.jsp">
			<jsp:param value="6" name="currentIndex"/>
		</jsp:include>	
	</c:if>
	<c:if test="${employee.department.department_id == 3}">
		<jsp:include page="../../Components/nav-left-aside-parking.jsp">
			<jsp:param value="4" name="currentIndex" />
		</jsp:include>	
	</c:if>

	<jsp:include page="resourse/BookingOfficeViewMain.jsp">
		<jsp:param value="${searchString}" name="searchString" />
		<jsp:param value="${filterBy}" name="filterBy" />
		<jsp:param name="currentPage" value="${currentPage}" />
		<jsp:param name="numberPage" value="${numberPage}" />
		<jsp:param name="ticketList" value="${listBooking}" />
		<jsp:param name="server" value="list" />
	</jsp:include>
	<jsp:include page="../../Components/message-container.jsp">
		<jsp:param name="successMessage" value="${successMessage}" />
		<jsp:param name="warningMessage" value="${warningMessage}" />
		<jsp:param name="errorMessage" value="${errorMessage}" />
	</jsp:include>
	<jsp:include page="../../Components/scriptLink.jsp"></jsp:include>
	<script src="../resourse/js/fetchAPI.js"></script>
	<script src="../resourse/js/form-handler.js"></script>
<script src="../resourse/js/repository.js"></script>
</body>
</html>