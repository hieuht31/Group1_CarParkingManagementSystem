<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<main>
	<h1>Trip ${param.action}</h1>
	<hr>
	<form action="${param.action}" method="post" id="form">
		<c:if test="${requestScope.trip != null}">
			<input type="hidden" value="${requestScope.trip.trip_id}"
				name="trip_id" />
		</c:if>
		<div class="input-box">
			<label for="destination">Destination<span>(*)</span></label> <input
				type="text" id="destination" name="destination"
				<c:if test="${requestScope.trip != null}">value="${trip.destination}"</c:if>
				required placeholder="Destination" >
		</div>
		<div class="input-box">
			<label for="departure_time">Departure time<span>(*)</span></label> <input
				type="time" id="departure_time" name="departure_time"
				<c:if test="${requestScope.trip != null}">value="${trip.departure_time}"</c:if>
				required>
		</div>
		<div class="input-box">
			<label for="driver">Driver<span>(*)</span></label> <input
				type="text" id="driver" name="driver"
				<c:if test="${requestScope.trip != null}">value="${trip.driver}"</c:if>
				required placeholder="Driver">
		</div>
		<div class="input-box">
			<label for="car_type">Car type<span>(*)</span></label> <input
				type="text" id="car_type" name="car_type"
				<c:if test="${requestScope.trip != null}">value="${trip.car_type}"</c:if>
				required placeholder="Car type">
		</div>
		<div class="input-box">
			<label for="maximum_number">Maximum online ticket number<span>(*)</span></label> <input
				type="number" id="maximum_number" name="maximum_online_ticket_number"
				<c:if test="${requestScope.trip != null}">value="${trip.maximum_online_ticket_number}"</c:if>
				required placeholder="Max online ticket number">
		</div>
		<div class="input-box">
			<label for="departure_date">Departure date<span>(*)</span></label> <input
				type="date" id="departure_date" name="departure_date"
				<c:if test="${requestScope.trip != null}">value="${trip.departure_date}"</c:if>
				required >
		</div>
		<footer>
			<button class="btn btn-m warning" type="reset">
				<i class="fas fa-undo"></i>Reset
			</button>
			<button class="btn btn-m success" type="button" id="btn-${param.action}">
				<i class="fas fa-plus"></i><span class="up-first">${param.action}</span>
			</button>
			<button class="btn btn-m danger" type="button" id="btn-back">
				<i class="fas fa-times"></i>Cancel
			</button>
		</footer>
	</form>
</main>