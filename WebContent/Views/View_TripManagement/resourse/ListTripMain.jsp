<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<main>
	<h1>Trip List</h1>
	<hr>
	<div class="search-container">
		<form action="#" method="post">
			<div class="search-field search-group">
				<i class="fas fa-search"></i> <input type="search"
					name="searchString" value="">
			</div>
			<div class="filter-field search-group">
				<label for="filter-type"><i class="fas fa-filter"></i>Filter
					by</label><select class="select-text-overflow" name="filterBy"
					id="filter-type" data-selected="">
					<option value="destination">Destination</option>
					<option value="departure_time" data-type='time'>Departure time</option>
					<option value="driver">Driver</option>
					<option value="car_type">Car type</option>
					<option value="booked_ticket_number" data-type='number'>Booked number</option>
				</select>
			</div>
			<button class="filter-submit" onclick="filter()" type="button">Search</button>
			<div class="date-filter">
				<input type="date" name="date-filter" value="" />
			</div>
		</form>
	</div>
	<table id="table-result">
		<thead>
			<tr>
				<td>No</td>
				<td>Destination</td>
				<td>Departure date</td>
				<td>Departure time</td>
				<td>Driver</td>
				<td>Car type</td>
				<td><span title= "Booked ticket number">Booked ticket number</span></td>
				<td>Action</td>
			</tr>
		</thead>
		<tbody>

		</tbody>
	</table>
	<div class="table-pagination" style="margin-top: 50px;">
		<ul class="pagination" id="pagination">
		</ul>
	</div>
</main>