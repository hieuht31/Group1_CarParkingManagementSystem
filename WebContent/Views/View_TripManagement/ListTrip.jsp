<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Employee</title>
<jsp:include page="../../Components/headLink.jsp"></jsp:include>
<link rel="stylesheet" href="../resourse/css/common-style.css">

</head>
<body>
	<jsp:include page="../../Components/confirm-box.jsp" />
	<jsp:include page="../../Components/header.jsp">
		<jsp:param value="Trip" name="logo" />
		<jsp:param value="fas fa-plane rotate" name="classes" />
		<jsp:param name="userName" value="Service staff" />
	</jsp:include>
	<c:if test="${employee.department.department_id == 1}"> 
		<jsp:include page="../../Components/nav-left-aside-admin.jsp">
			<jsp:param value="10" name="currentIndex"/>
		</jsp:include>	
	</c:if>
	<c:if test="${employee.department.department_id ==4}">
		<jsp:include page="../../Components/nav-left-aside-service.jsp">
			<jsp:param value="0" name="currentIndex" />
		</jsp:include>	
	</c:if>
	<jsp:include page="resourse/ListTripMain.jsp" />
	<jsp:include page="../../Components/scriptLink.jsp"></jsp:include>
	<script src="../resourse/js/fetchAPI.js"></script>
	<script src="../resourse/js/form-handler.js"></script>
	<script src="../resourse/js/repository.js"></script>
</body>
</html>