<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<%@ include file="../../Components/headLink.jsp"%>

<link rel="stylesheet" href="./resourse/css/toast.css">
<link rel="stylesheet" href="./resourse/css/login.css">

<!--===============================================================================================-->
</head>
<body>
	<div class="limiter">
		<div class="container-background"></div>
		<div class="container-login">
			<div class="wrap-login">
				<form class="login-form" id="login-form" method="post">
					 <span class="login-form-title">
						Sign in </span>
					<div class="wrap-input">
						<div class="input-icon">
							<i class="fa fa-envelope icon" aria-hidden="true"></i> <input 
								class="input h-50" type="text" name="account" placeholder="Account name">
						</div>

					</div>

					<div class="wrap-input">
						<div class="input-icon">
							<i class="fa fa-lock icon" aria-hidden="true"></i> <input
								class="input" type="password" name="password"
								placeholder="Password">
						</div>

					</div>
					<div class="remember-input">
						<input type="checkbox" name="remember" id="remember">
						<label>Remember
							me</label>
					</div>
					<div id="login-error"></div>
					<div>
						<button class="btn btn-primary">Login</button>
					</div>

					<div class="text-center "
						style="margin-top: 20px; margin-bottom: 30px;">
						<a href="#"> Create your Account <i
							class="fa fa-long-arrow-right " aria-hidden="true"></i>
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<jsp:include page="../../Components/message-container.jsp"/>

	<%@ include file="../../Components/scriptLink.jsp"%>
	<script src="resourse/js/common.js"></script>
	<script src="<%=request.getContextPath()%>/resourse/js/login.js"></script>
</body>
</html>