/**
 * 
 */
// method validate phone number
jQuery.validator.addMethod("phoneValid", function(value, element) {
	return this.optional(element) || /^[0][0-9]{9}$/.test(value);
}, "Invalid phone number");
// method validate password
jQuery.validator.addMethod("passwordValid", function(value, element) {
	return this.optional(element) || /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,50}$/.test(value);
}, "Invalid password");
// method validate date of birth
$.validator.addMethod("maxDate", function(value, element) {
	var curDate = new Date();
	var inputDate = new Date(value);
	if (inputDate < curDate) {
		return true;
	}
	return false;
}, "Date must be less than or equal today")
$.validator.addMethod("is18", function(value, element) {
	var curDate = new Date();
	var inputDate = new Date(value);
	var age = curDate.getFullYear() - inputDate.getFullYear()
	var m = curDate.getMonth() - inputDate.getMonth()
	var day = curDate.getDate() - inputDate.getDate()
	if(age<0) return false;
	if (m<0 || m==0 && day <0) {
		age--
	}
	if(age<18) return false;
	return true;
}, "Age must greater 18")
// method validate text field
$.validator.addMethod("stringValidate", function(value, element) {
	return this.optional(element) || /^(?=.*[a-zA-Z])[a-zA-Z][a-zA-Z\d\s]{2,49}$/.test(value);
}, "Input value just contain alpha character,digital and has length form 3 - 50");
// method validate name field
$.validator.addMethod("nameValidate", function(value, element) {
	return this.optional(element) || /^(?=.*[a-zA-Z])[a-zA-Z][a-zA-Z\d]{2,49}$/.test(value);
}, "Input value just contain alpha character and has length form 3 - 50");
$.validator.addMethod("validDate", function(value, element) {
	console.log(value >= $("#fromDate").val())
	return this.optional(element) || (value >= $("#fromDate").val())
}, "To date must greater from date");
$.validator.addMethod("fillFromDate", function(value, element) {
	return this.optional(element) || ($("#fromDate").val()=='')==false
}, "Please input from date");
$.validator.addMethod("isPositive", function(value, element) {
	return this.optional(element) || (Number(value) >= 0)
}, "Please input positive number");
$.validator.addMethod("licensePlateValidate", function(value, element) {
	return this.optional(element) || /^\d{2}[a-zA-Z]-\d{3}\.\d{2}$/.test(value)
}, "Please input valid license plate (Ex: 29A-300.00)");
$("#form").validate({
	onfocusout: false,
	onkeyup: false,
	onclick: false,
	rules: {
		"fullName": {
			required: true,
			maxlength: 50
		},
		"phoneNumber": {
			required: true,
			phoneValid: true
		},
		"dateOfBirth": {
			required: true,
			maxDate: true,
			is18:true
		},
		"sex": {
			required: true
		},
		"address": {
			maxlength: 50
		},
		"email": {
			email: true,
			maxlength: 50
		},
		"account": {
			required: true,
			maxlength: 50
		},
		"password": {
			required: true,
			passwordValid: true
		},
		"department": {
			required: true
		},
		"office_name": {
			required: true,
			stringValidate: true
		},
		"trip": {
			required: true
		},
		"place": {
			required: true,
			stringValidate: true,
		},
		"office_price": {
			required: true,
			number: true
		},
		"toDate": {
			fillFromDate: true,
			required: true,
			validDate: true
		},
		// parking lot add form 
		"park_name": {
			required: true,
			stringValidate: true
		},
		"park_place": {
			required: true,
			stringValidate: true
		},
		"office_area": {
			required: true,
			number: true,
			isPositive: true
		},
		"park_price": {
			required: true,
			number: true,
			isPositive: true
		},
		"max_park_slot": {
			required: true,
			number: true,
			isPositive: true
		},
		// car add
		"license_plate": {
			required: true,
			licensePlateValidate: true
		},
		"car_type": {
			required: true,
			stringValidate: true
		},
		"car_color": {
			required: true,
			stringValidate: true
		},
		"company": {
			required: true,
			stringValidate: true
		},
		"parkingLot": {
			required: true
		},
		// trip validate
		"destination": {
			required: true,
			stringValidate: true
		},
		"departure_time": {
			required: true
		},
		"car_type": {
			required: true,
			stringValidate: true
		},
		"maximum_online_ticket_number": {
			required: true,
			number: true,
			isPositive: true
		},
		"departure_date": {
			required: true
		},
		// ticket add
		"customer": {
			required: true,
			stringValidate: true
		},
		"bookingTime": {
			required: true
		},
		"trip": {
			required: true,
		},
		"licensePlate": {
			required: true
		}
	},
	messages: {
		"fullName": {
			required: "Please enter your full name.",
			maxlength: "Full name cannot be more than 50 characters."
		},
		"phoneNumber": {
			required: "Please enter your phone number (Ex:0355928414)",
			phoneValid: "Invalid phone number (Ex:0355928414)"
		},
		"dateOfBirth": {
			required: "Please enter your date of birth.",
			maxDate: "Date of birth must be less than current date."
		},
		"sex": {
			required: "Please choose your option."
		},
		"address": {
			maxlength: "Address cannot be more than 50 characters."
		},
		"email": {
			email: "Please enter a valid email address.",
			maxlength: "Email address cannot be more than 50 characters."
		},
		"account": {
			required: "Please enter your account.",
			maxlength: "Account cannot be more than 50 characters."
		},
		"password": {
			required: "Password must include uppercase,lowercase and number.",
			passwordValid: "Password must include uppercase,lowercase and number."
		},
		"department": {
			required: "Please choose your option."
		},
		// booking form
		"office_name": {
			required: "Please input office name",
			stringValidate: "Office name just content 50 non-specials characters"
		},
		"trip": {
			required: "Please select an trip"
		},
		"place": {
			required: "Please input place value",
			stringValidate: "Place just content 50 non-specials characters"
		},
		"office_price": {
			required: "Please input price",
			number: "Please input number"
		},
		"toDate": {
			fillFromDate: "Input from date",
			required: "Please input to date",
			validDate: "To date must greater from date"
		},
		// car add
		"license_plate": {
			required: "Please input license plate",
			licensePlateValidate: "Wrong format, please input with format (Ex: 29A-300.00)"
		},
		"car_type": {
			required: "Please input car type",
			stringValidate: "Car type just contain 50 non-special characters"
		},
		"car_color": {
			required: "Please input car color",
			stringValidate: "Color just contain 50 non-special characters"
		},
		"company": {
			required: "Please input company",
			stringValidate: "Company just contain max 50 non-special characters"
		},
		"parkingLot": {
			required: "Please select parking lot"
		},
		// trip validate
		"destination": {
			requried: "Please input destination",
			stringValidate: "Destination just contain 3-50 non-special character"
		},
		"departure_time": {
			required: "Please input departure time"

		},
		"car_type": {
			required: "Please input car type",
			stringValidate: "Car type just contain 3-50 non-special"
		},
		"maximum_online_ticket_number": {
			required: "Please input max online ticket number",
			number: "Please input a number",
			isPositive: "Please input a positive number"
		},
		"departure_date": {
			required: "Please input departure date"
		},
		// ticket add
		"customer": {
			required: "Please input customer name",
			nameValidate: "Customer name just contain 3 -50 alphabet character and space"
		},
		"bookingTime": {
			required: "Please input booking time"
		},
		"trip": {
			required: "Please select trip",
		},
		"licensePlate": {
			required: "Please select car"
		}
	},
	highlight: function(element) {
		$(element).addClass('has-error invalid');
	},
	unhighlight: function(element) {
		$(element).removeClass('has-error invalid');
	},
	onfocusout: function(element) {
		this.element(element); // triggers validation
	},
	onkeyup: function(element) {
		if (!$(element).hasClass('invalid')) {
			$(element).next().remove();
		}
		this.element(element); // triggers validation
	},
});

