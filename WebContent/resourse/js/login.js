(function ($) {
	"use strict";
	$("#login-form").validate({
		onfocusout: false,
		onkeyup: false,
		onclick: false,
		rules: {
			"account": {
				required: true,
				maxlength: 50
			},
			"password": {
				required: true,
				minlength: 6
			}
		},
		messages: {
			"account": {
				required: "Please enter your account name.",
				maxlength: "Account name cannot be more than 50 characters."
			},
			"password": {
				required: "Please enter your password",
				minlength: "Password must be at least 6 characters"
			}
		},
		highlight: function (element) {
			$(element).addClass('has-error');
		},
		unhighlight: function (element) {
			$(element).removeClass('has-error');
		},
		onfocusout: function (element) {
			this.element(element); // triggers validation
		},
		onkeyup: function (element, event) {
			if ($(element).hasClass('valid')) {
				$(element).next().remove();
			}
			this.element(element); // triggers validation
		},
	});

	// call ajax to login
	$("#login-form").submit(
		function (e) {
			e.preventDefault();
			var $form = $(this);
			if ($form.valid()) {
				$.ajax({
					type: "POST",
					url: "./LoginController",
					data: $form.serialize(),
					success: function (data) {
						console.log(data);
						if (data.success&&data.data!=undefined) {
							let emp = data.data;
							if(emp.department.department_id == 1 || emp.department.department_id ==2)
							window.location.href = "./employee/list_employee";
							if(emp.department.department_id ==3)
							window.location.href = "./car/list";
							if(emp.department.department_id ==4)
							window.location.href = "./trip/list";
						} else {
							//$("#login-error").html("Invalid account or password.");
							//$("#login-error").css("color", "red");
							//$("#login-error").css("margin-bottom", "10px");
							messageRender("fe-error","Invalid account or password.")
						}
					}
				});
			}
		});

})(jQuery);