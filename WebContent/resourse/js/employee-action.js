document.querySelectorAll(".drop-nav").forEach((dropNav) => {
  dropNav.querySelector(".drop-nav-title").onclick = () => {
    dropNav.classList.toggle("drop");
  };
});


$(document).ready(function () {
  var sessionEmployee ;
  // call ajax await
  $.ajax({
    type: "POST",
    url: "../employee/employee_action",
    data: {
      action: "get-session-employee"
    },
    dataType: "json",
    async: false,
    success: function (data) {
      sessionEmployee = data.data;
    }
  });
  console.log(sessionEmployee);
   if (sessionEmployee.department.department_name == "Admin") {
     // undisable button reset password
     $("#btn-reset-password").attr("disabled", false);
     // disable account and password input
     $("#account").attr("disabled", true);
     $("#password").attr("disabled", true);
   } else {
    var employee_id = $("#employee_id").val();
    console.log("employee_id: " + employee_id);
     if (sessionEmployee.employee_Id != employee_id) {
      console.log("1");
       // disable all input
       //$("#form").find("input").attr("disabled", true);
       //$("#form").find("select").attr("disabled", true);
       // disable btn update and delete
       if(sessionEmployee.employee_Id > employee_id) $("#btn-update").attr("disabled", true);
       $("#btn-delete").attr("disabled", true);
       $("#btn-reset-password").attr("disabled", true);
     }
   }
  $("#icon-eye").click(function () {
    if ($("#password").attr("type") == "password") {
      $("#password").attr("type", "text");
      $("#icon-eye").removeClass("fa-eye-slash");
      $("#icon-eye").addClass("fa-eye");
    } else {
      $("#password").attr("type", "password");
      $("#icon-eye").removeClass("fa-eye");
      $("#icon-eye").addClass("fa-eye-slash");
    }
  });
  // method validate phone number
  jQuery.validator.addMethod("phoneValid", function (value, element) {
    return this.optional(element) || /^[0][0-9]{9}$/.test(value);
  }, "Invalid phone number");
  // method validate password
  jQuery.validator.addMethod("passwordValid", function (value, element) {
    return this.optional(element) || /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,50}$/.test(value);
  }, "Invalid password");
  // method validate date of birth
  $.validator.addMethod("maxDate", function (value, element) {
    var curDate = new Date();
    var inputDate = new Date(value);
    if (inputDate < curDate) {
      return true;
    }
    return false;
  }, "Date must be less than or equal today");
  $("#form").validate({
    onfocusout: false,
    onkeyup: false,
    onclick: false,
    rules: {
      "fullName": {
        required: true,
        maxlength: 50
      },
      "phoneNumber": {
        required: true,
        phoneValid: true
      },
      "dateOfBirth": {
        required: true,
        maxDate: true
      },
      "sex": {
        required: true
      },
      "address": {
        maxlength: 50
      },
      "email": {
        email: true,
        maxlength: 50
      },
      "account": {
        required: true,
        maxlength: 50
      },
      "password": {
        required: true,
        passwordValid: true
      },
      "department": {
        required: true
      }
    },
    messages: {
      "fullName": {
        required: "Please enter your full name.",
        maxlength: "Full name cannot be more than 50 characters."
      },
      "phoneNumber": {
        required: "Please enter your phone number (Ex:0355928414)",
        phoneValid: "Invalid phone number (Ex:0355928414)"
      },
      "dateOfBirth": {
        required: "Please enter your date of birth.",
        maxDate: "Date of birth must be less than current date."
      },
      "sex": {
        required: "Please choose your option."
      },
      "address": {
        maxlength: "Address cannot be more than 50 characters."
      },
      "email": {
        email: "Please enter a valid email address.",
        maxlength: "Email address cannot be more than 50 characters."
      },
      "account": {
        required: "Please enter your account.",
        maxlength: "Account cannot be more than 50 characters."
      },
      "password": {
        required: "Password must include uppercase,lowercase and number.",
        passwordValid: "Password must include uppercase,lowercase and number."
      },
      "department": {
        required: "Please choose your option."
      }
    },
    highlight: function (element) {
      $(element).addClass('has-error');
    },
    unhighlight: function (element) {
      $(element).removeClass('has-error');
    },
    onfocusout: function (element) {
      this.element(element); // triggers validation
    },
    onkeyup: function (element, event) {
      if ($(element).hasClass('valid')) {
        $(element).next().remove();
      }
      this.element(element); // triggers validation
    },
  });
  // onslick btn-reset
  $("#btn-reset").click(function () {
    $("#form").validate().resetForm();
    $("#form").find('.has-error').removeClass('has-error');
    // label has class error so remove it
    $("#form").find('label.error').remove();
  });
  // onlick btn-back so back to previous page
  $("#btn-back").click(function () {
    window.history.back();
  });
  // onlick btn-add so add new employee
  $("#btn-add").click(function (e) {
    // prevent default action
    e.preventDefault();
    var $form = $(this);
    // check form is valid or not
    if ($("#form").valid()) {
      // call ajax to add new employee
      $.ajax({
        type: "POST",
        url: "../employee/add",
        data: $("#form").serialize(),
        success: function (data) {
          console.log(data);
          if (data.success) {
            Swal.fire({
              icon: 'success',
              title: data.message,
              showConfirmButton: false,
              timer: 1000
            });
          } else {
            Swal.fire({
              icon: 'error',
              title: data.message,
              timer: 1000
            });
          }
        }
      });
    }
  });
  $("#btn-update").click(function (e) {
    e.preventDefault();
    var $form = $(this);
    if ($("#form").valid()) {
      $.ajax({
        type: "POST",
        url: "../employee/update",
        data: 
          $("#form").serialize() + "&user_login_department=" + sessionEmployee.department.department_name,
        success: function (data) {
          if (data.success) {
            Swal.fire({
              icon: 'success',
              title: data.message,
              showConfirmButton: false,
              timer: 1000
            }),
              // reload page after 1s
              setTimeout(function () {
                location.reload();
              }, 1000);
          } else {
            Swal.fire({
              icon: 'error',
              title: data.message,
              timer: 1000
            });
          }
        }
      });
    }
  });

  $("#btn-delete").click(function (e) {
    e.preventDefault();
    // swal button confirm
    Swal.fire({
      title: 'Are you sure?',
      text: "Do you want to delete this employee?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        var employee_id = $("#employee_id").val();
        $.ajax({
          type: "POST",
          url: "../employee/delete",
          data: {
            employee_id: employee_id
          },
          success: function (data) {
            if (data.success) {
              Swal.fire({
                icon: 'success',
                title: data.message,
                showConfirmButton: false,
                timer: 1000
              }),
                // reload page after 1s
                setTimeout(function () {
                  location.href = "../employee/list_employee";
                }, 1000);
            } else {
              Swal.fire({
                icon: 'error',
                title: data.message,
                timer: 1000
              });
            }
          }
        });
      }
    });
  });
  $("#btn-reset-password").click(function (e) {
    e.preventDefault();
    // swal button confirm
    Swal.fire({
      title: 'Are you sure?',
      text: "Do you want to reset password of this employee?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, reset it!'
    }).then((result) => {
      if (result.isConfirmed) {
        var employee_id = $("#employee_id").val();
        $.ajax({
          type: "POST",
          url: "../employee/employee_action",
          data: {
            employee_id: employee_id,
            action: "reset-password"
          },
          dataType: "json",
          success: function (data) {
            if (data.success) {
              Swal.fire({
                icon: 'success',
                title: data.message,
                showConfirmButton: false,
                timer: 1000
              }),
                // reload page after 1s
                setTimeout(function () {
                  location.reload();
                }, 1000);
            } else {
              Swal.fire({
                icon: 'error',
                title: data.message,
                timer: 1000
              });
            }
          }
        });
      }
    });
  });
});