document.querySelectorAll(".drop-nav").forEach((dropNav) => {
    dropNav.querySelector(".drop-nav-title").onclick = () => {
        dropNav.classList.toggle("drop");
    };
});

function checkValidSearch(filterBy, searchValue) {
    if (filterBy == 'employee_birthdate') {
        // check valid date format yyyy-mm-dd
        let dateRegex = /^([0-9]{4})-([0-9]{2})-([0-9]{2})$/;
        if (!dateRegex.test(searchValue)) {
            Swal.fire({
                icon: 'warning',
                title: 'Invalid date format',
                text: 'Please enter date in format yyyy-mm-dd'
              });
            return false;
        } else {
            return true;
        }
    }
    return true;
}

$(document).ready(function () {
    getFilterParams();

});

$(document).on('click', '#pagination li', function () {
    var page = $(this).attr('attr-page');
    if (page != undefined && page != "") {
        getFilterParams(Number(page));
    }
});

function filter() {
    getFilterParams();
}

function getFilterParams(pageNum) {
    let filterBy = $('select[name="filterType"]').val();
    let searchValue = $('input[name="ticketSearch"]').val();
    let pageSize = 5;
    let pageNumber = pageNum || 1;
    if (checkValidSearch(filterBy, searchValue) == false) {
        return;
    } else {
        getDataTable(pageNumber, pageSize, searchValue, filterBy);
    }
}
function renderPaging(pageNumber, pageSize, totalRecord) {
    let totalPage = Math.ceil(totalRecord / pageSize);
    const pageCaculate = pages(pageNumber, totalPage, 1);
    $('#pagination').empty();
    if (totalPage <= 0) return;
    $('#pagination').append(`<li class="page-item ${pageNumber == 1 ? 'disabled' : ''}" attr-page=${pageNumber == 1 ? 1 : (pageNumber - 1)}><a class="page-link" href="#">Previous</a></li>`);
    pageCaculate.forEach(element => {
        $('#pagination').append(`<li class="page-item ${element == pageNumber ? 'active' : ''}" attr-page=${element}><a class="page-link" href="#">${element}</a></li>`);
    });
    $('#pagination').append(`<li class="page-item ${pageNumber == totalPage ? 'disabled' : ''}" attr-page=${pageNumber == totalPage ? totalPage : (pageNumber + 1)}><a class="page-link" href="#">Next</a></li>`);

}
function getDataTable(pageNumber = 1, pageSize = 5, searchValue = '', filterBy = 'employee_name') {
    $.ajax({
        url: "../employee/list_employee",
        type: "POST",
        dataType: "json",
        data: {
            pageNumber,
            pageSize,
            searchValue,
            filterBy
        },
        success: function (res) {
            const data = res.data;
            renderTable(data.data);
            renderPaging(data.pageNumber, data.pageSize, data.totalRecord);

        }
    });
}

function renderTable(data) {
    $('#table-result tbody').empty();
    if (data.length == 0) {
        $('#table-result tbody').append(`<tr><td colspan="7">No matches</td></tr>`);
        return;
    }
    else {
        data.forEach(element => {
            $('#table-result tbody').append(`s
        <tr>
            <td>${element.employee_Id}</td>
            <td>${element.employee_name}</td>
            // moment format datefrom month day , year to yyyy-mm-dd
            <td>${moment(element.employee_birthdate).format('YYYY-MM-DD')}</td>
            <td>${element.employee_address}</td>
            <td>${element.employee_phone}</td>
            <td>${element.department.department_name}</td>
            <td>
                <a href="../employee/update?employee_id=${element.employee_Id}" ><i class="fas fa-eye mr-5"></i>View</a>
            </td>
        </tr>
        `);
        });
    }
}
function pages(pageNumber, totalPage, delta = 2) {
    let pages = [];
    for (let i = 1; i <= totalPage; i++) {
        if (i == 1 || i == totalPage || (i >= pageNumber - delta && i <= pageNumber + delta)) {
            pages.push(i);
        } else if (i == pageNumber - (delta + 1) || i == pageNumber + (delta + 1)) {
            pages.push('...');
        }
    }
    return pages;
}