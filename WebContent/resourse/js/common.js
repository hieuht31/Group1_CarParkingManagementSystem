/**
 * 
 */
document.querySelectorAll(".drop-nav").forEach((dropNav) => {
	dropNav.querySelector(".drop-nav-title").onclick = () => {
		dropNav.classList.toggle("drop");
	};
});

//  toast show

var messageRender = (messageType, message) => {
	let messageContainer = document.querySelector(".fe-toast-container")
	let toast = document.createElement('div');
	toast.classList.add(`fe-toast`)
	toast.classList.add(`fe-toast-show`)
	toast.classList.add(`${messageType}`)
	toast.innerHTML = `<div class="fe-toast-header">
    ${messageType.charAt(3).toUpperCase() + messageType.slice(4)}<i class="fas fa-times"></i>
  </div>
  <div class="fe-toast-content">${message}</div>`
	toast.querySelector(".fas").onclick = () => {
		toast.classList.remove("fe-toast-show");
	};
	messageContainer.appendChild(toast)
	setTimeout(() => {
		messageContainer.removeChild(toast)
		messageContainer.querySelectorAll('.fe-toast').forEach((trash) => {
			if (!trash.classList.contains('fe-toast-show')) trash.remove()
		})
	}, 5000)
}
document.querySelectorAll(".fe-toast").forEach((toast) => {
	toast.querySelector(".fas").onclick = () => {
		toast.classList.remove("fe-toast-show");
	};
});
/*
let toastBtns = document.querySelectorAll(".toast-show-btn button");
toastBtns[0].onclick = () => {
  messageRender("success","Add ticket success!")
};

toastBtns[1].onclick = () => {
  messageRender("warning","Please select month and year")
};

toastBtns[2].onclick = () => {
  messageRender("error","Oops have some error with connect server");
};
*/
// confirm 

class CustomerConfirm {
	constructor() {
		this.render = (mess, url) => {
			let customerConfirm = document.getElementById("confirm-box");
			let confirmMessage = customerConfirm.querySelector(".confirm-message");
			confirmMessage.innerHTML = mess;
			customerConfirm.querySelector(
				".confirm-footer"
			).innerHTML = `<button type="button" data-url=${url} class="btn btn-m danger">Yes</button>
        <button type="button" class="btn btn-m primary">No</button>`;
			customerConfirm.querySelectorAll(".btn").forEach((btn, index) => {
				btn.onclick = () => {
					customerConfirm.classList.remove("show");
					// click to yes
					if (index == 0) {
						//sendRequest("post",`${btn.dataset.url}`,(response)=>{console.log(response)})
						sendForm("post", `${btn.dataset.url}`)
						//sendRequest("DELETE", "Delete.html", loadDoc);
						console.log(`sended request to ${btn.dataset.url}`);
					}
				};
			});
			//      console.log(customerConfirm.querySelector(".btn").dataset.url);
			//      console.log(this.var);
			customerConfirm.classList.add("show");
		};
	}
}
var Alter = new CustomerConfirm();
/*
document.querySelector(".show-confirm").onclick = () => {
  new CustomerConfirm().render("hello", "delete?id=1");
};*/
document.querySelectorAll(".action-btn.delete").forEach((deleteBtn) => {
	deleteBtn.onclick = (e) => {
		e.preventDefault();
		Alter.render("Xoa ban ghi nay?", `${deleteBtn.getAttribute("href")}`);
	};
});

// Date filter field handler
let dateFilter = document.querySelector(".date-filter-field");
let selectDay = document.querySelector('select[name="day"]');
let selectMonth = document.querySelector('select[name="month"]');
let selectYear = document.querySelector('select[name="year"]');

const MONTH_IN_YEAR = [
	"Jan",
	"Feb",
	"Mar",
	"Apr",
	"May",
	"Jun",
	"Jul",
	"Aug",
	"Sep",
	"Oct",
	"Nov",
	"Dec",
];

if (dateFilter != null) {
	let currentDate =
		dateFilter.dataset.filter == ""
			? new Date()
			: new Date(dateFilter.dataset.filter);
	const monthRender = () => {
		if (selectMonth.length != 1) return;
		MONTH_IN_YEAR.forEach((month, index) => {
			console.log(currentDate.getMonth());
			selectMonth.innerHTML += `<option value="${index + 1}" 
      ${currentDate.getMonth() == index ? "selected" : ""
				}>${month}</option>`;
		});
	};

	const yearRender = () => {
		if (selectYear.length != 1) return;
		for (
			let year = currentDate.getFullYear() - 50;
			year <= currentDate.getFullYear() + 50;
			year++
		) {
			selectYear.innerHTML += `<option value="${year}" ${currentDate.getFullYear() == year ? "selected" : ""
				}>${year}</option>`;
		}
	};
	selectMonth.onclick = () => {
		currentDate = new Date();
		if (selectYear.selectedIndex == 0) {
			messageRender("warning", "Please select year");
			return;
		}
		monthRender();
	};
	// monthRender()
	selectYear.onclick = () => {
		currentDate = new Date();
		selectYear.setAttribute("required", "");
		selectMonth.setAttribute("required", "");
		selectDay.setAttribute("required", "");
		yearRender();
	};

	// console.log(selectMonth.value);
	getDayInMonth = (month, year) => {
		return new Date(selectYear.value, selectMonth.value, 0).getDate();
	};
	const dayInMonthRender = () => {
		if (selectMonth.selectedIndex == 0 || selectYear.selectedIndex == 0) {
			messageRender("warning", "Select month and year");
			return;
		}
		selectDay.innerHTML = ``;
		selectDay.innerHTML = `<option >--Day--</option>`;
		for (let day = 1; day <= getDayInMonth(); day++) {
			selectDay.innerHTML += `<option value="${day}" ${currentDate.getDate() == day ? "selected" : ""
				}>${day}</option>`;
		}
	};
	selectDay.onclick = () => {
		if (selectDay.length != 1) {
			return;
		}
		dayInMonthRender();
	};
	// data-filter onchange handler
	selectYear.onchange = () => {
		console.log(selectYear.selectedIndex);
		if (selectYear.selectedIndex == 0) {
			document.querySelector(
				".date-filter-field"
			).innerHTML = `<select name="day">
      <option value="">--Day--</option> 
  </select>
  <select name="month">
      <option value="">--Month--</option>
  </select>
  <select name="year">
      <option value="">--Year--</option>
  </select>`;
		} else {
			console.log("the hell");
			selectYear.setAttribute("required", "");
			selectMonth.setAttribute("required", "");
			selectDay.setAttribute("required", "");
		}
	};

	if (dateFilter.dataset.filter != "") {
		yearRender();
		monthRender();
		dayInMonthRender();
	}
}

let dateFilterField = document.querySelector("input[name='date-filter']");
if (dateFilterField != null) {
	let isInput = false;

	dateFilterField.addEventListener('keypress', () => {
		isInput = true;	
	})
	dateFilterField.onchange = () => {
		isChanging = true;
		console.log(dateFilterField.checkValidity())
		if(isInput) {
			setTimeout(() => {
				dateFilterField.parentElement.parentElement.submit();
				console.log("send")
			}, 5000)			
		} 
		else setTimeout(() => {
			dateFilterField.parentElement.parentElement.submit();
			console.log("send")
		}, 500)
	}

}



/*
let filterBy = document.querySelector("#filter-type");
if(filterBy.dataset.selected != ""){
	console.log(filterBy)
	filterBy.selectedIndex = --filterBy.dataset.selected
}
filterBy.onchange = ()=>{
	console.log(filterBy.selectedOptions)
}
var sendForm = (method, url) => {
	let form = document.createElement("form");
	form.action = url;
	form.method = method;
	document.body.appendChild(form);
	form.submit();
}*/