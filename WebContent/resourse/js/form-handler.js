document.querySelectorAll(".drop-nav").forEach((dropNav) => {
	dropNav.querySelector(".drop-nav-title").onclick = () => {
		dropNav.classList.toggle("drop");
	};
});


function activeDelete(){
	$(".action-btn.delete").each(function() {
		$(this).click(function(e) {
			e.preventDefault();
			// swal button confirm
			Swal.fire({
				title: 'Are you sure?',
				text: "Do you want to delete this record?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			}).then((result) => {
				if (result.isConfirmed) {
					$.ajax({
						type: "POST",
						url: $(this).attr('href'),
						success: function(data) {
							if (data.success) {
								Swal.fire({
									icon: 'success',
									title: data.message,
									showConfirmButton: false,
									timer: 1000
								}),
									// reload page after 1s
									setTimeout(function() {
										filter();
									}, 1000);
							} else {
								Swal.fire({
									icon: 'error',
									title: data.message,
									timer: 1000
								});
							}
						}
					});
				}
			});
		});
	});
}
// onlick btn-back so back to previous page
$("#btn-back").click(function() {
	window.history.back();
});
// onlick btn-add so add new employee
$("#btn-add").click(function(e) {
	// prevent default action
	e.preventDefault();
	var $form = $(this);
	// check form is valid or not
	if ($("#form").valid()) {
		// call ajax send data to add 
		$.ajax({
			type: "POST",
			url: "add",
			data: $("#form").serialize(),
			success: function(data) {
				console.log(data);
				if (data.success) {
					Swal.fire({
						icon: 'success',
						title: data.message,
						showConfirmButton: false,
						timer: 1000
					});
				} else {
					Swal.fire({
						icon: 'error',
						title: data.message,
						timer: 1000
					});
				}
			}
		});
	}
});

$("#btn-update").click(function(e) {
	e.preventDefault();
	var $form = $(this);
	if ($("#form").valid()) {
		$.ajax({
			type: "POST",
			url: "update",
			data: $("#form").serialize(),
			success: function(data) {
					console.log(data)
				if (data.success) {
					Swal.fire({
						icon: 'success',
						title: data.message,
						showConfirmButton: false,
						timer: 1000
					}),
						// reload page after 1s
						setTimeout(function() {
							location.reload();
						}, 1000);
				} else {
					Swal.fire({
						icon: 'error',
						title: data.message,
						timer: 1000
					});
				}
			}
		});
	}
});


