<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<nav>
	<ul>
		<li class="drop-nav repo">
			<div class="drop-nav-title">
				<i class="fas fa-plane rotate"></i>Welcome
				${sessionScope.employee.employee_name}<i class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links">
				<li class="drop-nav-link"><a
					href="../user/profile?employee_id=${sessionScope.employee.employee_Id}"><i
						class="fas fa-list"></i>User profile</a></li>
				<li class="drop-nav-link"><a href="../LogoutController"><i
						class="fas fa-plus"></i>Logout</a></li>
			</ul>
		</li>
		<li class="drop-nav"><div class="drop-nav-title
		
		">
				<i class="fas fa-tachometer-alt"></i><a>Dashboard</a>
			</div></li>
		<li class="drop-nav">
			<div class="drop-nav-title">
				<i class="fas fa-plane rotate"></i>Employee manager<i
					class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links">

				<li class="drop-nav-link" style="padding-left: 40px !important;"><a
					href="../employee/list_employee"><i class="fas fa-list"></i>List
						Employee</a></li>
				<li class="drop-nav-link" style="padding-left: 40px !important;"><a
					href="../employee/add"><i class="fas fa-plus"></i>Add Employee</a></li>
			</ul>
		</li>
	</ul>
</nav>