<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<nav>
	<ul>
		<li class="drop-nav repo">
			<div class="drop-nav-title">
				<i class="fas fa-plane rotate"></i>Welcome Admin<i
					class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links">
				<li class="drop-nav-link"><a
					href="../user/profile?employee_id=${sessionScope.employee.employee_Id}"><i
						class="fas fa-list"></i>User profile</a></li>
				<li class="drop-nav-link"><a href="../LogoutController"><i
						class="fas fa-plus"></i>Logout</a></li>
			</ul>
		</li>
		<li class="drop-nav"><div class="drop-nav-title
		">
				<i class="fas fa-tachometer-alt"></i><a>Dashboard</a>
			</div></li>

		<li class="drop-nav ${param.currentIndex>0&&param.currentIndex<3?"drop":""}">
			<div class="drop-nav-title">
				<i class="fas fa-plane rotate"></i>Employee manager<i
					class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links">
				<li class="drop-nav-link ${param.currentIndex==1?"active":""}"><a
					href="../employee/list_employee"><i class="fas fa-list"></i>List
						Employee</a></li>
				<li class="drop-nav-link ${param.currentIndex==2?"active":""}"><a
					href="../employee/add"><i class="fas fa-plus"></i>Add Employee</a></li>
			</ul>
		</li>
		<li class="drop-nav ${(param.currentIndex>3 && param.currentIndex<6)?"drop":""}">
			<div class="drop-nav-title">
				<i class="fas fa-car "></i>Car manager<i class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links">
				<li class="drop-nav-link ${param.currentIndex==4?"active":""}"><i
					class="fas fa-list"></i><a href="../car/view">Car list</a></li>
				<li class="drop-nav-link ${param.currentIndex==5?"active":""}"><i
					class="fas fa-plus"></i><a href="../car/add">Add Car</a></li>
			</ul>
		</li>
		<li class="drop-nav ${(param.currentIndex>5 && param.currentIndex<8)?"drop":""}">
			<div class="drop-nav-title">
				<i class="fas fa-cart-plus"></i>Booking office manager<i
					class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links">
				<li class="drop-nav-link ${param.currentIndex==6?"active":""}"><i
					class="fas fa-list"></i><a href="../booking/view">Booking
						office list</a></li>
				<li class="drop-nav-link ${param.currentIndex==7?"active":""}"><i
					class="fas fa-plus"></i><a href="../booking/add">Add Booking
						office</a></li>
			</ul>
		</li>
		<li
			class="drop-nav ${(param.currentIndex>7 && param.currentIndex<10)?"drop":""}">
			<div class="drop-nav-title">
				<i class="fas fa-map-marker-alt"></i>Parking lot manager<i
					class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links">
				<li class="drop-nav-link ${param.currentIndex==8?"active":""}"><i
					class="fas fa-list"></i><a href="../park/view">Parking lot list</a></li>
				<li class="drop-nav-link ${param.currentIndex==9?"active":""}"><i
					class="fas fa-plus"></i><a href="../park/add">Add Parking lot</a></li>
			</ul>
		</li>
		<li
			class="drop-nav ${(param.currentIndex>9 && param.currentIndex<12)?"drop":""}">
			<div class="drop-nav-title">
				<i class="fas fa-plane rotate"></i>Trip manager<i
					class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links ">
				<li class="drop-nav-link ${param.currentIndex==10?"active":""}"><i
					class="fas fa-list"></i><a href="../trip/list">List Trip</a></li>
				<li class="drop-nav-link ${param.currentIndex==11?"active":""}"><i
					class="fas fa-plus"></i><a href="../trip/add">Add Trip</a></li>
			</ul>
		</li>
		<li
			class="drop-nav ${(param.currentIndex>11 && param.currentIndex<14)?"drop":""}">
			<div class="drop-nav-title">
				<i class="fas fa-ticket-alt rotate"></i>Ticket manager<i
					class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links">
				<li class="drop-nav-link ${param.currentIndex==12?"active":""}"><i
					class="fas fa-list"></i><a href="../ticket/view">Ticket list</a></li>
				<li class="drop-nav-link ${param.currentIndex==13?"active":""}"><i
					class="fas fa-plus"></i><a href="../ticket/add">Add Ticket</a></li>
			</ul>
		</li>
	</ul>
</nav>