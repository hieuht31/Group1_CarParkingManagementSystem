<header>
	<div class="logo-container">
		<i class="${param.classes}"></i><span>${param.logo}</span>
	</div>
	<div class="content">
		<i class="fas fa-stream"></i><a href="../user/profile?employee_id=${sessionScope.employee.employee_Id}"><span>Welcome ${sessionScope.employee.employee_name}</span></a> <span><a href="../LogoutController"><i
				class="fas fa-sign-out-alt"></i>Logout</a></span>
	</div>
</header>