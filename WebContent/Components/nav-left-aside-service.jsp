<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<nav>
	<ul>
		<li class="drop-nav repo">
			<div class="drop-nav-title">
				<i class="fas fa-plane rotate"></i>Welcome
				${sessionScope.employee.employee_name}<i class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links">
				<li class="drop-nav-link"><a
					href="../user/profile?employee_id=${sessionScope.employee.employee_Id}"><i
						class="fas fa-list"></i>User profile</a></li>
				<li class="drop-nav-link"><a href="../LogoutController"><i
						class="fas fa-plus"></i>Logout</a></li>
			</ul>
		</li>
		<li class="drop-nav ${param.currentIndex<2?"drop":""}">
			<div class="drop-nav-title">
				<i class="fas fa-plane rotate"></i>Trip manager<i
					class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links ">
				<li
					${param.currentIndex==0?"class=\"drop-nav-link active\"":"class=\"drop-nav-link\""}><i
					class="fas fa-list"></i><a href="../trip/list">List Trip</a></li>
				<li
					${param.currentIndex==1?"class=\"drop-nav-link active\"":"class=\"drop-nav-link\""}><i
					class="fas fa-plus"></i><a href="../trip/add">Add Trip</a></li>
			</ul>
		</li>
		<li class="drop-nav ${param.currentIndex>=2?"drop":""}">
			<div class="drop-nav-title">
				<i class="fas fa-ticket-alt rotate"></i>Ticket manager<i
					class="fas fa-angle-left"></i>
			</div>
			<ul class="drop-nav-links">
				<li
					${param.currentIndex==2?"class=\"drop-nav-link active\"":"class=\"drop-nav-link\""}><i
					class="fas fa-list"></i><a href="../ticket/view">Ticket list</a></li>
				<li
					${param.currentIndex==3?"class=\"drop-nav-link active\"":"class=\"drop-nav-link\""}><i
					class="fas fa-plus"></i><a href="../ticket/add">Add Ticket</a></li>
			</ul>
		</li>
	</ul>



</nav>